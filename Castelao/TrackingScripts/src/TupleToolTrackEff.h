/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLTRACKEFF_H 
#define TUPLETOOLTRACKEFF_H 1

// Include files
// from Gaudi
#include "Kernel/IParticleTupleTool.h"
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Math/Boost.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "Event/State.h"
#include "Event/CaloCluster.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticle2MCAssociator.h"


/** @class TupleToolTrackEff TupleToolTrackEff.h
 *  
 *  Fill tracking efficiency information: Check if MuonTT track is matched to a long track
 *
 * - head_Assoc: Is MuonTT track matched to a long track?
 * - head_TTHitAssoc: Is MuonTT track matched to a long track with TT hits?
 * - head_noTTHitAssoc: Is MuonTT track matched to a long track without TT hits?
 * 
 * if Verbose is true:
 *
 * - head_upgradedMass: Invariant mass of tag track and track associated to probe track
 * - head_nTTHits: Number of TT hits of MuonTT track.
 * - head_Matched_nTTHits: Number of TT hits on track matched to MuonTT track.
 * - head_Matched_Px: x component of momentum of track matched to MuonTT track.
 * - head_Matched_Py: y component of momentum of track matched to MuonTT track.
 * - head_Matched_Pz: z component of momentum of track matched to MuonTT track.
 * - head_Matched_P: absolut value of momentum of track matched to MuonTT track.
 * - head_Matched_dP: error on first track state of track matched to MuonTT track.
 * - head_Matched_probChi2: ProbChi2 of track matched to MuonTT track.
 * - head_Matched_Chi2NDoF: Chi2 / ndof of track matched to MuonTT track.
 * - head_DeltaR: Delta R between MuonTT track and track matched to MuonTT track. 
 * - head_DeltaP: Momentum difference between MuonTT track and track matched to MuonTT track. 
 * - head_wrongDeltaR: Delta R between MuonTT track and track closest to MuonTT track, not being the matched one. A track was matched in this event.  
 * - head_wrongDeltaP: Momentum difference between MuonTT track and track closest to MuonTT track, not being the matched one. A track was matched in this event.
 * - head_wrongP: Momentum of track closest to MuonTT track, not being the matched one. A track was matched in this event.
 * - head_wrongDeltaR2: Delta R between MuonTT track and track closest to MuonTT track. No track was matched in this event.  
 * - head_wrongDeltaP2: Momentum difference between MuonTT track and track closest to MuonTT track. No track was matched in this event. 
 * - head_wrongP2: Momentum of track closest to MuonTT track. No track was matched in this event.
 * - head_MatchedMC_Px: x component of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_Py: y component of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_Pz: z component of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_P: Absolute value of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_PID: PID of MC particle mached to the track matched to MuonTT track.
 *
 * \sa TupleToolTrackEff, DecayTreeTuple, MCDecayTreeTuple
 * 
 *  @author Michel De Cian
 *  @date   2013-01-21
 */

class TupleToolTrackEff : public TupleToolBase, virtual public IParticleTupleTool {


public: 
  /// Standard constructor
  TupleToolTrackEff( const std::string& type, 
                      const std::string& name,
                      const IInterface* parent);

  virtual ~TupleToolTrackEff( ); ///< Destructor
  virtual StatusCode initialize() override;
  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& ) override;
 

protected:

private:
  
  StatusCode purityAssoc(const LHCb::Track* assocTrack, bool& correctMatch);
  const LHCb::MCParticle* assocMCPart( const LHCb::Track* assocTrack );
  void commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* muonTTTrack, double& muFraction, double& TTFraction, bool& hasTTHits);
  int numberAddedTTHits(const LHCb::Track* track);
  StatusCode inverseLHCbIDMatch( const LHCb::Particle* part, Tuples::Tuple& tuple, const std::string& head);
  StatusCode checkWrongDeltaR( const LHCb::Track* assocTrack, const LHCb::Track* muonTTTrack, double& wrongDeltaR, double& wrongDeltaP, double& wrongP);
  double getUpgradedMass(const LHCb::Particle* top,const LHCb::Particle* part,  const LHCb::Track* longTrack );
 
  double m_ttAssocFraction;
  double m_muonAssocFraction;
  bool m_inverseMatch;
  std::string m_motherLocation;
  std::string m_inverseMatchLocation;
  bool m_MC;
  
  IParticle2MCAssociator* m_p2mcAssoc;
  
  

};
#endif // TUPLETOOLTRACKEFF_H
