/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "VeloMuonTrackAnalyzer.h"
#include <boost/foreach.hpp>
#include "Linker/LinkedTo.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
//#include "LHCbID.h"
#include "Kernel/OTChannelID.h"
#include "TrackInterfaces/IHitExpectation.h"
//-----------------------------------------------------------------------------
// Implementation file for class : VeloMuonTrackAnalyzer
//
// 2011-03-04 : Paul Seyfert
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( VeloMuonTrackAnalyzer )

/// Default constructor with declaration of properties
VeloMuonTrackAnalyzer::VeloMuonTrackAnalyzer( const std::string& name, ISvcLocator* pSvcLocator )
  : GaudiAlgorithm ( name , pSvcLocator )
{
  declareProperty("InputLocation"    , m_inlocation      = "Rec/VeloMuon/Tracks");
  declareProperty("MC",                m_MC              = false);
  declareProperty("LinkLocations"    , m_linklocations   = std::vector<std::string>() );
  declareProperty("LinkCriteria"     , m_linkcriteria    = std::vector<int>() );
  declareProperty("LinkInfos"        , m_linkinfos       = std::vector<int>() );
  declareProperty("TrackTypes"       , m_linktypes       = std::vector<int>() );
}

VeloMuonTrackAnalyzer::~VeloMuonTrackAnalyzer() {}

StatusCode VeloMuonTrackAnalyzer::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  m_clonefinder = tool<ITrackCloneFinder>("TrackCloneFinder/low",this);
  return sc;
}

StatusCode VeloMuonTrackAnalyzer::finalize() {
  return GaudiAlgorithm::finalize();
}

StatusCode VeloMuonTrackAnalyzer::execute() {
  verbose() << "executing" << endmsg;
  if (!exist<LHCb::Tracks>(m_inlocation)) return StatusCode::SUCCESS;
  LHCb::Tracks* intracks = get<LHCb::Tracks>(m_inlocation);
  verbose() << "looking at " << intracks->size() << " VeloMuon Tracks" << endmsg;
  BOOST_FOREACH (LHCb::Track* track, *intracks) {
    SmartRefVector<LHCb::Track>::iterator vmAncestor;
/*    for (vmAncestor = track->ancestors().begin(); vmAncestor != track->ancestors().end(); ++vmAncestor) {
      if (LHCb::Track::Velo == (*vmAncestor)->type()) 
	// found check's velo
        break;
    }
  */  
    if (false && vmAncestor == track->ancestors().end()) {
      warning() << "bad checktrack" << endmsg;
      track->addInfo(4001,0);
      track->addInfo(4002,0);
      track->addInfo(4003,0);
    } else {
      LHCb::Track vbuffer,mbuffer;
      verbose() << "LHCbIDs: ";
      for (std::vector<LHCb::LHCbID>::const_iterator idIt = track->lhcbIDs().begin() ; track->lhcbIDs().end() != idIt ; ++idIt){
        verbose() << ".";
        if ((*idIt).isVelo())
          vbuffer.addToLhcbIDs(*idIt);
        else
          mbuffer.addToLhcbIDs(*idIt);
      } verbose() << endmsg;
      for (unsigned int critIt = 0 ; critIt < m_linklocations.size() ; ++critIt) {
        verbose() << "criterion " << critIt << endmsg;
//	LHCb::Tracks* conftracks = get<LHCb::Tracks>(m_linklocations[critIt]);
	if (!exist<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles")) {
		error() << "no muons in this event??? WTF?? " << endmsg; 
		return StatusCode::SUCCESS;
	}
          
        LHCb::Particle::Range confparts = get<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles");
	int bestconfirmed = 0;
        verbose() << "checking " << confparts.size() << " particles" << endmsg;
        BOOST_FOREACH(const LHCb::Particle* const pIt , confparts) {
        //for (LHCb::Particles::const_iterator pIt = confparts->begin() ; confparts->end() != pIt ; ++pIt) {
          LHCb::Track confbuffer;
          confbuffer.addToLhcbIDs((pIt)->proto()->track()->lhcbIDs());
          confbuffer.addToLhcbIDs((pIt)->proto()->muonPID()->muonTrack()->lhcbIDs()); 
	  // better not require on ancestors ?
	  if (m_linktypes[critIt]!=(pIt)->proto()->track()->type()) continue;
	  // here I should do some sanity-checks and look at the criterion
	  int confirmed = 0;

	  // link directly to the longtrack, but still use the velomuon's velo ancestor not to compare muon hits
//          if (m_clonefinder->areClones(*(*cIt),*(*vmAncestor)))

//          if (confbuffer.nCommonLhcbIDs(vbuffer) >= (track->nLHCbIDs() -4 )* 0.7 && 
 //             confbuffer.nCommonLhcbIDs(mbuffer) >= 3 )
	  if (0==mbuffer.nLHCbIDs()) error() << "No Muon hits" << endmsg;
	  if (0==vbuffer.nLHCbIDs()) error() << "No velo hits" << endmsg;
	  verbose() << "do confirmation" << endmsg;
          verbose() << confbuffer.nLHCbIDs() << " in confbuffer" << endmsg;
          verbose() << (int(confbuffer.nCommonLhcbIDs(mbuffer))) << " common muon hits" << endmsg;
          verbose() << (int(confbuffer.nCommonLhcbIDs(vbuffer))) << " common velo hits" << endmsg;
          confirmed |= (int(confbuffer.nCommonLhcbIDs(mbuffer)) >= 2)<<1;
          confirmed |= (int(confbuffer.nCommonLhcbIDs(mbuffer)) >= 3)<<2;
          confirmed |= (int(confbuffer.nCommonLhcbIDs(mbuffer)) >= 4)<<3;
          confirmed |= (int(confbuffer.nCommonLhcbIDs(vbuffer)) >= (track->nLHCbIDs() -4 ) * 0.5)<<4;
          confirmed |= (int(confbuffer.nCommonLhcbIDs(vbuffer)) >= (track->nLHCbIDs() -4 ) * 0.7)<<5;
          confirmed |= (int(confbuffer.nCommonLhcbIDs(vbuffer)) >= (track->nLHCbIDs() -4 ) * 1.0)<<6;
          if (confirmed>bestconfirmed) bestconfirmed=confirmed;
//          if (m_clonefinder->areClones(*(*cIt),*(track)))
//	    confirmed |= 1<<VeloMuonLinkCrit::directclone;
//	  SmartRefVector<LHCb::Track>::iterator cAncestor;
//	  for (cAncestor = (*cIt)->ancestors().begin() ; cAncestor != (*cIt)->ancestors().end() ; ++cAncestor) {
//	    if (LHCb::Track::Velo == (*cAncestor)->type()) break; // this is the Velo ancestor
//	  }
//	  if (cAncestor == (*cIt)->ancestors().end()) continue; // no Velo ancestor has been found, next conftrack
          continue;
//	  if (m_clonefinder->areClones(*(*vmAncestor),*(*cAncestor)))
//	  if (m_clonefinder->areClones(*(track),*(*cAncestor)))
//	    confirmed |= 1<<VeloMuonLinkCrit::ancestorclone;
	}
	track->addInfo(m_linkinfos[critIt],(int)bestconfirmed);
      }
    }
  }
  verbose() << "executed" << endmsg;
  return StatusCode::SUCCESS;
}

