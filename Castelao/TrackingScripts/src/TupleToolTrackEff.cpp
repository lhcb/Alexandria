/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 
#include "gsl/gsl_sys.h"

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Vector3DTypes.h"

// local
#include "TupleToolTrackEff.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolTrackEff
//
// 2013-01-21 : Michel De Cian
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolTrackEff )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolTrackEff::TupleToolTrackEff( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : TupleToolBase ( type, name , parent )
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("TTAssocFraction", m_ttAssocFraction = 0.6);
  declareProperty("MuonAssocFraction", m_muonAssocFraction = 0.7);
  declareProperty("InverseMatch", m_inverseMatch = false);
  declareProperty("InverseMatchLocation", m_inverseMatchLocation = "Phys/Jpsi/Particles");
  declareProperty("MotherLocation", m_motherLocation = "Phys/Jpsi/Particles");
  declareProperty("MC", m_MC = false);
}
//=============================================================================
// Destructor
//=============================================================================
TupleToolTrackEff::~TupleToolTrackEff() {} 

//=============================================================================
// initialize
//=============================================================================
StatusCode TupleToolTrackEff::initialize(){
  if( ! TupleToolBase::initialize() ) return StatusCode::FAILURE;

  std::string p2mcAssocType("DaVinciSmartAssociator");
  m_p2mcAssoc = tool<IParticle2MCAssociator>(p2mcAssocType,this);

  
  return StatusCode::SUCCESS ;
}
//=============================================================================
// Fill
//=============================================================================
StatusCode TupleToolTrackEff::fill( const LHCb::Particle* top
                                     , const LHCb::Particle* part
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  
  // -------------------------
  // -- This is to obtain samples to train the association on fully reconstructed decays.
  // -------------------------
  if(m_inverseMatch){
    if ( msgLevel(MSG::INFO) ) info() << "TupleToolTrackEff is in inverse match mode!" << endmsg;
    inverseLHCbIDMatch( top, tuple, head).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    return StatusCode::SUCCESS;
  }
  // ---------------------
  

  if( part == top ){
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "Mother resonance does not have track: skipping" << endmsg;
    return StatusCode::SUCCESS ;
  }
  
  // -- Navigate back from the (MuonTT) particle to the track.
  const LHCb::ProtoParticle* proto = part->proto();
  if(!proto){
    if ( msgLevel(MSG::WARNING) ) warning() << "Could not find protoparticle corresponding to particle" << endmsg;
    return StatusCode::SUCCESS ;
  }
   
  const LHCb::Track* muonTTTrack = proto->track();
  if(!muonTTTrack){
    if ( msgLevel(MSG::WARNING) ) warning() <<  "Could not find track corresponding to protoparticle" << endmsg;
    return StatusCode::SUCCESS ;
  }

  // ---------------------------------------------------------------
  
  int maxAssocFractionTT =  m_ttAssocFraction;
  int maxAssocFractionMuon = m_muonAssocFraction;

  
  // -- Get all long tracks in the event
  if( !exist<LHCb::Tracks>(LHCb::TrackLocation::Default)){
    if ( msgLevel(MSG::ERROR) ) error() << "Container " << LHCb::TrackLocation::Default << " does not exist" << endmsg;
    return StatusCode::SUCCESS;
  }
  
  LHCb::Tracks* tracks = get<LHCb::Tracks>(LHCb::TrackLocation::Default);
  const LHCb::Track* assocTrack = NULL;

  bool TTHitAssoc = false;
  bool noTTHitAssoc = false;

  for( LHCb::Tracks::const_iterator it = tracks->begin() ; it != tracks->end() ; ++it){

    LHCb::Track* longTrack = *it;
    if(longTrack->type() != 3) continue; // -- track has to be a long track
    
    double muFraction = 0;
    double TTFraction = 0;
    bool hasTTHits = false;
  
    // -- Check the overlap between MuonTT track and long track in the muon system and the TT
    commonLHCbIDs(longTrack, muonTTTrack, muFraction, TTFraction, hasTTHits); 
    
    // -- Split case depending if the long track has TT hits or not...
    if(hasTTHits){
      if(muFraction > m_muonAssocFraction && TTFraction > m_ttAssocFraction ){
	
	TTHitAssoc = true;
	
	if( maxAssocFractionTT < TTFraction || maxAssocFractionMuon < muFraction){
	  assocTrack = longTrack;
	}
	  
	if( maxAssocFractionTT < TTFraction) maxAssocFractionTT = TTFraction;
	if( maxAssocFractionMuon < muFraction) maxAssocFractionMuon = muFraction;

      }
      
    }else{
      if(muFraction >  m_muonAssocFraction){
	
	noTTHitAssoc = true;
	
	if( maxAssocFractionMuon < muFraction){
	  assocTrack = longTrack;
	}
	
	if( maxAssocFractionMuon < muFraction) maxAssocFractionMuon = muFraction;
	
      }
    }
  }
  
  
  bool assoc = TTHitAssoc || noTTHitAssoc;
 
  tuple->column(head+"_TTHitAssoc", TTHitAssoc).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_noTTHitAssoc", noTTHitAssoc).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Assoc", assoc).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  // -- This ends the basic functionality of this tool. The rest is for more special cases and therefore only written out
  // -- if verbose() is set to true.

  if( isVerbose() ){

    double upgradedMass = getUpgradedMass( top, part, assocTrack );
    
    // -- Associate MC particle to the long track that was associated to the muonTT track.
    const LHCb::MCParticle* assocPart = NULL;
    if( m_MC && assocTrack ) assocPart = assocMCPart( assocTrack );
    
    int muonTTTTHits = numberAddedTTHits( muonTTTrack );

    double sigma_pLong = -1000.0;
    double pChi2Long =  -1000.0; 
    double chi2NDoFLong = -1000.0;
    int assocTTHits = -1;
    
    double deltaR = -1;
    double deltaP = -1;
    
    double wrongDeltaR = -1;
    double wrongDeltaP = -1;
    double wrongP = -1;
    
    double wrongDeltaR2 = -1;
    double wrongDeltaP2 = -1;
    double wrongP2 = -1;

    double deltaPhi = -1000.0;
    double deltaEta = -1000.0;

    double assocTrackPX = -1e10;
    double assocTrackPY = -1e10;
    double assocTrackPZ = -1e10;
    double assocTrackP = -1e10;

    if( assocTrack != NULL ){
      
      assocTrackPX = assocTrack->momentum().X();
      assocTrackPY = assocTrack->momentum().Y();
      assocTrackPZ = assocTrack->momentum().Z();
      assocTrackP = assocTrack->p();

      sigma_pLong = sqrt(assocTrack->firstState().errP2());
      pChi2Long =  assocTrack->probChi2();
      chi2NDoFLong = assocTrack->chi2()/assocTrack->nDoF();
     
      assocTTHits = numberAddedTTHits( assocTrack );
      // ----------------------------------------------------------
      
      // -- Calculate Delta R between the muonTT track and the associated track
      deltaPhi = fabs( assocTrack->phi() - muonTTTrack->phi() );
      if(deltaPhi > M_PI) deltaPhi  = 2*M_PI-deltaPhi;
      deltaEta = assocTrack->pseudoRapidity() - muonTTTrack->pseudoRapidity();
      deltaR = sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta );
      deltaP = assocTrack->p() - muonTTTrack->p();
      // -----------------------------------------------------------------
    
      checkWrongDeltaR( assocTrack, muonTTTrack, wrongDeltaR, wrongDeltaP, wrongP ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      
            
      // -- If there was no track associated, check what the DeltaR of the closest is.
    }else{
      checkWrongDeltaR( assocTrack, muonTTTrack, wrongDeltaR2, wrongDeltaP2, wrongP2 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    }


    // -- And now fill the tuple...
    
    tuple->column(head+"_upgradedMass", upgradedMass).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_nTTHits", muonTTTTHits).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_nTTHits", assocTTHits).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    
    tuple->column(head+"_Matched_Px", assocTrackPX).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_Py", assocTrackPY).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_Pz", assocTrackPZ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_P", assocTrackP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_dP", sigma_pLong).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_probChi2", pChi2Long ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_Chi2NDoF", chi2NDoFLong).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    
    tuple->column(head+"_DeltaR", deltaR).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_DeltaP", deltaP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_wrongDeltaR", wrongDeltaR).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_wrongDeltaP", wrongDeltaP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_wrongP", wrongP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_wrongDeltaR2", wrongDeltaR2).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_wrongDeltaP2", wrongDeltaP2).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_wrongP2", wrongP2).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

    if(m_MC && assocPart){
      tuple->column(head+"_MatchedMC_Px", assocPart->momentum().x()).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_Py", assocPart->momentum().y()).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_Pz", assocPart->momentum().z()).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_P", assocPart->p()).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_PID", assocPart->particleID().pid()).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    }
    

  }
  

  return StatusCode::SUCCESS ;
  
}
//=============================================================================
// Count how many TT hit and Muon hit LHCbIDs are the same
//=============================================================================
void TupleToolTrackEff::commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* muonTTTrack, double& muFraction, double& TTFraction, bool& hasTTHits){

  // -- Get the MuonPID objects
  if( !exist<LHCb::MuonPIDs>( LHCb::MuonPIDLocation::Offline ) ){
    muFraction = 0;
    TTFraction = 0;
    return;
  }

  LHCb::MuonPIDs* muPIDs   = get<LHCb::MuonPIDs>( LHCb::MuonPIDLocation::Offline ); 

  // -- Get the LHCbIDs of the Probe-track and fill them into a Muon- and a TT container
  std::vector<LHCb::LHCbID> muonTTTrackIDs = muonTTTrack->lhcbIDs();
  std::vector<LHCb::LHCbID> muonTTTrackTTIDs;
  std::vector<LHCb::LHCbID> muonTTTrackMuonIDs;
  
  std::vector<LHCb::LHCbID> longTrackTTIDs;

  int longMatch = 0;
  int muonMatch = 0;

  for(LHCb::MuonPIDs::iterator iMu = muPIDs->begin() ; iMu != muPIDs->end() ; ++iMu){

    LHCb::MuonPID* muPID = (*iMu);

    const LHCb::Track* muonTrack = muPID->muonTrack(); // The track segment in the muon station
    const LHCb::Track* idTrack = muPID->idTrack(); // The track which was id'ed
   
    // -- Match the track we want to test with the track from PIDs...
    if(idTrack != longTrack || muonTrack == NULL || idTrack == NULL) continue;
    
    longTrackTTIDs = idTrack->lhcbIDs();
    //muonPID = muPID;

    muonMatch = (int)muonTrack->nCommonLhcbIDs( *muonTTTrack );
    longMatch = (int)idTrack->nCommonLhcbIDs( *muonTTTrack );
    
  }
  
  // -- Check if the long track has TT hits
  hasTTHits = false;
  for( std::vector<LHCb::LHCbID>::iterator it = longTrackTTIDs.begin() ; it != longTrackTTIDs.end() ; ++it){
    
    LHCb::LHCbID id = *it;
    if(id.isTT()){
      hasTTHits = true;
      break;
    }
    
  }
  
  // -- Sort LHCbIDs of muonTT track in muon IDs and TT IDs
  for( std::vector<LHCb::LHCbID>::iterator it = muonTTTrackIDs.begin() ; it != muonTTTrackIDs.end() ; ++it){

    LHCb::LHCbID id = (*it);

    if(id.isMuon()) muonTTTrackMuonIDs.push_back(id);
    if(id.isTT()) muonTTTrackTTIDs.push_back(id);

  }
  
  
  muFraction = double(muonMatch)/muonTTTrackMuonIDs.size();
  TTFraction = double(longMatch)/muonTTTrackTTIDs.size();

}
//=============================================================================
// Count how many TT hit and Muon hit LHCbIDs are the same
//=============================================================================
int TupleToolTrackEff::numberAddedTTHits(const LHCb::Track* track){

  int nTTHits = 0;

  for(std::vector<LHCb::LHCbID>::const_iterator it = track->lhcbIDs().begin() ;it != track->lhcbIDs().end(); ++it){
    LHCb::LHCbID id = (*it);
	
    if( id.isTT() ) nTTHits++;
  }
  
  return nTTHits;
}
//=============================================================================
// Check purity of association
//=============================================================================
StatusCode TupleToolTrackEff::purityAssoc(const LHCb::Track* assocTrack, bool& correctMatch){

  // ----------------------------
  // -- Make a check on MC: If I associate a track, how often is it the right one?
  if(!exist<LHCb::Particle::Range>( m_motherLocation )){
    if ( msgLevel(MSG::WARNING) ) warning() << "Could not find container: " << m_motherLocation << endmsg;
    return StatusCode::FAILURE;
  }
  
  
  
  LHCb::Particle::Range mother = get<LHCb::Particle::Range>( m_motherLocation );
  LHCb::Particle::Range::const_iterator it = mother.begin(); 
  if( it == mother.end() ) return StatusCode::FAILURE;
  
  
  const SmartRefVector< LHCb::Particle >& daughters = (*it)->daughters();
  if(daughters.size() != 2) return StatusCode::SUCCESS;
  
  const LHCb::Track* longTrack = NULL;
  
  // -- make sure the tag has the right charge
  if( daughters.at(0)->particleID().threeCharge()/3 == assocTrack->charge() ){
    
    // -- Get the long track from the fully reconstructed Mother
    if( !daughters.at(0)->proto() ) return StatusCode::SUCCESS;
    if( !daughters.at(0)->proto()->track() ) return StatusCode::SUCCESS;
    longTrack = daughters.at(0)->proto()->track();
    
  }
  
  if( daughters.at(1)->particleID().threeCharge()/3 == assocTrack->charge() ){
    
    // -- Get the long track from tag-and-probe
    if( !daughters.at(1)->proto() ) return StatusCode::SUCCESS;
    if( !daughters.at(1)->proto()->track() ) return StatusCode::SUCCESS;
    longTrack = daughters.at(1)->proto()->track();
    
  }
  
  if( longTrack == assocTrack ){
    correctMatch = true;
  }else{
    correctMatch = false;
  }
  
  return StatusCode::SUCCESS;

}
//=============================================================================
// Check if the associated track really belongs to a muon from the corresponding mother in MC
//=============================================================================
const LHCb::MCParticle* TupleToolTrackEff::assocMCPart( const LHCb::Track* assocTrack ){
  

  LHCb::Particle::Range longTracks;

  // -- Warning: This is hardcoded!
  if(exist<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles")){
    longTracks = get<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles");
  }else{
    if ( msgLevel(MSG::DEBUG) ) debug() << "Could not find container: " << "Phys/StdAllLooseMuons/Particles" << endmsg;
    return NULL;
  }
  
  const LHCb::Particle* assocPart = NULL;

  for(LHCb::Particle::Range::const_iterator it = longTracks.begin() ; it != longTracks.end() ; ++it){

    const LHCb::Particle* part = (*it);
    if( !part->proto() ) continue;
    if( !part->proto()->track() ) continue;

    const LHCb::Track* track = part->proto()->track();

    if( track == assocTrack){
      assocPart = part;
      break;
    }

  }
    
  if( assocPart == NULL) return NULL;
  
  const LHCb::MCParticle* mcp = NULL;
  if( assocPart->particleID().pid() != 0){
    mcp = m_p2mcAssoc->relatedMCP( assocPart );
  }
 
  

  return mcp;


}
//=============================================================================
// Take a reconstructed long track, get LHCbIDs and see how many of them are also on the muonTT track
//=============================================================================
StatusCode TupleToolTrackEff::inverseLHCbIDMatch( const LHCb::Particle* top, Tuples::Tuple& tuple, const std::string& head ){

  
  
  if(!exist<LHCb::Particle::Range>( m_inverseMatchLocation )){
    if ( msgLevel(MSG::WARNING) ) warning() << "Could not find container: " << m_inverseMatchLocation << endmsg;
    return StatusCode::SUCCESS;
  }

  LHCb::Particle::Range mother = get<LHCb::Particle::Range>( m_inverseMatchLocation );
  LHCb::Particle::Range::const_iterator it = mother.begin(); 
  const SmartRefVector< LHCb::Particle >& daughters = (*it)->daughters();
  if(daughters.size() == 0){
    if ( msgLevel(MSG::WARNING) ) warning() << "Mother particle does not have daughters!" << endmsg;
    return StatusCode::SUCCESS;
  }
  
    
  // -- Get the daughters of the reconstructed Mother (one long and one muonTT)
  const SmartRefVector< LHCb::Particle >& muonTTDaughters = top->daughters();
  if( muonTTDaughters.size() != 2){
    if ( msgLevel(MSG::WARNING) ) warning() << "Mother particle does not have 2 daughters!" << endmsg;
    return StatusCode::SUCCESS;
  }
  
  const LHCb::Particle* muonTTPart = NULL;
  const LHCb::Track* muonTTLongTrack = NULL;
  const LHCb::Track* longTrack1 = NULL;
  const LHCb::Track* longTrack2 = NULL;
  

  // -- Get the long track and the muonTT particle from tag-and-probe
  if( muonTTDaughters.at(1)->proto() && muonTTDaughters.at(0)->proto()){
    if( muonTTDaughters.at(1)->proto()->track() && muonTTDaughters.at(0)->proto()->track()){
      if( muonTTDaughters.at(1)->proto()->track()->history() == LHCb::Track::PatForward || 
	  muonTTDaughters.at(1)->proto()->track()->history() == LHCb::Track::TrackMatching ){
	muonTTPart =  muonTTDaughters.at(0);
	muonTTLongTrack =  muonTTDaughters.at(1)->proto()->track();
      }else{
	muonTTPart =  muonTTDaughters.at(1);
	muonTTLongTrack =  muonTTDaughters.at(0)->proto()->track();
      }
    }
  }
  
  
  // -- make sure the tag has the right charge
  if( daughters.at(0)->particleID() == muonTTPart->particleID()){
  
    // -- Get the long track from normal reconstruction
    if( !daughters.at(0)->proto() || !daughters.at(1)->proto() ) return StatusCode::SUCCESS;
    if( !daughters.at(0)->proto()->track() || !daughters.at(1)->proto()->track() ) return StatusCode::SUCCESS;
    longTrack1 = daughters.at(0)->proto()->track();
    longTrack2 = daughters.at(1)->proto()->track();
  }
  
  if( daughters.at(1)->particleID() == muonTTPart->particleID()){
  
    // -- Get the long track from normal reconstruction
    if( !daughters.at(1)->proto() ) return StatusCode::SUCCESS;
    if( !daughters.at(1)->proto()->track() ) return StatusCode::SUCCESS;
    longTrack1 = daughters.at(1)->proto()->track();
    longTrack2 = daughters.at(0)->proto()->track();
  }

  // -- This ensures the tag track and one long track from the full reconstruction are the same
  if( longTrack1 != muonTTLongTrack ) return StatusCode::SUCCESS;

  // -- At this stage we should have matched one long track from the normal reconstruction with the tag from the tag-and-probe
  double muFraction = -1;
  double TTFraction = -1;
  bool hasTTHits = false;
  double deltaR = 10;
  double deltaP = 100000;

  
  // -- now get the muonTT track from the probe
  if(!muonTTPart->proto()) return StatusCode::SUCCESS; 
  if(!muonTTPart->proto()->track())  return StatusCode::SUCCESS;
  const LHCb::Track* muonTTTrack = muonTTPart->proto()->track();

  // -- now check how many lhcbIDs these tracks have in common
  commonLHCbIDs(longTrack2, muonTTTrack, muFraction,TTFraction, hasTTHits);
  
  // -- Calculate DeltaR between long track and muonTT track
  double deltaPhi = fabs( longTrack2->phi() - muonTTTrack->phi() );
  if(deltaPhi > M_PI) deltaPhi  = 2*M_PI-deltaPhi;
  
  double deltaEta = longTrack2->pseudoRapidity() - muonTTTrack->pseudoRapidity();
  
  deltaR = sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta );
    
  // -----------------------------------------------------

  deltaP = longTrack2->p() - muonTTTrack->p();
  
  // -- Fill the information for the correctly matched track
  tuple->column(head+"_muonFraction", muFraction).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_TTFraction", TTFraction).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_hasTTHits", hasTTHits).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_deltaR", deltaR).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_deltaP", deltaP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  

  // -- And now check the fraction for tracks which DO NOT belong to the decay
  if(!exist<LHCb::Tracks>(LHCb::TrackLocation::Default)){
    tuple->column(head+"_muonFractionWrong", -1).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_TTFractionWrong", -1).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_hasTTHits", -1).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_deltaRWrong", -1).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_deltaPWrong", -1).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    return StatusCode::SUCCESS;
  }

  if( !exist<LHCb::Tracks>(LHCb::TrackLocation::Default)){
    if ( msgLevel(MSG::WARNING) ) warning() << "Could not find container of long tracks" << endmsg;
    return StatusCode::SUCCESS;
  }
  
  LHCb::Tracks* tracks = get<LHCb::Tracks>(LHCb::TrackLocation::Default);
  

  muFraction = -1;
  TTFraction = -1;
  bool hasTTHitsWrong = false;
  deltaR = 10;
  deltaP = 100000;

  for( LHCb::Tracks::const_iterator it = tracks->begin() ; it != tracks->end() ; ++it){

    const LHCb::Track* wrongTrack = (*it);
    if( wrongTrack == longTrack2 ) continue; // exclude the right one (the one which was used before to match the probe track)
    if( wrongTrack->charge() != muonTTTrack->charge() ) continue;
    
    
    double muFractionVar = -2;
    double TTFractionVar = -2;
    bool hasTTHitsWrongVar = false;

    commonLHCbIDs(wrongTrack, muonTTTrack, muFractionVar,TTFractionVar, hasTTHitsWrongVar);

    if(muFractionVar > muFraction || TTFractionVar > TTFraction){
      muFraction = muFractionVar;
      TTFraction = TTFractionVar;
      hasTTHitsWrong = hasTTHitsWrongVar;
    }
      
    // -----------------------------------------------------
    double deltaPhi = fabs( wrongTrack->phi() - muonTTTrack->phi() );
    if(deltaPhi > M_PI) deltaPhi  = 2*M_PI-deltaPhi;
    
    double deltaEta = wrongTrack->pseudoRapidity() - muonTTTrack->pseudoRapidity();

    double deltaRVar = sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta );
    if(deltaRVar < deltaR) deltaR = deltaRVar;

    double deltaPVar = wrongTrack->p() - muonTTTrack->p();
    if( fabs(deltaPVar) < fabs(deltaP)) deltaP = deltaPVar;
    // -----------------------------------------------------



  }
  
  tuple->column(head+"_muonFractionWrong", muFraction).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_TTFractionWrong", TTFraction).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_hasTTHitsWrong", hasTTHitsWrong).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_deltaRWrong", deltaR).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_deltaPWrong", deltaP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return StatusCode::SUCCESS;

}


//=============================================================================
// Check delta R for muon tracks which are not associated!
//=============================================================================
StatusCode TupleToolTrackEff::checkWrongDeltaR( const LHCb::Track* assocTrack, const LHCb::Track* muonTTTrack, 
						double& wrongDeltaR, double& wrongDeltaP, double& wrongP){

  
  LHCb::MuonPIDs* muPIDs = get<LHCb::MuonPIDs>( LHCb::MuonPIDLocation::Offline ); 

  double deltaR = 10;
  double deltaP = -1;

  for(LHCb::MuonPIDs::iterator iMu = muPIDs->begin() ; iMu != muPIDs->end() ; ++iMu){
    
    const LHCb::MuonPID* muPID = (*iMu);
    if(!muPID->IsMuon()) continue;
    const LHCb::Track* wrongTrack = muPID->idTrack(); 
    if( wrongTrack == NULL) continue;
   
    if(assocTrack != NULL){
      if( wrongTrack == assocTrack || wrongTrack->charge() != assocTrack->charge()) continue;
    }else{
      if( wrongTrack->charge() != muonTTTrack->charge()) continue;
    }
    
    double deltaPhi = fabs( wrongTrack->phi() - muonTTTrack->phi() );
    if(deltaPhi > M_PI) deltaPhi  = 2*M_PI-deltaPhi;
    double deltaEta = wrongTrack->pseudoRapidity() - muonTTTrack->pseudoRapidity();
    double deltaRVar = sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta );
    if(deltaRVar < deltaR){
      deltaR = deltaRVar;
      deltaP = fabs( wrongTrack->p() - muonTTTrack->p() );
      wrongP = wrongTrack->p();
    }
 
  }

  wrongDeltaR = deltaR;
  wrongDeltaP = deltaP;

  return StatusCode::SUCCESS;




}
//=============================================================================
// Get the invariant mass of the long-long combination the muonTT track is associated to
//=============================================================================
double TupleToolTrackEff::getUpgradedMass(const LHCb::Particle* top,const LHCb::Particle* part,  const LHCb::Track* longTrack ){

  // -- Calculate the invariant mass of the Jpsi with the longTrack muon and the track associated to the muonTT muon.
  const SmartRefVector< LHCb::Particle >& recoDaughters = top->daughters();
  const LHCb::Particle* longTrackDaughter = NULL;
    
  // -- Mother needs to have exactly 2 daughters
  if( recoDaughters.size() != 2 ) return -1;

  // -- get particle which has the opposite charge than the muonTT particle
  if( recoDaughters.at(0)->charge() != part->charge() ){
    longTrackDaughter = recoDaughters.at(0);
  }
  if( recoDaughters.at(1)->charge() != part->charge() ){
    longTrackDaughter = recoDaughters.at(1);
  }
    
  Gaudi::LorentzVector assocTrackMomentum(0,0,0,0);
  
  if( !exist<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles") ) return -1; // is this good?
  LHCb::Particle::Range muonParticles = get<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles");
    
    
  // -- Find particle corresponding to the associated long track and get momentum
  for( LHCb::Particle::Range::const_iterator it = muonParticles.begin() ; it != muonParticles.end(); ++it){
    const LHCb::Particle* muonPart = (*it);
    if( !muonPart->proto() ) continue;
    if( !muonPart->proto()->track() ) continue;
    if( muonPart->proto()->track() == longTrack) assocTrackMomentum = muonPart->momentum();
    
  }
  
  // -- Calculate invariant mass of both daughters
  Gaudi::LorentzVector longTrackDaughterMomentum = longTrackDaughter->momentum();
  Gaudi::LorentzVector MotherMomentum = longTrackDaughterMomentum + assocTrackMomentum;
  double MotherMass = MotherMomentum.mass();
  
  return MotherMass;


}

