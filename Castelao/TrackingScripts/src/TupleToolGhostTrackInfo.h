/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLGhostTrackInfo_H
#define TUPLETOOLGhostTrackInfo_H 1

// Include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"
#include "MCInterfaces/ITrackGhostClassification.h"



/** @class TupleToolGhostTrackInfo
 *
 *  @author Paul Seyfert
 *  @date   2016-09-12
 */
class TupleToolGhostTrackInfo : public TupleToolBase,
                           virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolGhostTrackInfo( const std::string& type,
		    const std::string& name,
		    const IInterface* parent);

  virtual ~TupleToolGhostTrackInfo(){}; ///< Destructor

  virtual StatusCode initialize() override; ///< Algorithm initialization

  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& ) override;
  
private:
  ITrackGhostClassification*    m_ghostClassification;

};

#endif // TUPLETOOLGhostTrackInfo_H
