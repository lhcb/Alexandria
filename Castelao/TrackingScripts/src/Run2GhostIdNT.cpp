/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Kernel/HitPattern.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/KalmanFitResult.h"
#include "Event/STCluster.h"
#include "Event/VeloCluster.h"
#include "Event/GhostTrackInfo.h" 

#include "TMath.h"

// local
#include "Run2GhostIdNT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Run2GhostIdNT
//
// 2015-02-06 : Paul Seyfert
// following an earlier version by Angelo Di Canto
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( Run2GhostIdNT )

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
Run2GhostIdNT::Run2GhostIdNT( const std::string& type,
                                const std::string& name,
                                const IInterface* parent)
: GaudiTupleTool ( type, name, parent )
{
  declareInterface<IGhostProbability>(this);

}

//=============================================================================
//

namespace {
 //static const int largestChannelIDType = 1+std::max(LHCb::LHCbID::OT,std::max(LHCb::LHCbID::TT,std::max(LHCb::LHCbID::Velo,LHCb::LHCbID::IT)));
 static const int largestTrackTypes = 1+LHCb::Track::Ttrack;}


StatusCode Run2GhostIdNT::finalize()
{
  return GaudiTupleTool::finalize();
}

StatusCode Run2GhostIdNT::initialize()
{
  if( !GaudiTupleTool::initialize() ) return StatusCode::FAILURE;
  //IIncidentSvc* incsvc = svc<IIncidentSvc>("IncidentSvc") ;
  //incsvc->addListener(this, IncidentType::BeginEvent);

  m_ghostTool = tool<IGhostProbability>("Run2GhostId",this);
  m_classification = tool<ITrackGhostClassification>("AllTrackGhostClassification",      this);
  if (largestTrackTypes<=std::max(LHCb::Track::Ttrack,std::max(
     std::max(LHCb::Track::Velo,LHCb::Track::Upstream),
     std::max(LHCb::Track::Ttrack,LHCb::Track::Downstream))))
    return Warning("ARRAY SIZE SET WRONG (largestTrackTypes is smaller than enum LHCb::Track::Types",StatusCode::FAILURE);

  return StatusCode::SUCCESS;
}


//=============================================================================
StatusCode Run2GhostIdNT::execute(LHCb::Track& aTrack) const
{
  std::vector<float> variables      = m_ghostTool->netInputs(aTrack);
  std::vector<std::string> varnames = m_ghostTool->variableNames(aTrack.type());
  varnames.push_back("offset");

  if (m_ghostTool->execute(aTrack).isFailure()) {
    return StatusCode::SUCCESS;
  }

  LinkerTool < LHCb::Track, LHCb::MCParticle >* link = new LinkerTool < LHCb::Track, LHCb::MCParticle >( evtSvc(), "Rec/Track/Best" );
  const LinkerTool < LHCb::Track, LHCb::MCParticle >::DirectType* table = link->direct();
  LinkerTool < LHCb::Track, LHCb::MCParticle >::DirectType::Range range = table->relations( &aTrack );


  Tuples::Tuple tup = GaudiTupleTool::nTuple( "tracks",CLID_ColumnWiseTuple);

  if (varnames.size() != variables.size()) fatal() << "ALARM" << endmsg;
  for (unsigned i = 0 ; i < varnames.size() ; ++i) {
    tup->column( varnames[i].c_str(), variables[i] ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  }

  tup->column( "ghostprob", (float)aTrack.ghostProbability() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tup->column("tracks_PP_TrackHistory", (float)aTrack.history()).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tup->column("tracks_TRACK_Type", (float)aTrack.type()).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  LHCb::GhostTrackInfo gInfo;
  if (m_classification->info(aTrack,gInfo).isSuccess()) {
    tup->column( "ghostCat",gInfo.classification() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  }
  tup->column("tracks_assoc", (float)(int)(!( range.empty() ))).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tup->column("mctruepid", (float)(int) ((range.empty())?0:(range.begin()->to()->particleID().pid()))).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  tup->write().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return StatusCode::SUCCESS;
}

