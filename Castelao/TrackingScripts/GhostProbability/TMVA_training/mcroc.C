/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <TChain.h>
#include <TLegend.h>
#include <TString.h>
#include <vector>
#include <utility>
#include <TGraphAsymmErrors.h>
#include <TCanvas.h>
#include <TEfficiency.h>
#include <TH1F.h>

const unsigned Nclass(3u);
const int points(400);


//std::pair<TGraphAsymmErrors*,TGraphAsymmErrors*> mcroc(TString fname = "tmva_outfile.root") {
TGraphAsymmErrors* mcroc(TString fname = "tmva_outfile.root") {
  float scanpoint = -0.02;
  
  std::vector<TEfficiency*> sigeffs;
  std::vector<TEfficiency*> bkgeffs;
  std::vector<TChain*> trees;
  std::vector<TString> classifiers;
  std::vector<TString> filenames;
  classifiers.push_back("next3");
  filenames.push_back("myalg.root");
  classifiers.push_back("ghostprob");
  filenames.push_back("myalg.root");
  classifiers.push_back("1.-(tracks_TRACK_CHI2NDOF/3.)");
  filenames.push_back("myalg.root");

  //classifiers.push_back("25nsHLMLP");
  //classifiers.push_back("50nsLLMLP");
  //classifiers.push_back("50nsHLMLP");

  for (unsigned u = 0u ; u < Nclass ; ++u) {
    sigeffs.push_back( new TEfficiency(Form("sigeff%d",u),Form("sig eff %d",u),points,1,points+1));
    bkgeffs.push_back( new TEfficiency(Form("bkgeff%d",u),Form("bkg eff %d",u),points,1,points+1));
    trees.push_back(new TChain("tracks"));
    trees.back()->Add(filenames[u].Data());
  }

  //TString teston = "tracks_TRACK_Type==3";
  //TString teston = "1";//tracks_PP_TrackHistory==6&&tracks_TRACK_Type==3";
  //TString teston = "tracks_PP_TrackHistory==16&&tracks_TRACK_Type==3";
  TString teston = "tracks_TRACK_Type==3";
  //TString classifierB = "(1.-(tracks_TRACK_CHI2NDOF/3.))";//MLPTstatAndHistCats";

  TH1F* hist = new TH1F("hist","hist",100,-0.5,1.5);
  for (unsigned u = 0u ; u < Nclass ; ++u) {
    for (int i = 1 ; i <= points ; ++i) {
      float scanpoint = 0.00 + i * 0.0025;
      trees[u]->Draw("1>>hist",Form("%s&&tracks_assoc==1",teston.Data()));
      int all = hist->GetSum();
      trees[u]->Draw("1>>hist",Form("%s&&tracks_assoc==1&&%s>%f",teston.Data(),classifiers[u].Data(),scanpoint));
      int passed = hist->GetSum();
      int j = 0;
      sigeffs[u]->SetTotalEvents(i,all);
      sigeffs[u]->SetPassedEvents(i,passed);
    }
    for (int i = 1 ; i <= points ; ++i) {
      float scanpoint = 0.00 + i * 0.0025;
      trees[u]->Draw("1>>hist",Form("%s&&tracks_assoc==0",teston.Data()));
      int all = hist->GetSum();
      trees[u]->Draw("1>>hist",Form("%s&&tracks_assoc==0&&%s>%f",teston.Data(),classifiers[u].Data(),scanpoint));
      int passed = hist->GetSum();
      int j = 0;
      bkgeffs[u]->SetTotalEvents(i,all);
      bkgeffs[u]->SetPassedEvents(i,all-passed);
    }
  }
  float sigeffC[Nclass][points];
  float sigeffEl[Nclass][points];
  float sigeffEu[Nclass][points];
  float bkgeffC[Nclass][points];
  float bkgeffEl[Nclass][points];
  float bkgeffEu[Nclass][points];


  for (unsigned u = 0u ; u < Nclass ; u++) {
    for (int p = 0 ; p < points ; ++p) {
      sigeffC[u][p] = sigeffs[u]->GetEfficiency(p+1);
      sigeffEl[u][p]= sigeffs[u]->GetEfficiencyErrorLow(p+1);
      sigeffEu[u][p]= sigeffs[u]->GetEfficiencyErrorUp(p+1);
    }
    for (int p = 0 ; p < points ; ++p) {
      bkgeffC[u][p] = bkgeffs[u]->GetEfficiency(p+1);
      bkgeffEl[u][p]= bkgeffs[u]->GetEfficiencyErrorLow(p+1);
      bkgeffEu[u][p]= bkgeffs[u]->GetEfficiencyErrorUp(p+1);
    }
  }

  std::vector<TGraphAsymmErrors*> rocs;
  for (unsigned u = 0u ; u < Nclass ; ++u) {
    rocs.push_back(new TGraphAsymmErrors(points, sigeffC[u], bkgeffC[u], sigeffEl[u], sigeffEu[u], bkgeffEl[u], bkgeffEu[u]));
    rocs.back()->SetTitle(classifiers[u]);
  }

  rocs[0]->SetLineColor(kRed);
  rocs[0]->SetMarkerColor(kRed);

  rocs[1]->SetLineColor(kBlue);
  rocs[1]->SetMarkerColor(kBlue);

  rocs[2]->SetLineColor(kBlack);
  rocs[2]->SetMarkerColor(kBlack);

//  rocs[3]->SetLineColor(kMagenta);
//  rocs[3]->SetMarkerColor(kMagenta);
//
//  rocs[4]->SetLineColor(kGreen+2);
//  rocs[4]->SetMarkerColor(kGreen+2);
//
//  rocs[5]->SetLineColor(kCyan);
//  rocs[5]->SetMarkerColor(kCyan);

  TCanvas* can = new TCanvas();
  rocs[0]->Draw("AP");
  rocs[0]->SetMarkerStyle(kDot);
  rocs[0]->SetFillColor(kWhite);
  rocs[0]->SetFillStyle(0);
  for (unsigned u = 1u ; u < Nclass ; ++u) {
    rocs[u]->SetMarkerStyle(kDot);
    rocs[u]->SetFillColor(kWhite);
    rocs[u]->SetFillStyle(0);
    rocs[u]->Draw("P same");
  }

  can->BuildLegend()->SetFillColor(0);
  return rocs[0];
//  return std::pair<TGraphAsymmErrors*,TGraphAsymmErrors*>(rocA,rocB);
}
