/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Kernel/HitPattern.h"

#include "Event/FTRawCluster.h"
#include "Event/FTCluster.h"
#include "Event/Particle.h"
#include "Event/STCluster.h"
#include "Event/Track.h"
#include "Event/VPCluster.h"
#include "Event/KalmanFitResult.h"

#include "TMath.h"

// local
#include "TupleToolGhostInfo.h"
#include "Ghostbuster_MLP.C"
#include "FlattenLong.C"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolGhostInfo
//
// 2013-12-15 : Angelo Di Canto
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_COMPONENT( TupleToolGhostInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  TupleToolGhostInfo::TupleToolGhostInfo( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
    : TupleToolBase ( type, name , parent )
{
  declareInterface<IParticleTupleTool>(this);
}

//=============================================================================
StatusCode TupleToolGhostInfo::initialize()
{
  if( !TupleToolBase::initialize() ) return StatusCode::FAILURE;

  // track fitter
  m_trackFitter = tool<ITrackFitter>("TrackMasterFitter",this);

  // expect hit patterns
  m_vpExpectation = tool<IVPExpectation>("VPExpectation");
  m_utExpectation = tool<IHitExpectation>("UTHitExpectation");
  m_ftExpectation = tool<IHitExpectation>("FTHitExpectation");

  // mva classifier
  initNamesLong();
  m_readerLong = new ReadMLP(m_inNames);
  if ( !m_readerLong->IsStatusClean() )
    return Error("Could not initialize MVA reader for long tracks",StatusCode::FAILURE,1);

  m_FlattenTableLong = new FlattenLong();
  if ( !m_FlattenTableLong->valid() )
    return Error("Could not initialize flattening table for long tracks",StatusCode::FAILURE,1);

  m_inputVec = new std::vector<double>;

  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode TupleToolGhostInfo::fill( const LHCb::Particle*
                                     , const LHCb::Particle* P
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix = fullName(head);

  bool test = StatusCode::SUCCESS;
  if ( !P ) return StatusCode::FAILURE;

  //first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) return StatusCode::SUCCESS;

  const LHCb::ProtoParticle* protop = P->proto();
  if (!protop) return StatusCode::SUCCESS;

  const LHCb::Track* track = protop->track();
  if (!track) return StatusCode::SUCCESS;

  if( !track->checkType(LHCb::Track::Long) ) {
    test &= tuple->column( prefix+"_GhostbusterMLP", -999. );
    test &= tuple->column( prefix+"_GhostProb", -999. );
    return StatusCode(test);
  }

  // fill input variables
  const LHCb::VPClusters* veloCont = getIfExists<LHCb::VPClusters>(LHCb::VPClusterLocation::VPClusterLocation);
  const LHCb::STClusters* utCont = getIfExists<LHCb::STClusters>(LHCb::STClusterLocation::UTClusters);
  typedef FastClusterContainer<LHCb::FTRawCluster,int> FTRawClusters;
  FTRawClusters* ftCont = getIfExists<FTRawClusters>( LHCb::FTRawClusterLocation::Default );
  if (!ftCont) {
    FTRawClusters* tmp = getIfExists<FTRawClusters>("Raw/FT/RawClusters");//needed to create clusters in LHCb::FTRawClusterLocation::Default
    ftCont = getIfExists<FTRawClusters>( LHCb::FTRawClusterLocation::Default );
  }
  if ( !veloCont || !utCont || !ftCont) {
    test &= tuple->column( prefix+"_GhostbusterMLP", -999. );
    test &= tuple->column( prefix+"_GhostProb", -999. );
    return StatusCode(test);
  }
  int nClusters = ((int) veloCont->size()+utCont->size()+ftCont->size());

  double chi2prob = TMath::Prob(track->chi2(),track->nDoF());
  double Mchi2 = track->info(LHCb::Track::FitMatchChi2,-1.);
  double Vchi2 = track->info(LHCb::Track::FitVeloChi2,-1.);
  double Tchi2 = track->info(LHCb::Track::FitTChi2,-1.);
  int    Vndof = track->info(LHCb::Track::FitVeloNDoF,-1);
  int    Tndof = track->info(LHCb::Track::FitTNDoF,-1);
  if (Mchi2<0. || Vchi2<0. || Tchi2<0. || Vndof<0 || Tndof<0) {
    test &= tuple->column( prefix+"_GhostbusterMLP", -999. );
    test &= tuple->column( prefix+"_GhostProb", -999. );
    return StatusCode(test);
  }

  double pt = track->pt();
  double eta = track->pseudoRapidity();

  int observedVP(0), observedUT(0), observedFT(0);
  std::vector<LHCb::LHCbID> ids = track->lhcbIDs();
  for(std::vector<LHCb::LHCbID>::const_iterator id=ids.begin(); id!=ids.end(); ++id) {
    switch( id->detectorType() ) {
      case LHCb::LHCbID::VP: observedVP++; break;
      case LHCb::LHCbID::UT: observedUT++; break;
      case LHCb::LHCbID::FT: observedFT++; break;
    }
  }

  int expectedVP = ((int) m_vpExpectation->nExpected(*track));
  int expectedUT = ((int) m_utExpectation->nExpected(*track));
  int expectedFT = ((int) m_ftExpectation->nExpected(*track));

  LHCb::Track *refitted = const_cast<LHCb::Track*>(track);
  LHCb::KalmanFitResult* fit = static_cast<LHCb::KalmanFitResult*>( refitted->fitResult() );
  if (!fit) {
    m_trackFitter->fit(*refitted).ignore();
    fit = static_cast<LHCb::KalmanFitResult*>( refitted->fitResult() );
  }
  if (!fit) {
    test &= tuple->column( prefix+"_GhostbusterMLP", -999. );
    test &= tuple->column( prefix+"_GhostProb", -999. );
    return StatusCode(test);
  }
  int nUTOutliers = ((int) (fit->nMeasurements(LHCb::Measurement::UT) - fit->nActiveMeasurements(LHCb::Measurement::UT)));
  int nFTOutliers = ((int) (fit->nMeasurements(LHCb::Measurement::FT) - fit->nActiveMeasurements(LHCb::Measurement::FT)));

  m_inputVec->clear();
  m_inputVec->reserve( m_inNames.size() );
  m_inputVec->push_back( pt );
  m_inputVec->push_back( eta );
  m_inputVec->push_back( nClusters );
  m_inputVec->push_back( observedVP );
  m_inputVec->push_back( observedUT );
  m_inputVec->push_back( observedFT );
  m_inputVec->push_back( observedVP-expectedVP );
  m_inputVec->push_back( observedUT-expectedUT );
  m_inputVec->push_back( observedFT-expectedFT );
  m_inputVec->push_back( chi2prob );
  m_inputVec->push_back( Mchi2 );
  m_inputVec->push_back( TMath::Prob(Vchi2,Vndof) );
  m_inputVec->push_back( TMath::Prob(Tchi2,Tndof) );
  m_inputVec->push_back( nUTOutliers );
  m_inputVec->push_back( nFTOutliers );

  // get mva discriminant
  double ghostprob = m_readerLong->GetMvaValue( *m_inputVec );
  test &= tuple->column( prefix+"_GhostbusterMLP", ghostprob );

  // flattening
  ghostprob = m_FlattenTableLong->value(ghostprob);
  test &= tuple->column( prefix+"_GhostProb", ghostprob );

  return StatusCode(test);
}

//=============================================================================
void TupleToolGhostInfo::initNamesLong()
{
  m_inNames.clear();

  m_inNames.push_back("pt");
  m_inNames.push_back("eta");
  m_inNames.push_back("nClusters");
  m_inNames.push_back("observedVP");
  m_inNames.push_back("observedUT");
  m_inNames.push_back("observedFT");
  m_inNames.push_back("excessVP");
  m_inNames.push_back("excessUT");
  m_inNames.push_back("excessFT");
  m_inNames.push_back("chi2prob");
  m_inNames.push_back("Mchi2");
  m_inNames.push_back("Vchi2prob");
  m_inNames.push_back("Tchi2prob");
  m_inNames.push_back("nUTOutliers");
  m_inNames.push_back("nFTOutliers");

  return;
}
