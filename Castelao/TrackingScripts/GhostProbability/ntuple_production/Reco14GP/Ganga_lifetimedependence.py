###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
bk_query=BKQuery(path='/MC/MC11a/Beam3500GeV-2011-MagUp-Nu2-EmNoCuts/Sim05/Trig0x40760037Flagged/Reco12a/Stripping17NoPrescalingFlagged/13264001')
ds=bk_query.getDataset()
      
import re
if not(re.match(".*Bender_v.*\/Phys.*",os.getcwd())) :
    print "Bender version not identified"
the_version=re.sub("\/Phys.*","",re.sub(".*Bender_","",os.getcwd()))
er = GaudiPython( project="Bender"
                  , version=the_version
                  , script = os.getcwd()+'/lifetime_dependence.py'
                           )   
  

j = Job(application = er)
j.name = "lifetime_ghostprob"

j.application.user_release_area = re.sub("\/Bender_v.*","",os.getcwd())
the_installarea = re.sub("Bender_v.*\/Phys.*","Bender_"+the_version+"/InstallArea/x86_64-slc5-gcc46-opt",os.getcwd())
j.inputdata = ds
j.splitter = SplitByFiles ( filesPerJob = 3 , maxFiles = 10)
j.merger = None

j.outputdata = ['zoontuple.root']


j.backend    = Dirac()
j.prepare()
j.submit()
