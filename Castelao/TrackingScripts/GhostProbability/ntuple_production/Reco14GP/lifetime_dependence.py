###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################
# steering, apply changes here

# NB: jump to "BOOKMARK" for further changes, which I don't dare to steere from up here
# for vim users: go with the curser on BOOKMARK and hit shift+3 (or whichever keybinding you have for the # sign) and go through other occurances with the "n" key

# NB: check https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/Tau23mu for which packages are needed to use this script

# these can be added in principle, but I don't want to blow up the ntuples with uninteresting stuff now.
stable_InputCollections = []
stable_DecayName = []
if False:
   stable_InputCollections = ["Phys/StdLooseAnyPions/Particles","Phys/StdLoosePions/Particles","Phys/StdAllNoPIDsPions/Particles","Phys/Anything/Particles"]
   stable_DecayName = ["StdLooseAnyPions","StdLoosePions","StdAllNoPIDsPions","StdAllNoPIDsAnyPions"]

unstable_InputCollections = ['/Event/AllStreams/Phys/B02DPiD2HHHBeauty2CharmLine/Particles']
unstable_DecayName = ["B02DPiD2HHHBeauty2CharmLine"]

NEVT = -1

# make sure your database tags are correct.
# any claim DaVinci would do this automatically correct is a lie!
from Configurables import CondDB, DaVinci
DaVinci().DataType = "2011"
DaVinci().DDDBtag = "MC11-20111102"
DaVinci().CondDBtag = "sim-20111111-vc-mu100"
DaVinci().Simulation = True

# steering, apply changes here
##############################





# FIXME: no PID on "Any" particles. fix for isMuon exists, but is not implemented here.
# EDIT: fixed?

# to be used with sim+std://MC/MC11a/Beam3500GeV-2011-MagDown-Nu2-EmNoCuts/Sim05/Trig0x40760037Flagged/Reco12a/Stripping17NoPrescalingFlagged/13264001/ALLSTREAMS.DST

































#######
# make track fitting work in DaVinci

import re,sys
print "USAGE:"
print "interactively"
print "    python " + sys.argv[0]
print "on the grid"
print "    sorry, get inspired by Phys/Tau23Mu/options/Ganga_TryBlind.py"
print ""
print "further information on how to use this script can be found on https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/Tau23mu"

from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()

if re.match(".*-2010.*",DaVinci().DDDBtag) :
   if not "2010"==DaVinci().DataType :
     print "I VERY MUCH DOUBT YOU HAVE THE CORRECT DB TAG"
     sys.exit()

# make track fitting work in DaVinci
#######

###############################################################
# recompute ANN output (might be desired for studying ANN ghost

from Configurables import ChargedProtoANNPIDConf,GaudiSequencer
nnpidseq = GaudiSequencer("ANNGPIDSeq")
annconf = ChargedProtoANNPIDConf()
annconf.DataType = "2012"
#annconf.NetworkVersions["2011"] = "MC10TuneV1"
#annconf.NetworkVersions["2010"] = "MC10TuneV1" 
annconf.RecoSequencer = nnpidseq

DaVinci().appendToMainSequence( [ nnpidseq ] )

# recompute ANN output (might be desired for studying ANN ghost
###############################################################

#########################
# copy and paste remanent

from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

# copy and paste remanent
#########################


####################################
# get particles from all track types
from Configurables import ChargedProtoANNPIDConf,GaudiSequencer,ChargedPP2MC,ChargedProtoParticleMaker,DelegatingTrackSelector,TrackAssociator,DataOnDemandSvc,TrackSelector,TrackToDST,NoPIDsParticleMaker,BackgroundCategory,P2MCPFromProtoP,ChargedProtoParticleAddMuonInfo,FilterDesktop


def makeMyProtoP(trackcont):
         #unpacker = UnpackTrack(trackcont+"UnpackTrack")
         #unpacker.InputName="pRec/Track/"+trackcont
         #unpacker.OutputName="Rec/Track/"+trackcont
         downprotoseq = GaudiSequencer(trackcont+"ProtoPSeq")
         downprotos = ChargedProtoParticleMaker(trackcont+"ProtoPMaker")
         downprotos.Inputs = ["Rec/Track/"+trackcont]
         downprotos.Output = "Rec/ProtoP/"+trackcont+"ProtoPMaker"
         downprotos.addTool( DelegatingTrackSelector, name="TrackSelector" )
         tracktypes = [ "Long","Upstream","Downstream","Ttrack","Velo","VeloR" ]
         downprotos.TrackSelector.TrackTypes = tracktypes
         selector = downprotos.TrackSelector

         for tsname in tracktypes:
                 selector.addTool(TrackSelector,name=tsname)
                 ts = getattr(selector,tsname)
                 # Set Cuts
                 ts.TrackTypes = [tsname]

         # add MuonPID
         addmuonpid = ChargedProtoParticleAddMuonInfo(trackcont+"addmuoninfo")
         addmuonpid.InputMuonPIDLocation = "Rec/Muon/MuonPID"
         addmuonpid.ProtoParticleLocation = downprotos.Output + "/ProtoParticles"

         # set up associators
         assoctr = TrackAssociator(trackcont+"AssocTr")
         assoctr.TracksInContainer = "Rec/Track/"+trackcont
         assocpp=ChargedPP2MC(trackcont+"AssocPP")
         assocpp.TrackLocations = [ "Rec/Track/"+trackcont ]
         assocpp.InputData = [ "Rec/ProtoP/"+trackcont+"ProtoPMaker" ]
         assocpp.OutputTable = "Relations/Rec/ProtoP/"+trackcont+"ProtoPMaker"
         # DST post treatment
         TrackToDST(trackcont+"TrackToDST").TracksInContainer = "Rec/Track/"+trackcont
         if ( DaVinci().Simulation ) :
                 downprotoseq.Members += [ TrackToDST(trackcont+"TrackToDST"), assoctr, downprotos, addmuonpid, assocpp ]
                 DataOnDemandSvc().AlgMap.update( {
                         #"/Event/Rec/Track/"+trackcont : unpacker.getFullName(),
                         "/Event/Rec/ProtoP/"+trackcont+"ProtoPMaker" : downprotoseq.getFullName(),
                         "/Event/Relations/Rec/ProtoP/"+trackcont+"ProtoPMaker" : downprotoseq.getFullName()
                         } )
         else :
                 downprotoseq.Members += [ TrackToDST(trackcont+"TrackToDST"), downprotos, addmuonpid ]
                 DataOnDemandSvc().AlgMap.update( {
                         #"/Event/Rec/Track/"+trackcont : unpacker.getFullName(),
                         "/Event/Rec/ProtoP/"+trackcont+"ProtoPMaker" : downprotoseq.getFullName()
                         } )
 
 
def makeMyParticles(name, trackcont, particle):
         # set up particle maker
         particleMaker =  NoPIDsParticleMaker(name , Particle = particle)
         particleMaker.Input = "Rec/ProtoP/"+trackcont+"ProtoPMaker"
         DataOnDemandSvc().AlgMap.update( {
                 "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
                 "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName() } )

makeMyProtoP("Best")
makeMyParticles("Anything","Best","pion")

from CommonParticles.Utils import *

algorithm2 = FilterDesktop( 'StdLooseAnyPions',
                           Inputs = ["Phys/Anything/Particles"],
                           Code = defaultCuts() )

## configure Data-On-Demand service 
locations = updateDoD ( algorithm2 )

# get particles from all track types
####################################



###################
# write your ntuple

from Configurables import ZooWriter, ProtoPInfo2ParticleInfo
z = ZooWriter("z")
# a little bit redundant
# trackinfo 101 = clone distance
# trackinfo  16 = match chi2
# trackinfo  21 = match chi2 in fit
z.ExtraInfoList = [ 5,16,17,18,19,20,21,101,10004,10005,10006,10007,10020,10021,10022 ]
z.ParticleInfoList = [ 10020,10021,10022,10030,10031,20000,20001,20002 ]
for i in [101,102,103,200,201,364,365]:
   z.ParticleInfoList += [10100+i]
z.addTool(BackgroundCategory("BackgroundCategory"))
z.BackgroundCategory.addTool(P2MCPFromProtoP("P2MCPFromProtoP"))
z.BackgroundCategory.P2MCPFromProtoP.Locations = [
                 "Relations/Rec/ProtoP/Charged",
                 "Relations/Rec/ProtoP/Upstream",
                 "Relations/Rec/ProtoP/Neutrals",
                 "Relations/Rec/ProtoP/BestProtoPMaker"]

z.InputCollections = stable_InputCollections
z.DecayName = stable_DecayName
z.InputCollections += unstable_InputCollections
z.DecayName += unstable_DecayName
z.WriteTrackInfo = True
z.WriteDLL = True
z.WriteOccupancies = True
z.WriteHitPattern = True
z.WriteExpectedHitPattern = True
#z.IntelligentPV = True
#z.WriteLHCbIDs = True
z.WriteMC = True
z.WriteMCtrees = True
#z.PackedStatesZList = [8000,15000]
#z.PackedStatesList = [221,-221]
z.MCList = [15,431]
z.Filename = "zoontuple.root"
ProtoPInfoList = [700,701,702,703,704,705]
for i in ProtoPInfoList:
   z.ParticleInfoList += [10100+i]
z.ParticleInfoList += [10100]
z.ParticleInfoList += [10100-1]

# write your ntuple
###################


#####################
# some DaVinci config (static part)

DaVinci().EvtMax = -1
#DaVinci().Lumi = True
DaVinci().InputType = 'DST'
 
# some DaVinci config
#####################

#############################
# putting the things together

from Configurables import GaudiSequencer

wrap = GaudiSequencer("wrapper")

# configure where Bender will take place
intermediate = GaudiSequencer("intermediate")
intermediate.ModeOR = True
intermediate.ShortCircuit = False
intermediate.IgnoreFilterPassed = True
intermediate2 = GaudiSequencer("intermediate2")
intermediate2.ModeOR = True
intermediate2.ShortCircuit = False
intermediate2.IgnoreFilterPassed = True

DaVinci().appendToMainSequence( [intermediate2,intermediate,wrap] )

# prepare all the particles which will be read by the zoowriter
pcs = []
from Configurables import TrackModifier
for l in range(len(z.InputCollections)):
   pc = ProtoPInfo2ParticleInfo("infos4"+z.DecayName[l])
   tr = TrackModifier("modifytracks"+z.DecayName[l])
   tr.Location = z.InputCollections[l]
   tr.Hack = False
   pc.Offset = 10100
   pc.Location = z.InputCollections[l]
   pc.Infos = ProtoPInfoList
   pcs+=[tr,pc]
intermediate2.Members+= pcs


# BOOKMARK
# if you run interactively, put your datacard here.
# if you run on the grid, leave it commented out.
# this card here is from the first MC 2012 incl JPsi -> mu mu production

#from Gaudi.Configuration import * 
#from GaudiConf import IOHelper
#IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000001_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000002_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000003_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000004_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000005_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000006_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000007_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000008_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000009_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000010_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000012_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000013_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000014_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000015_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000016_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000018_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000019_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000020_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000021_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000022_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000023_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000025_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000026_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000027_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000029_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000030_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000031_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000032_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000033_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000034_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000035_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000036_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000037_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000038_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000039_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000040_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000041_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000042_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000043_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000044_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000045_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000046_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000047_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000049_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000050_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000052_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000053_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000054_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000055_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000056_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000057_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000058_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000059_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000060_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000061_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000062_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000063_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000064_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000065_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000066_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000067_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000069_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000071_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000072_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000074_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000075_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000076_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000077_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000078_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000079_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000082_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000084_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000085_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000086_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000087_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000088_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000089_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000090_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000091_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000092_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000093_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000094_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000095_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000097_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000099_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000100_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000101_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000102_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000103_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000104_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000105_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000106_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000107_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000108_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000109_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000110_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000111_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000112_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000114_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000115_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000116_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000117_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000119_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000120_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000121_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000122_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000123_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000124_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000125_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000126_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000128_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000129_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000130_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000131_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000132_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000133_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000134_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000135_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000136_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000137_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000138_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000139_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000140_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000141_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000142_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000143_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000144_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000145_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000146_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000147_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000148_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000149_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000150_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000151_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000152_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000153_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000154_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000155_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000156_1.allstreams.dst',
#'LFN:/lhcb/MC/MC12/ALLSTREAMS.DST/00018435/0000/00018435_00000157_1.allstreams.dst'
#], clear=True)
#FileCatalog().Catalogs += [ 'xmlcatalog_file:newjpsi.xml' ]

    
#########################
# okay, start GaudiPython

from GaudiPython.Bindings import AppMgr
gaudi = AppMgr()

# okay, start GaudiPython
#########################

#######################################################
# I was told, this is needed to do tistossing correctly

TISTOS = gaudi.toolsvc().create("TriggerTisTos",interface="ITriggerTisTos")
TISTOS.setTOSFrac(3,0.005)

# I was told, this is needed to do tistossing correctly
#######################################################



########################################################################
# prepare all the particles which will be read by the zoowriter (part 2)

from Tau23Mu.myhacktrack import myhacktrack
manipulator = gaudi.algorithm('GaudiSequencer/intermediate',True)
for i in range(len(stable_InputCollections)) :
   manipulator.Members+=[myhacktrack('manipulate'+str(i),Inputs = [stable_InputCollections[i]],LookIn = stable_InputCollections[i],Nprong =3).name()]

# prepare all the particles which will be read by the zoowriter (part 2)
########################################################################

###########
# okay, run

gaudi.run(1)
# do you need to make debuging in the first event?
# print gaudi.evtsvc()['Strip/Phys/DecReports']

gaudi.run(NEVT)
gaudi.stop()
gaudi.finalize()

# okay, run
###########
