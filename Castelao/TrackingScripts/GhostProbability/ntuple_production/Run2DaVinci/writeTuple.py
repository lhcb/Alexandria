###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from Configurables import DaVinci

DaVinci().InputType = "DST"

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False

from Configurables import LHCbApp

LHCbApp().DDDBtag   = "dddb-20140729"
LHCbApp().CondDBtag = "sim-20140730-vc-md100"
#dddb-20140729 Condition DB: sim-20140730-vc-md100

DaVinci().DataType  = "2012"



from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()
from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
from Configurables import TupleToolGhostInfo, TrackMasterFitter, TupleToolProtoPData, TupleToolTrackInfo
from Configurables import Run2GhostId, TrackNNGhostId
from DecayTreeTuple.Configuration import *
myToolList = [  "TupleToolMCTruth", "TupleToolKinematic"]

alg = DecayTreeTuple("TestGhostNtuple")
alg.Decay = "[pi+]cc"
alg.TupleName = "Tuple"
alg.Inputs = [ "Phys/StdLooseAnyPions/Particles" ]




alg.addBranches({
  "tracks" : "^([pi+]cc)"
  })
oldghostinfo = alg.tracks.addTupleTool('TupleToolGhostInfo/oldghostinfo')
oldghostinfo.ExtraName = "OldImplementation"
oldghostinfo.addTool(Run2GhostId("Run2GhostId"),name="Run2GhostId")
#oldghostinfo.addTool(TrackNNGhostId("TrackNNGhostId"),name="TrackNNGhostId")

from Configurables import TrackInitFit, TrackMasterFitter
from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
from Configurables import TrackBestTrackCreator, TrackMasterFitter, TrackStateInitTool, FastVeloFitLHCbIDs

def giveitafit(thething):

   thething.addTool(TrackInitFit,"TrackInitFit")
   thething.TrackInitFit.addTool( TrackMasterFitter, name = "Fit" )
   thething.TrackInitFit.addTool( TrackStateInitTool, name = "Init")
   thething.TrackInitFit.Init.UseFastMomentumEstimate = True
   # for 25ns
   ConfiguredMasterFitter( getattr(thething.TrackInitFit, "Fit"), SimplifiedGeometry = True, LiteClusters = True , MSRossiAndGreisen=  True)

   # for 50ns
   #ConfiguredMasterFitter( getattr(thething.TrackInitFit, "Fit"), SimplifiedGeometry = True, LiteClusters = True )
   thething.TrackInitFit.Init.VeloFitterName = "FastVeloFitLHCbIDs"
   thething.TrackInitFit.Init.addTool(FastVeloFitLHCbIDs, name = "FastVeloFitLHCbIDs")
   if DaVinci().InputType == "MDST" or True:
      thething.TrackInitFit.RootInTES = MyStream
      thething.TrackInitFit.Fit.RootInTES = MyStream
      thething.TrackInitFit.Init.RootInTES = MyStream
      thething.TrackInitFit.Init.FastVeloFitLHCbIDs.RootInTES = MyStream


giveitafit(oldghostinfo)

for tool in myToolList:
   alg.tracks.addTupleTool(tool)

infotool = alg.tracks.addTupleTool("TupleToolTrackInfo/infotool")
infotool.Verbose=True
infotool.DataList = [ "FitVeloChi2", "FitVeloNDoF", "FitTChi2", "FitTNDoF", "FitMatchChi2", "NCandCommonHits", "Cand1stQPat", "Cand2ndQPat", "Cand1stChi2Mat", "Cand2ndChi2Mat" ]
historytool = alg.tracks.addTupleTool("TupleToolProtoPData/historytool")
historytool.DataList = [ "TrackHistory" ]


from Configurables import DaVinci
DaVinci().UserAlgorithms = [ alg ]

DaVinci().Simulation = True
DaVinci().TupleFile = "TestGhostNtuple.root"
DaVinci().EvtMax = -1
DaVinci().Lumi = False

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"






####################################
# get particles from all track types
from Configurables import ChargedProtoANNPIDConf,GaudiSequencer,ChargedPP2MC,ChargedProtoParticleMaker,DelegatingTrackSelector,TrackAssociator,DataOnDemandSvc,TrackSelector,TrackToDST,NoPIDsParticleMaker,BackgroundCategory,P2MCPFromProtoP,ChargedProtoParticleAddMuonInfo,FilterDesktop

def makeMyProtoP(trackcont):
         downprotoseq = GaudiSequencer(trackcont+"ProtoPSeq")
         downprotos = ChargedProtoParticleMaker(trackcont+"ProtoPMaker")
         downprotos.Inputs = ["Rec/Track/"+trackcont]
         downprotos.Output = "Rec/ProtoP/"+trackcont+"ProtoPMaker"
         downprotos.addTool( DelegatingTrackSelector, name="TrackSelector" )
         tracktypes = [ "Long","Upstream","Downstream","Ttrack","Velo","VeloR" ]
         downprotos.TrackSelector.TrackTypes = tracktypes
         selector = downprotos.TrackSelector

         for tsname in tracktypes:
                 selector.addTool(TrackSelector,name=tsname)
                 ts = getattr(selector,tsname)
                 # Set Cuts
                 ts.TrackTypes = [tsname]

         # add MuonPID
         addmuonpid = ChargedProtoParticleAddMuonInfo(trackcont+"addmuoninfo")
         addmuonpid.InputMuonPIDLocation = "Rec/Muon/MuonPID"
         addmuonpid.ProtoParticleLocation = downprotos.Output + "/ProtoParticles"

         # set up associators
         assoctr = TrackAssociator(trackcont+"AssocTr")
         assoctr.TracksInContainer = "Rec/Track/"+trackcont
         assocpp=ChargedPP2MC(trackcont+"AssocPP")
         assocpp.TrackLocations = [ "Rec/Track/"+trackcont ]
         assocpp.InputData = [ "Rec/ProtoP/"+trackcont+"ProtoPMaker" ]
         assocpp.OutputTable = "Relations/Rec/ProtoP/"+trackcont+"ProtoPMaker"
         # DST post treatment
         TrackToDST(trackcont+"TrackToDST").TracksInContainer = "Rec/Track/"+trackcont
         if ( DaVinci().Simulation ) :
                 downprotoseq.Members += [ TrackToDST(trackcont+"TrackToDST"),
                                           #assoctr,
                                           downprotos,
                                           addmuonpid,
                                           #assocpp
                                           ]
                 DataOnDemandSvc().AlgMap.update( {
                         #"/Event/Rec/Track/"+trackcont : unpacker.getFullName(),
                         "/Event/Rec/ProtoP/"+trackcont+"ProtoPMaker" : downprotoseq.getFullName(),
                         "/Event/Relations/Rec/ProtoP/"+trackcont+"ProtoPMaker" : downprotoseq.getFullName()
                         } )
         else :
                 downprotoseq.Members += [ TrackToDST(trackcont+"TrackToDST"), downprotos, addmuonpid ]
                 DataOnDemandSvc().AlgMap.update( {
                         #"/Event/Rec/Track/"+trackcont : unpacker.getFullName(),
                         "/Event/Rec/ProtoP/"+trackcont+"ProtoPMaker" : downprotoseq.getFullName()
                         } )
 
 
def makeMyParticles(name, trackcont, particle):
         # set up particle maker
         particleMaker =  NoPIDsParticleMaker(name , Particle = particle)
         particleMaker.Input = "Rec/ProtoP/"+trackcont+"ProtoPMaker"
         DataOnDemandSvc().AlgMap.update( {
                 "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
                 "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName() } )

makeMyProtoP("Best")
makeMyParticles("Anything","Best","pion")
from CommonParticles.Utils import *

algorithm2 = FilterDesktop( 'StdLooseAnyPions',
                           Inputs = ["Phys/Anything/Particles"],
                           Code = defaultCuts() )

## configure Data-On-Demand service 
locations = updateDoD ( algorithm2 )

# get particles from all track types
####################################

