###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
########################################################################
# This is a (sketch) for a file to run the algorithms to get tracking efficiency numbers on MC.
# Note that this will not run out of the box, may be coded in style which does not follow any reasonable conventions, and may be outdated.
# The correct way to get the numbers is by using the stripping lines, this only serves to check certain things and to explain the principle.
#########################################################################

from os import environ
import GaudiKernel.SystemOfUnits as Units 
from Gaudi.Configuration import *
from Configurables import (FilterDesktop,
                           GaudiSequencer,
                           DecayTreeTuple,
                           CombineParticles,
                           EventSelector,
                           TupleToolTrigger,
                           TupleToolMCTruth,
                           TupleToolGeometry,
                           TupleToolKinematic,
                           TupleToolPropertime,
                           TupleToolPrimaries,
                           TupleToolEventInfo,
                           TupleToolPid,
                           TupleToolTrackInfo,
                           TupleToolVtxIsoln,
                           TupleToolTagging,
                           TupleToolDecay,
                           TupleToolTISTOS,
                           TupleToolP2VV,
                           LoKi__Hybrid__TupleTool,
                           LoKi__Hybrid__MCTupleTool,
                           DaVinci,
                           L0MuonAlg,
                           ChargedProtoParticleMaker,
                           NoPIDsParticleMaker,
                           TrackSelector,
                           MuonNNetRec,
                           EventTuple,
                           OfflineVertexFitter,
                           MomentumCombiner,
                           ParticleTransporter,
                           MessageSvc,
                           TrackMasterFitter,
                           MessageSvc,
                           PrintDecayTree,
                           PatAddTTCoord,
                           MuonCombRec,
                           PatLHCbID2MCParticle,
                           UnpackMCParticle,
                           PatTrack2MCParticle,
                           TupleToolTISTOS,
                           MuonTTTrack,
                           TrackMasterExtrapolator,
                           SelDSTWriter,
                           InputCopyStream,
                           FilterDesktop,
                           ProtoParticleMUONFilter,
                           TisTosParticleTagger,
                           TriggerTisTos,
                           L0DecReportsMaker,
                           L0SelReportsMaker,
                           MuonTrackMomRec
                           )
from Configurables import LoKi__Hybrid__PlotTool as PlotTool 
from Configurables import LoKi__Hybrid__FilterCriterion as LoKiFilterCriterion
from DecayTreeTuple.Configuration import *
from PhysSelPython.Wrappers import (SelectionSequence, Selection, DataOnDemand)

#
LoKiTool = LoKi__Hybrid__TupleTool( 'LoKiTool' )
MCLoKiTool = LoKi__Hybrid__MCTupleTool( 'MCLoKiTool' )
########################################################################

########################################################################
DaVinci().EvtMax = 5000
DaVinci().DataType = "2010"
DaVinci().Simulation = True
DaVinci().TupleFile = "TrackEffTest.root"
########################################################################
GetDaMuonTrack = MuonTTTrack("GetDaMuonTrack")
GetDaMuonTrack.AddTTHits = True
GetDaMuonTrack.FillMuonStubInfo = False
GetDaMuonTrack.ToolName = "MuonCombRec"
GetDaMuonTrack.OutputLevel = 4
GetDaMuonTrack.MC = False
GetDaMuonTrack.addTool( MuonCombRec )
GetDaMuonTrack.MuonCombRec.ClusterTool = "MuonClusterRec" # to enable: "MuonClusterRec"
GetDaMuonTrack.MuonCombRec.CloneKiller = False
GetDaMuonTrack.MuonCombRec.StrongCloneKiller = False
GetDaMuonTrack.MuonCombRec.SeedStation = 2 # try setting a different seed station
GetDaMuonTrack.MuonCombRec.DecodingTool = "MuonHitDecode"
GetDaMuonTrack.MuonCombRec.PadRecTool = "MuonPadRec"
GetDaMuonTrack.MuonCombRec.AssumePhysics = True
GetDaMuonTrack.addTool( PatAddTTCoord )
GetDaMuonTrack.PatAddTTCoord.YTolSlope = 400000.0
GetDaMuonTrack.PatAddTTCoord.XTol = 25.0 # was 12.0
GetDaMuonTrack.PatAddTTCoord.XTolSlope = 400000.0
GetDaMuonTrack.PatAddTTCoord.MaxChi2Tol = 7.0 # was not included
GetDaMuonTrack.PatAddTTCoord.MinAxProj = 5.5 # was 2.5
GetDaMuonTrack.PatAddTTCoord.MajAxProj = 25.0 # was 22.0
GetDaMuonTrack.addTool( TrackMasterFitter )
GetDaMuonTrack.TrackMasterFitter.OutputLevel = 4
GetDaMuonTrack.TrackMasterFitter.MaterialLocator = "SimplifiedMaterialLocator"
GetDaMuonTrack.addTool( TrackMasterExtrapolator )
GetDaMuonTrack.TrackMasterExtrapolator.MaterialLocator = "SimplifiedMaterialLocator"
GetDaMuonTrack.Output = "Rec/Track/MuonTTTracks"
###################################################################################
MuonTTPParts = ChargedProtoParticleMaker("MuonTTPParts")
MuonTTPParts.addTool( TrackSelector )
MuonTTPParts.TrackSelector.TrackTypes = [ "Long" ]
MuonTTPParts.Inputs = ["Rec/Track/MuonTTTracks"]
MuonTTPParts.Output = "Rec/ProtoP/MuonTTProtoP"
MuonTTPParts.OutputLevel = 4
##################################################################################
MuonTTParts = NoPIDsParticleMaker("MuonTTParts")
MuonTTParts.Particle = 'muon'
MuonTTParts.addTool( TrackSelector )
MuonTTParts.TrackSelector.TrackTypes = [ "Long" ]
MuonTTParts.Input =  "Rec/ProtoP/MuonTTProtoP"
MuonTTParts.OutputLevel = 4
##################################################################################
plusFilter1 = FilterDesktop("plusFilter1")
minusFilter1 = FilterDesktop("minusFilter1")
plusFilter1.Inputs = [ "/Event/Phys/StdAllLooseMuons/Particles" ] # Long
plusFilter1.Code = "(Q > 0)"
minusFilter1.Inputs = [ "Phys/MuonTTParts/Particles" ]  # MuonTT
minusFilter1.Code = "(Q < 0)"
minusFilter1.OutputLevel = 4
plusFilter1.OutputLevel = 4
##################################################################################
plusFilter2 = FilterDesktop("plusFilter2")
minusFilter2 = FilterDesktop("minusFilter2")
plusFilter2.Inputs = [ "Phys/MuonTTParts/Particles" ] # MuonTT
plusFilter2.Code = "(Q > 0)"
minusFilter2.Inputs = [ "/Event/Phys/StdAllLooseMuons/Particles" ]# Long
minusFilter2.Code = "(Q < 0)"
##################################################################################
Jpsi1 = CombineParticles("Jpsi1")
Jpsi1.DecayDescriptor = "J/psi(1S) -> mu+ mu-"
Jpsi1.Inputs = [ "Phys/minusFilter1/Particles","Phys/plusFilter1/Particles" ]
Jpsi1.DaughtersCuts = {"mu+"	:	"(PT > 1000*MeV)  & (ISMUON) & (PIDmu > -2)",
                    "mu-"	:	"PT > 0*MeV"}
Jpsi1.MotherCut = "(ADMASS('J/psi(1S)') < 500*MeV)"
###################################################################################
Jpsi2 = CombineParticles("Jpsi2")
Jpsi2.DecayDescriptor = "J/psi(1S) -> mu+ mu-"
Jpsi2.Inputs = [ "Phys/minusFilter2/Particles","Phys/plusFilter2/Particles" ]
Jpsi2.DaughtersCuts = {"mu-"	:	"(PT > 1000*MeV)  & (ISMUON) & (PIDmu > -2)",
                    "mu+"	:	"PT > 0*MeV"}
Jpsi2.MotherCut = "(ADMASS('J/psi(1S)') < 500*MeV)"
###################################################################################
JpsiLongLong = CombineParticles("JpsiLongLong")
JpsiLongLong.DecayDescriptor = "J/psi(1S) -> mu+ mu-"
JpsiLongLong.Inputs = [ "Phys/StdAllLooseMuons/Particles" ]
JpsiLongLong.DaughtersCuts = {"mu-"	:	"(PT > 100*MeV) & (ISMUON) & mcMatch( 'J/psi(1S) ==> ^mu+ ^mu-')",
                              "mu+"	:	"(PT > 100*MeV) & (ISMUON) & mcMatch( 'J/psi(1S) ==> ^mu+ ^mu-')"}
JpsiLongLong.MotherCut = "(ADMASS('J/psi(1S)') < 100*MeV)"
JpsiLongLong.Preambulo = [ 
    "from LoKiPhysMC.decorators import *" , 
    "from LoKiPhysMC.functions import mcMatch"
    ]
###################################################################################
Jpsi1Tuple = DecayTreeTuple("Jpsi1Tuple")
Jpsi1Tuple.Inputs = [ "Phys/Jpsi1/Particles" ] 
Jpsi1Tuple.Decay = "J/psi(1S) -> ^mu- ^mu+"
Jpsi1Tuple.OutputLevel = 4
####################################################################################
Jpsi1Tuple.ToolList = [
"LoKi::Hybrid::TupleTool/LoKiTool"
,"TupleToolGeometry"
,"TupleToolKinematic"
,"TupleToolPid"
#,"TupleToolTrackEff"
,"TupleToolTrackInfo"
,"TupleToolTISTOS"
,"TupleToolRecoStats"
,"TupleToolPropertime"
,"TupleToolEventInfo"
,"TupleToolMCTruth"
]

Jpsi1Tuple.addTool(LoKiTool, name = 'LoKiTool')
Jpsi1Tuple.LoKiTool.Variables = {
    "eta" : "ETA",
    "phi" : "PHI"
    }

Jpsi1Tuple.addTupleTool( "TupleToolTrackEff/TupleToolTrackEff" )
Jpsi1Tuple.TupleToolTrackEff.MuonAssocFraction = 0.7
Jpsi1Tuple.TupleToolTrackEff.TTAssocFraction = 0.6
Jpsi1Tuple.TupleToolTrackEff.InverseMatch = False
Jpsi1Tuple.TupleToolTrackEff.Verbose = False
Jpsi1Tuple.TupleToolTrackEff.MC = False

Jpsi1Tuple.addTool(TupleToolMCTruth)
Jpsi1Tuple.TupleToolMCTruth.ToolList =  [
    "MCTupleToolKinematic",                                                                                           
    "MCTupleToolHierarchy" 
    ]
####################################################################################
Jpsi2Tuple = DecayTreeTuple("Jpsi2Tuple")
Jpsi2Tuple.Inputs = [ "Phys/Jpsi2/Particles" ] 
Jpsi2Tuple.Decay = "J/psi(1S) -> ^mu- ^mu+"
Jpsi2Tuple.OutputLevel = 4
Jpsi2Tuple.ToolList = Jpsi1Tuple.ToolList

Jpsi2Tuple.addTool(LoKiTool, name = 'LoKiTool')
Jpsi2Tuple.LoKiTool.Variables = {
    "eta" : "ETA",
    "phi" : "PHI"
    }
Jpsi2Tuple.addTupleTool( "TupleToolTrackEff/TupleToolTrackEff" )
Jpsi2Tuple.TupleToolTrackEff.MuonAssocFraction = 0.7
Jpsi2Tuple.TupleToolTrackEff.TTAssocFraction = 0.6
Jpsi2Tuple.TupleToolTrackEff.InverseMatch = False
Jpsi2Tuple.TupleToolTrackEff.Verbose = False
Jpsi2Tuple.TupleToolTrackEff.MC = False

Jpsi2Tuple.addTool(TupleToolMCTruth)
Jpsi2Tuple.TupleToolMCTruth.ToolList =  [
    "MCTupleToolKinematic",                                                                                           
    "MCTupleToolHierarchy" 
    ]
####################################################################################
JpsiLongLongTuple = DecayTreeTuple("JpsiLongLongTuple")
JpsiLongLongTuple.Inputs = [ "Phys/JpsiLongLong/Particles" ] 
JpsiLongLongTuple.Decay = "J/psi(1S) -> ^mu- ^mu+"
JpsiLongLongTuple.OutputLevel = 4
JpsiLongLongTuple.ToolList = Jpsi1Tuple.ToolList

JpsiLongLongTuple.addTool(LoKiTool, name = 'LoKiTool')
JpsiLongLongTuple.LoKiTool.Variables = {
    "eta" : "ETA",
    "phi" : "PHI"
    }

####################################################################################
from Configurables import MCDecayTreeTuple
MCTuple = MCDecayTreeTuple("MCTuple")
MCTuple.Decay = "J/psi(1S) -> ^mu+ ^mu-"
MCTuple.ToolList = [    
    "LoKi::Hybrid::MCTupleTool/MCLoKiTool",
    "MCTupleToolKinematic",
    "MCTupleToolPrimaries",
    "MCTupleToolReconstructed"
    ]
MCTuple.addTool( MCLoKiTool, name = 'MCLoKiTool')
MCTuple.MCLoKiTool.Variables = {
    "eta" : "MCETA",
    "phi" : "MCPHI"
}
####################################################################################
# Add all the algorithms
Seq1 = GaudiSequencer("Seq1")
Seq2 = GaudiSequencer("Seq2")

#Seq1.Members += [ JpsiLongLong, plusFilter1, GetDaMuonTrack, MuonTTPParts, MuonTTParts, minusFilter1, Jpsi1, Jpsi1Tuple ]
#Seq2.Members += [ JpsiLongLong, minusFilter2, GetDaMuonTrack, MuonTTPParts, MuonTTParts, plusFilter2, Jpsi2, Jpsi2Tuple ]
Seq1.Members += [ plusFilter1, GetDaMuonTrack, MuonTTPParts, MuonTTParts, minusFilter1, Jpsi1, Jpsi1Tuple ]
Seq2.Members += [ minusFilter2, GetDaMuonTrack, MuonTTPParts, MuonTTParts, plusFilter2, Jpsi2, Jpsi2Tuple ]

######################################################################################

# DAVINCI MAIN OPTIONS !!!!!!!

######################################################################################
######################################################################################
######################################################################################
#MessageSvc().Format = "% F%100W%S%7W%R%T %0W%M"
DaVinci().MainOptions = ""
DaVinci().UserAlgorithms = [ Seq1, Seq2 ]

DaVinci().CondDBtag = "sim-20101210-vc-md100"
DaVinci().DDDBtag = "head-20101026"


######################################################################################
######################################################################################
######################################################################################

# Your input files
EventSelector().Input   = [
]
