There are two scripts which need to be used to calculate the tracking efficiency with the Long Method, given an nTuple with the TupleToolTrackEff:
- prepareTrackEffData.C
- trackEffFit.C

prepareTrackEffData condenses the dataset to the variables which are needed. In the case of MC, the MC is reweighted in the number of tracks, if a sample of collision data to reweight to is provided. Note that there is a flag "mc" which enables or disables this option, and a flag for producing some plots.

trackEffFit fits the data in momentum and pseudorapidity, number of tracks and number of primary vertices and stores the numerators and denominators in a root file. A weight is used to perform a weighted MLL fit for MC. For collision data, the weight is one (as set in prepareTrackEffData). A flag enables the plots of mass shapes, another the plots of efficiencies.

Note that for both scripts you need to provide the correct input of your tuples.
Both scripts can be compiled with ACLiC.

-- Michel De Cian, michel.de.cian@cern.ch, 23.1.2013 --
