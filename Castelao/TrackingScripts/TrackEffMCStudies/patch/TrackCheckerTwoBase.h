/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TrackCheckerTwoBase.h,v 1.9 2009-08-27 07:54:36 smenzeme Exp $
#ifndef TRACKCHECKERTWOBASE_H
#define TRACKCHECKERTWOBASE_H 1
 
// linkers
#include "Linker/LinkerTool.h"

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

#include <string>

// interfaces
#include "MCInterfaces/IMCReconstructible.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Kernel/ITrajPoca.h"
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "MCInterfaces/IIdealStateCreator.h"
#include "MCInterfaces/IVisPrimVertTool.h"
#include "MCInterfaces/ITrackGhostClassification.h"


#include "Event/Track.h"
#include "Event/MCParticle.h"

/** @class TrackCheckerTwoBase TrackCheckerTwoBase.h "TrackCheckers/TrackCheckerTwoBase"
 * 
 *  Base class for track monitoring: essentially a 'box' of common tools

 *  @author M. Needham.
 *  @date   7-5-2007
 */                 
                                                           
class TrackCheckerTwoBase : public GaudiTupleAlg {
                                                                             
 public:
                                                                             
  /** Standard construtor */
  TrackCheckerTwoBase( const std::string& name, ISvcLocator* pSvcLocator );
                                                                             
  /** Destructor */
  virtual ~TrackCheckerTwoBase();

  /** Algorithm initialization */
  virtual StatusCode initialize();

  /** Algorithm execute */
  virtual StatusCode execute();

  /** Algorithm finalize */
  virtual StatusCode finalize();

  /** Input track container location 
  *  @return location
  */
  const std::string& inputContainerB() const; 
  const std::string& inputContainerA() const; 


 public:
   
  /** Get a pointer to Magnetic field service
  *  @return field service
  */
  IMagneticFieldSvc* fieldSvc() const;
 
  /** Get a pointer to the poca tool
  *  @return poca tool
  */
  ITrajPoca* pocaTool() const;

  /** Get a pointer to the track selection tool
  *  @return field service
  */
  IMCReconstructible* selector() const;

  /** Get a pointer to the idealStateCreator
  *  @return IdealStateCreator
  */
  IIdealStateCreator* idealStateCreator() const;

  /** Get a pointer to the track extrapolator
  *  @return extrapolator
  */
  ITrackExtrapolator* extrapolator() const;

  
  typedef LinkerTool<LHCb::Track, LHCb::MCParticle> AsctTool;
  typedef AsctTool::InverseType InverseTable;
  typedef InverseTable::Range InverseRange;
  typedef InverseTable::iterator inverse_iterator;

  typedef AsctTool::DirectType DirectTable;
  typedef DirectTable::Range DirectRange;
  typedef DirectTable::iterator direct_iterator;

  /** Direct association table track to particle */
  DirectTable* directTableA() const;
  DirectTable* directTableB() const;

  /** Inverse association table particle to track */
  InverseTable* inverseTableB() const;
  InverseTable* inverseTableA() const;

  /** initialize per event */
  StatusCode initializeEvent();

  /** small struct for link info */
  struct LinkInfo{
    const LHCb::Track* track;
    unsigned int clone;
    double purity;
  };

  /** link to truth 
  * @param  aTrack track
  * @return linked particle 
  */
  const LHCb::MCParticle* mcTruth(const LHCb::Track* aTrack) const;

  /** What tracks were reconstructed from this particle
  * @param particle
  * @return track 
  */
  LinkInfo reconstructed(const LHCb::MCParticle* particle) const;
  LinkInfo reconstructedInRefContainer(const LHCb::MCParticle* particle) const;

  LinkInfo reconstructedA(const LHCb::MCParticle* particle) const;
  LinkInfo reconstructedB(const LHCb::MCParticle* particle) const;

  /** What tracks were reconstructed from this particle
  * @param particle
  * @return rec tracks 
  */
  TrackCheckerTwoBase::InverseRange reconstructedTracksA(const LHCb::MCParticle* particle) const;
  TrackCheckerTwoBase::InverseRange reconstructedTracksB(const LHCb::MCParticle* particle) const;

  /** Selected as type
  *
  * @return bool
  */
  bool selected(const LHCb::MCParticle* particle) const;

  /** Whether to split by algorithm
  *  @return splitByAlgorithm true or false
  */ 
  bool splitByAlgorithm() const;

  /** Whether to split by algorithm
  *  @return splitByType true or false
  */ 
  bool splitByType() const { return m_splitByType ; }

  /** To avoid hard coding...
  *  @return string all
  */ 
  const std::string& all() const;

  /** Pointer to the visible primary vertex tool
  *  @return IVisPrimVertTool
  */
  IVisPrimVertTool* visPrimVertTool() const;

  /** Pointer to ghost classification tool
  *  @return ITrackGhostClassification
  */
  ITrackGhostClassification* ghostClassification() const;

  /** Is a b child ? ie has b quark somewhere in history
  * @param  mcPart MC particle
  * @return bool true/false 
  */ 
  bool bAncestor(const LHCb::MCParticle* mcPart) const;

  /** Is a lambda/ks 
  * @param  mcPart MC particle
  * @return bool true/false 
  */
  bool ksLambdaAncestor(const LHCb::MCParticle* mcPart) const;


  /** are all stable daughters of this particle reconstructible?
   * @param  mcPart MC particle
   * @return bool true/false
   */

  bool allDaughtersReconstructible(const LHCb::MCParticle* mcPart) const;

  bool bAncestorWithReconstructibleDaughters(const LHCb::MCParticle* mcPart) const;
  
  const std::string& tracksInContainerB() { return m_tracksInContainerB ; }
  const std::string& tracksInContainerA() { return m_tracksInContainerA ; }


 protected:

  std::string m_tracksRefContainerA;   ///< Reference Tracks container location
  std::string m_tracksRefContainerB;   ///< Reference Tracks container location
 
 private:

  std::string m_extrapolatorName; 
  std::string m_selectorName;
 protected:
  std::string m_tracksInContainerA;    ///< Input Tracks container location 
  std::string m_tracksInContainerB;    ///< Input Tracks container location 
 private:
  std::string m_linkerInTableA;
  std::string m_linkerInTableB;
 
  ITrajPoca*         m_poca;          ///<  Pointer to the ITrajPoca interface  
  IMagneticFieldSvc* m_pIMF;          ///<  Pointer to the magn. field service

  std::string m_selectionCriteria;
  IMCReconstructible::RecCategory m_recCat; ///<  Pointer to selector 
  IMCReconstructible* m_selector;           ///<  Pointer to selector   

  ITrackExtrapolator* m_extrapolator; ///<  Pointer to extrapolator
  IIdealStateCreator* m_stateCreator; ///<  IdealStateCreator
  IVisPrimVertTool* m_visPrimVertTool; ///< Visible primary vertices..

  std::string m_ghostToolName;
  ITrackGhostClassification* m_ghostClassification;  ///< Pointer to ghost tool 

  AsctTool m_associatorA;
  DirectTable* m_directTableA;
  InverseTable* m_inverseTableA;
  AsctTool m_associatorB;
  DirectTable* m_directTableB;
  InverseTable* m_inverseTableB;


  AsctTool m_associator_refContainerA;
  DirectTable* m_directTable_refContainerA;
  AsctTool m_associator_refContainerB;
  DirectTable* m_directTable_refContainerB;
  InverseTable* m_inverseTable_refContainerB;
  InverseTable* m_inverseTable_refContainerA;


  bool m_splitByAlgorithm;
  bool m_splitByType;

  std::string m_all;                                                                       

protected:

  double m_chicut;
  bool   m_kl;


};

inline IMagneticFieldSvc* TrackCheckerTwoBase::fieldSvc() const{
  return m_pIMF;
}

inline ITrajPoca* TrackCheckerTwoBase::pocaTool() const{
  return m_poca;
}

inline IMCReconstructible* TrackCheckerTwoBase::selector() const{
  return m_selector;
}

inline bool TrackCheckerTwoBase::selected(const LHCb::MCParticle* particle) const{
  return selector()->isReconstructibleAs(m_recCat, particle);
}

inline ITrackExtrapolator* TrackCheckerTwoBase::extrapolator() const{
  return m_extrapolator;
}

inline IIdealStateCreator* TrackCheckerTwoBase::idealStateCreator() const{
  return m_stateCreator;
}

inline IVisPrimVertTool* TrackCheckerTwoBase::visPrimVertTool() const{
  return m_visPrimVertTool;
}

inline const std::string& TrackCheckerTwoBase::inputContainerB() const{
  return m_tracksInContainerB;
}
inline const std::string& TrackCheckerTwoBase::inputContainerA() const{
  return m_tracksInContainerA;
}

inline TrackCheckerTwoBase::DirectTable* TrackCheckerTwoBase::directTableA() const{
  return m_directTableA;
} 

inline TrackCheckerTwoBase::DirectTable* TrackCheckerTwoBase::directTableB() const{
  return m_directTableB;
} 
inline TrackCheckerTwoBase::InverseTable* TrackCheckerTwoBase::inverseTableA() const{
  return m_inverseTableA;
} 
inline TrackCheckerTwoBase::InverseTable* TrackCheckerTwoBase::inverseTableB() const{
  return m_inverseTableB;
} 


inline bool TrackCheckerTwoBase::splitByAlgorithm() const{
  return m_splitByAlgorithm;
}

inline const std::string& TrackCheckerTwoBase::all() const{
  return m_all;
}

inline ITrackGhostClassification* TrackCheckerTwoBase::ghostClassification() const {
  return m_ghostClassification;
}


#endif // TRACKCHECKERBASE_H
