/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#include "TrackOverlapCreator.h"

#include <boost/foreach.hpp>


//-----------------------------------------------------------------------------
// Implementation file for class : TrackOverlapCreator
//
// 2012-06-04 : Paul Seyfert <pseyfert@cern.ch>
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( TrackOverlapCreator );


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackOverlapCreator::TrackOverlapCreator(const std::string& name, ISvcLocator* pSvcLocator)
  : GaudiAlgorithm(name, pSvcLocator)
{
  ;
}

//=============================================================================
// Destructor
//=============================================================================
TrackOverlapCreator::~TrackOverlapCreator() {;}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackOverlapCreator::initialize()
{
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if (sc.isFailure()) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;


  return StatusCode::SUCCESS;
}


/**
* @brief do two tracks use perfectly the same hits?
*
* @param one a track
* @param two another track
*
* @return true if the hit content of both tracks is perfectly identical
*/
bool TrackOverlapCreator::samehits(LHCb::Track* one, LHCb::Track* two) {

  if (one == two) return true;

  unsigned fromids = 0;
  unsigned toids = 0;

  BOOST_FOREACH(const LHCb::LHCbID id, one->lhcbIDs()) {
    if (id.isVelo() || id.isST() || id.isOT()) ++fromids;
  }
  BOOST_FOREACH(const LHCb::LHCbID id, two->lhcbIDs()) {
    if (id.isVelo() || id.isST() || id.isOT()) ++toids;
  }


  if (fromids > toids)
    std::swap(one, two);

  unsigned n_total = (fromids<toids)?fromids:toids;//one->nLHCbIDs();
  unsigned n_common = 0;

  BOOST_FOREACH(const LHCb::LHCbID id, one->lhcbIDs()) {
    // count only hits in "standard tracking detectors"
    if ((!id.isVelo()) && (!id.isST()) && (!id.isOT())) continue;
    if (std::binary_search(
          two->lhcbIDs().begin(), two->lhcbIDs().end(), id))
      ++n_common;
  }

  return n_common==n_total;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TrackOverlapCreator::execute()
{
  debug() << "==> Execute" << endmsg;

  LHCb::Tracks* newcont = get<LHCb::Tracks>("Rec/Track/Best");
  LHCb::Tracks* oldcont = get<LHCb::Tracks>("Rec/Track/AllBest");

  LHCb::Tracks* newclones = new LHCb::Tracks();

  newclones->reserve(newcont->size());
  //BOOST_FOREACH(const LHCb::Track* const tnew, newcont) {
  for (LHCb::Tracks::const_iterator tnew = newcont->begin() ; newcont->end() != tnew ; ++tnew) {
    newclones->add((*tnew)->clone());
  }

  LHCb::Tracks* overlap = new LHCb::Tracks();
  LHCb::Tracks* oldonly = new LHCb::Tracks();
  LHCb::Tracks* newonly = newclones;
  LHCb::Tracks* merge = new LHCb::Tracks();

  //BOOST_FOREACH(const LHCb::Track* const told , oldcont) {
  for (LHCb::Tracks::const_iterator told = oldcont->begin() ; oldcont->end() != told ; ++told) {
    bool overlaps = false;
    for (LHCb::Tracks::const_iterator tnew = newclones->begin() ; newclones->end() != tnew ; ++tnew) {
    //BOOST_FOREACH(const LHCb::Track* const tnew, newclones) {
      if (samehits(*told,*tnew)) {
        overlaps = true;
        newclones->erase(*tnew);
        break;
      }
    }
    LHCb::Track* theclone = (*told)->clone();
    merge->add(theclone);
    if (overlaps) {
      overlap->add(theclone);
    } else {
      oldonly->add(theclone);
    }
  }
  
  for (LHCb::Tracks::const_iterator tnew = newclones->begin() ; newclones->end() != tnew ; ++tnew) {
    //BOOST_FOREACH(const LHCb::Track* const tnew, newclones) {
    merge->add((*tnew)->clone());
  }

  put(overlap,"Rec/Track/Overlap");
  put(newonly,"Rec/Track/NewOnly");
  put(oldonly,"Rec/Track/OldOnly");
  put(merge,  "Rec/Track/Merge");

  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode TrackOverlapCreator::finalize()
{
  debug() << "==> Finalize" << endmsg;


  return GaudiAlgorithm::finalize();  // must be called after all other actions
}


