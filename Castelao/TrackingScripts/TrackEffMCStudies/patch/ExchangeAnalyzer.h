/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef INCLUDE_EXCHANGEANALYZER_H
#define INCLUDE_EXCHANGEANALYZER_H 1

#include "GaudiAlg/GaudiAlgorithm.h"
#include "TrackCheckerTwoBase.h"


/** @class ExchangeAnalyzer ExchangeAnalyzer.h
 *  
 *
 * @author Paul Seyfert <pseyfert@cern.ch>
 * @date   2012-06-06
 */
class ExchangeAnalyzer : public TrackCheckerTwoBase {

public:

  /// Standard Constructor
  ExchangeAnalyzer(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~ExchangeAnalyzer(); ///< Destructor

  virtual StatusCode initialize(); ///< Algorithm initialization
  virtual StatusCode    execute(); ///< Algorithm event execution
  virtual StatusCode   finalize(); ///< Algorithm finalize

protected:


private:

  std::vector< std::vector<int> > m_linkedIds;


};
#endif // INCLUDE_EXCHANGEANALYZER_H

