/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "TypeStat.h"

//event
#include "Event/Track.h"
#include "Event/State.h"
#include "Event/GhostTrackInfo.h"

// Linker
#include "Linker/AllLinks.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToStream.h"

#include "Map.h"

#include "AIDA/IHistogram1D.h"
DECLARE_COMPONENT( TypeStat );

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TypeStat::TypeStat(const std::string& name,
    ISvcLocator* pSvcLocator ) :
  TrackCheckerBase( name , pSvcLocator ){

    declareProperty( "RequireLongTrack",
        m_requireLong = false );


  }

//=============================================================================
// Destructor
//=============================================================================
TypeStat::~TypeStat() {};

StatusCode TypeStat::initialize()
{
  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = TrackCheckerBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  return StatusCode::SUCCESS;
}

std::vector<int> TypeStat::clonepattern(LHCb::Tracks* tracks, LHCb::Tracks::const_iterator iterout) {

  std::vector<int> pattern;
  for (int i = 0 ; i < 7 ; ++i) 
    pattern.push_back(0);

  const LHCb::MCParticle* particle = mcTruth(*iterout);
  if (0==particle) return pattern;

  LHCb::Tracks::const_iterator iterT = tracks->begin();
  for (; iterT != tracks->end(); ++iterT){
    if (iterT==iterout) continue;

     if (((*iterT)->checkFitStatus(LHCb::Track::Fitted)) && (*iterT)->chi2PerDoF() > m_chicut)
      continue;
    if (m_kl && (*iterT)->info(LHCb::Track::CloneDist,5000.) < 4999.)
      continue;
    
    const LHCb::MCParticle* thisp = mcTruth(*iterT);

    if (thisp==particle) {

      if ((*iterT)->checkType(LHCb::Track::Downstream)) {
        pattern[2]=1;
        continue;
      }
      if ((*iterT)->checkType(LHCb::Track::Ttrack)) {
        pattern[3]=1;
        continue;
      }
      if ((*iterT)->checkType(LHCb::Track::Velo)) {
        pattern[4]=1;
        continue;
      }
      if ((*iterT)->checkType(LHCb::Track::Upstream)) {
        pattern[5]=1;
        continue;
      }
      if ((*iterT)->checkHistory(LHCb::Track::PatForward)) {
        pattern[0]=1;
        continue;
      }
      if ((*iterT)->checkHistory(LHCb::Track::PatMatch)) {
        pattern[1]=1;
        continue;
      }
      if (iterT<iterout)
        pattern[6]=1;





    }
  }
  iterT = tracks->begin();
  for (; iterT != iterout; ++iterT){

     if (((*iterT)->checkFitStatus(LHCb::Track::Fitted)) && (*iterT)->chi2PerDoF() > m_chicut)
      continue;
    if (m_kl && (*iterT)->info(LHCb::Track::CloneDist,5000.) < 4999.)
      continue;
    
    const LHCb::MCParticle* thisp = mcTruth(*iterT);

    if (thisp==particle) {
      pattern[6]=1;
    }
  }
  return pattern;
}

//=============================================================================
// Execute
//=============================================================================
StatusCode TypeStat::execute()
{

  AllLinks<LHCb::MCParticle,ContainedObject> allIds( evtSvc(), msgSvc(), "Pat/LHCbID" );
  m_linkedIds.clear();
  LHCb::MCParticle* part = allIds.first();
  while ( NULL != part ) {
    unsigned int minSize = part->key();
    while ( m_linkedIds.size() <= minSize ) {
      std::vector<int> dum;
      m_linkedIds.push_back( dum );
    }
    m_linkedIds[part->key()].push_back( allIds.key() );
    part = allIds.next();
  }



  if (initializeEvent().isFailure()){
    return Error("Failed to initialize event", StatusCode::FAILURE);
  }

  // get the input data
  LHCb::Tracks* tracks = get<LHCb::Tracks>(inputContainer());



  LHCb::Tracks::const_iterator iterT = tracks->begin();
  for (; iterT != tracks->end(); ++iterT){
    if ((*iterT)->type() != LHCb::Track::Long && m_requireLong )
      continue;
    if (((*iterT)->checkFitStatus(LHCb::Track::Fitted)) && (*iterT)->chi2PerDoF() > m_chicut)
      continue;
    if (m_kl && (*iterT)->info(LHCb::Track::CloneDist,5000.) < 4999.)
      continue;
    counter("nTracksInThisEvent") += 1;
    const LHCb::MCParticle* particle = mcTruth(*iterT);

    bool isaghost = (particle==0);
    if (isaghost) counter("nGhosts") +=1;
    std::vector<int> cp = clonepattern(tracks,iterT);
    counter("nClones") += cp[6];
    if ((*iterT)->checkType(LHCb::Track::Long)) {
      counter("nLongTracks") += 1;
      if (isaghost)
        counter("nLongGhosts") +=1;
      counter("nLongClones") += cp[6];
    }
    if ((*iterT)->checkType(LHCb::Track::Downstream)) {
      counter("nDownTracks") += 1;
      if (isaghost)
        counter("nDownGhosts") +=1;
      counter("nDownisFWDclone") += cp[0];
      counter("nDownisMATclone") += cp[1];
      counter("nDownisDWNclone") += cp[2];
      counter("nDownisTTRclone") += cp[3];
      counter("nDownisVELclone") += cp[4];
      counter("nDownisUPSclone") += cp[5];
      counter("nDownisLONclone") += (cp[0])||(cp[1]);
      counter("nDownClones") += cp[6];
    }
    if ((*iterT)->checkType(LHCb::Track::Ttrack)) {
      counter("nTtrackTracks") += 1;
      if (isaghost)
        counter("nTtrackGhosts") +=1;
      counter("nTTrackisFWDclone") += cp[0];
      counter("nTTrackisMATclone") += cp[1];
      counter("nTTrackisDWNclone") += cp[2];
      counter("nTTrackisTTRclone") += cp[3];
      counter("nTTrackisVELclone") += cp[4];
      counter("nTTrackisUPSclone") += cp[5];
      counter("nTTrackisLONclone") += (cp[0])||(cp[1]);
      counter("nTtrackClones") += cp[6];
    }
    if ((*iterT)->checkType(LHCb::Track::Velo)) {
      counter("nVeloTracks") += 1;
      if (isaghost)
        counter("nVeloGhosts") +=1;
      counter("nVeloisFWDclone") += cp[0];
      counter("nVeloisMATclone") += cp[1];
      counter("nVeloisDWNclone") += cp[2];
      counter("nVeloisTTRclone") += cp[3];
      counter("nVeloisVELclone") += cp[4];
      counter("nVeloisUPSclone") += cp[5];
      counter("nVeloisLONclone") += (cp[0])||(cp[1]);
      counter("nVeloClones") += cp[6];
    }
    if ((*iterT)->checkType(LHCb::Track::Upstream)) {
      counter("nUpstreamTracks") += 1;
      if (isaghost)
        counter("nUpstreamGhosts") +=1;
      counter("nUpstreamisFWDclone") += cp[0];
      counter("nUpstreamisMATclone") += cp[1];
      counter("nUpstreamisDWNclone") += cp[2];
      counter("nUpstreamisTTRclone") += cp[3];
      counter("nUpstreamisVELclone") += cp[4];
      counter("nUpstreamisUPSclone") += cp[5];
      counter("nUpstreamisLONclone") += (cp[0])||(cp[1]);
      counter("nUpstreamClones") += cp[6];
    }
    if ((*iterT)->checkHistory(LHCb::Track::PatForward)) {
      counter("nForwardTracks") += 1;
      if (isaghost)
        counter("nForwardGhosts") +=1;
      counter("nForwardisFWDclone") += cp[0];
      counter("nForwardisMATclone") += cp[1];
      counter("nForwardisDWNclone") += cp[2];
      counter("nForwardisTTRclone") += cp[3];
      counter("nForwardisVELclone") += cp[4];
      counter("nForwardisUPSclone") += cp[5];
      counter("nForwardisLONclone") += (cp[0])||(cp[1]);
      counter("nForwardClones") += cp[6];
    }
    if ((*iterT)->checkHistory(LHCb::Track::PatMatch)) {
      counter("nMatchingTracks") += 1;
      if (isaghost)
        counter("nMatchingGhosts") +=1;
      counter("nMatchisFWDclone") += cp[0];
      counter("nMatchisMATclone") += cp[1];
      counter("nMatchisDWNclone") += cp[2];
      counter("nMatchisTTRclone") += cp[3];
      counter("nMatchisVELclone") += cp[4];
      counter("nMatchisUPSclone") += cp[5];
      counter("nMatchisLONclone") += (cp[0])||(cp[1]);
      counter("nMatchingClones") += cp[6];
    }
    
  }


  counter("nTracks") += counter("nTracksInThisEvent").flag();
  counter("nTracksInThisEvent") = 0;
  // then we want to look at efficiencies


  return StatusCode::SUCCESS;
};



StatusCode TypeStat::finalize(){

  info() << "N tracks      " << counter("nTracks").flag() << endmsg;
  info() << "N ghosts      " << counter("nGhosts").flag() << endmsg;
  info() << "N clones      " << counter("nClones").flag() << endmsg;
  info() << "N longtracks  " << counter("nLongTracks").flag() << endmsg;
  info() << "N longghosts  " << counter("nLongGhosts").flag() << endmsg;
  info() << "N longclones  " << counter("nLongClones").flag() << endmsg;
  info() << "N downtracks  " << counter("nDownTracks").flag() << endmsg;
  info() << "N downghosts  " << counter("nDownGhosts").flag() << endmsg;
  info() << "N downclones  " << counter("nDownClones").flag() << endmsg;
  info() << "N ttracks     " << counter("nTtrackTracks").flag() << endmsg;
  info() << "N tghosts     " << counter("nTtrackGhosts").flag() << endmsg;
  info() << "N tclones     " << counter("nTtrackClones").flag() << endmsg;
  info() << "N velotracks  " << counter("nVeloTracks").flag() << endmsg;
  info() << "N veloghosts  " << counter("nVeloGhosts").flag() << endmsg;
  info() << "N veloclones  " << counter("nVeloClones").flag() << endmsg;
  info() << "N fwdtracks   " << counter("nForwardTracks").flag() << endmsg;
  info() << "N fwdghosts   " << counter("nForwardGhosts").flag() << endmsg;
  info() << "N fwdclones   " << counter("nForwardClones").flag() << endmsg;
  info() << "N matchtracks " << counter("nMatchingTracks").flag() << endmsg;
  info() << "N matchghosts " << counter("nMatchingGhosts").flag() << endmsg;
  info() << "N matchclones " << counter("nMatchingClones").flag() << endmsg;
  info() << "N upstreamtracks " << counter("nUpstreamTracks").flag() << endmsg;
  info() << "N upstreamghosts " << counter("nUpstreamGhosts").flag() << endmsg;
  info() << "N upstreamclones " << counter("nUpstreamClones").flag() << endmsg;

  return TrackCheckerBase::finalize();
}
