/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef INCLUDE_TRACKOVERLAPCREATOR_H
#define INCLUDE_TRACKOVERLAPCREATOR_H 1

#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Track.h"


/** @class TrackOverlapCreator TrackOverlapCreator.h
 *  compare two track containers to each other, copy the overlap and the two non-overlapping parts to three output containers
 *
 * @author Paul Seyfert <pseyfert@cern.ch>
 * @date   2012-06-04
 */
class TrackOverlapCreator : public GaudiAlgorithm {

public:

  /// Standard Constructor
  TrackOverlapCreator(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~TrackOverlapCreator(); ///< Destructor

  virtual StatusCode initialize(); ///< Algorithm initialization
  virtual StatusCode    execute(); ///< Algorithm event execution
  virtual StatusCode   finalize(); ///< Algorithm finalize

protected:


private:

  bool samehits(LHCb::Track* one, LHCb::Track* two);


};
#endif // INCLUDE_TRACKOVERLAPCREATOR_H

