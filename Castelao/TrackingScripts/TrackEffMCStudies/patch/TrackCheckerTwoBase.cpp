/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TrackCheckerTwoBase.cpp,v 1.12 2009-09-09 14:32:55 cattanem Exp $
// Include files 
#include "TrackCheckerTwoBase.h"
#include "Event/Track.h"

#include "Map.h"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackCheckerTwoBase::TrackCheckerTwoBase( const std::string& name,
                                    ISvcLocator* pSvcLocator ) :
  GaudiTupleAlg( name , pSvcLocator ),
  m_associatorA(0,""),
  m_associatorB(0,""),
  m_associator_refContainerA(0,""),
  m_associator_refContainerB(0,""),
  m_all("ALL"){

  declareProperty( "TracksInContainerA",
                   m_tracksInContainerA = LHCb::TrackLocation::Default  );
  declareProperty( "TracksInContainerB",
                   m_tracksInContainerB = LHCb::TrackLocation::Default  );

  declareProperty( "TracksRefContainerA",
		   m_tracksRefContainerA = ""  );
  declareProperty( "TracksRefContainerB",
		   m_tracksRefContainerB = ""  );
 
  declareProperty( "Selector",
                   m_selectorName = "MCReconstructible" );
  declareProperty( "Extrapolator",
                   m_extrapolatorName = "TrackMasterExtrapolator" );  
  declareProperty( "LinkerInTableA",   m_linkerInTableA = ""       );
  declareProperty( "LinkerInTableB",   m_linkerInTableB = ""       );

  declareProperty("SplitByAlgorithm", m_splitByAlgorithm = false);
  declareProperty("SplitByType",      m_splitByType = false);

  declareProperty("GhostClassification",
                  m_ghostToolName = "LongGhostClassification" );

  declareProperty("SelectionCriteria", m_selectionCriteria = "ChargedLong"); 

  declareProperty( "ChiCut" , m_chicut = 100. );
  declareProperty( "KLCut"  , m_kl = false );
}

//=============================================================================
// Destructor
//=============================================================================
TrackCheckerTwoBase::~TrackCheckerTwoBase() {}; 

//=============================================================================
// Initialization. Check parameters
//=============================================================================
StatusCode TrackCheckerTwoBase::initialize()
{

  //static const std::string histoDir = "Track/" ;
  //if ( "" == histoTopDir() ) setHistoTopDir(histoDir);

  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Set the path for the linker table Track - MCParticle
  if ( m_linkerInTableA == "" ) m_linkerInTableA = m_tracksInContainerA;
  if ( m_linkerInTableB == "" ) m_linkerInTableB = m_tracksInContainerB;


  m_selector = tool<IMCReconstructible>(m_selectorName,
                                        "Selector", this );
  
  m_extrapolator = tool<ITrackExtrapolator>(m_extrapolatorName);
  
  // Retrieve the magnetic field and the poca tool
  m_poca = tool<ITrajPoca>("TrajPoca");
  m_pIMF = svc<IMagneticFieldSvc>( "MagneticFieldSvc",true );
  m_stateCreator = tool<IIdealStateCreator>("IdealStateCreator");
  m_visPrimVertTool = tool<IVisPrimVertTool>("VisPrimVertTool");
  m_ghostClassification = tool<ITrackGhostClassification>(m_ghostToolName,"GhostTool",this); 
  
  const TrackMaps::RecMap& theMap = TrackMaps::recDescription();
  m_recCat = theMap.find(m_selectionCriteria)->second;

  return StatusCode::SUCCESS;
};

StatusCode TrackCheckerTwoBase::execute(){
  
  if (initializeEvent().isFailure()){
    return Error("Failed to initialize event", StatusCode::FAILURE);
  }

  return StatusCode::SUCCESS;
}

StatusCode TrackCheckerTwoBase::initializeEvent(){

  // get the association table 
  m_associatorA = AsctTool(evtSvc(), m_tracksInContainerA);
  m_associatorB = AsctTool(evtSvc(), m_tracksInContainerB);
  m_directTableA = m_associatorA.direct();
  m_directTableB = m_associatorB.direct();
  
  m_inverseTableA = m_associatorA.inverse();
  m_inverseTableB = m_associatorB.inverse();

  if (m_tracksRefContainerA != ""){
    m_associator_refContainerA = AsctTool(evtSvc(), m_tracksRefContainerA);
    m_directTable_refContainerA = m_associator_refContainerA.direct();
    if (!m_directTable_refContainerA)
      return Error("Failed to find direct table for reference container",
                   StatusCode::FAILURE);
    
    m_inverseTable_refContainerA = m_associator_refContainerA.inverse();
    if (!m_inverseTable_refContainerA)
      return Error("Failed to find inverse table for refContainer", StatusCode::FAILURE); 
  }

  if (m_tracksRefContainerB != ""){
    m_associator_refContainerB = AsctTool(evtSvc(), m_tracksRefContainerB);
    m_directTable_refContainerB = m_associator_refContainerB.direct();
    if (!m_directTable_refContainerB)
      return Error("Failed to find direct table for reference container",
                   StatusCode::FAILURE);
    
    m_inverseTable_refContainerB = m_associator_refContainerB.inverse();
    if (!m_inverseTable_refContainerB)
      return Error("Failed to find inverse table for refContainer", StatusCode::FAILURE);

  }
  return StatusCode::SUCCESS;
} 
TrackCheckerTwoBase::LinkInfo TrackCheckerTwoBase::reconstructedA(const LHCb::MCParticle* particle) const{
  
  TrackCheckerTwoBase::LinkInfo info; 
  info.track = 0; // in case it is a ghost
  info.clone = 0;
  InverseRange rangeA = m_inverseTableA->relations(particle);
  if (!rangeA.empty()) {
    for (inverse_iterator it = rangeA.begin() ; it != rangeA.end(); ++it) {
      if (((!((it)->to()->checkFitStatus(LHCb::Track::Fitted))) || (it)->to()->chi2PerDoF() < m_chicut)&&((!m_kl) || (it->to()->info(LHCb::Track::CloneDist,5000.)>4999.) || (!(it->to()->hasInfo(LHCb::Track::CloneDist))))) {
        if (0==info.track) {
          info.track = (it)->to();
          info.purity = rangeA.begin()->weight();
        } else
          info.clone += 1;
        //info.clone = rangeB.size() - 1u;
      }
    }
  }
  if (info.track) return info;
  info.track = NULL;
  info.clone = 0;
  info.purity = -1.;

  return info; 
}
TrackCheckerTwoBase::LinkInfo TrackCheckerTwoBase::reconstructedB(const LHCb::MCParticle* particle) const{
  
  TrackCheckerTwoBase::LinkInfo info; 
  info.track = 0; // in case it is a ghost
  info.clone = 0;
  InverseRange rangeB = m_inverseTableB->relations(particle);
  if (!rangeB.empty()) {
    for (inverse_iterator it = rangeB.begin() ; it != rangeB.end(); ++it) {
      if (((!((it)->to()->checkFitStatus(LHCb::Track::Fitted))) || (it)->to()->chi2PerDoF() < m_chicut)&&((!m_kl) || (it->to()->info(LHCb::Track::CloneDist,5000.)>4999.) || (!(it->to()->hasInfo(LHCb::Track::CloneDist))))) {
        if (0==info.track) {
          info.track = (it)->to();
          info.purity = rangeB.begin()->weight();
        } else
          info.clone += 1;
        //info.clone = rangeB.size() - 1u;
      }
    }
  }
  if (info.track) return info;
  info.track = NULL;
  info.clone = 0;
  info.purity = -1.;

  return info; 
}


TrackCheckerTwoBase::LinkInfo TrackCheckerTwoBase::reconstructed(const LHCb::MCParticle* particle) const{
  
  TrackCheckerTwoBase::LinkInfo info; 
  info.track = 0; // in case it is a ghost
  info.clone = 0;
  InverseRange rangeA = m_inverseTableA->relations(particle);
  InverseRange rangeB = m_inverseTableB->relations(particle);
  if (!rangeA.empty() && !rangeB.empty()) {
    for (inverse_iterator it = rangeB.begin() ; it != rangeB.end(); ++it) {
      if (((!((it)->to()->checkFitStatus(LHCb::Track::Fitted))) || (it)->to()->chi2PerDoF() < m_chicut)&&((!m_kl) || (it->to()->info(LHCb::Track::CloneDist,5000.)>4999.) || (!(it->to()->hasInfo(LHCb::Track::CloneDist))))) {
        if (0==info.track) {
          info.track = (it)->to();
          info.purity = rangeB.begin()->weight();
        } else
          info.clone += 1;
        //info.clone = rangeB.size() - 1u;
      }
    }
  }
  if (info.track) return info;
  info.track = NULL;
  info.clone = 0;
  info.purity = -1.;

  return info; 
}

TrackCheckerTwoBase::LinkInfo TrackCheckerTwoBase::reconstructedInRefContainer(const LHCb::MCParticle* particle) const {
  fatal() << "not implemented" << endmsg; 
  TrackCheckerTwoBase::LinkInfo info; 
  info.track = 0; // in case it is a ghost
  InverseRange rangeA = m_inverseTable_refContainerA->relations(particle);
  InverseRange rangeB = m_inverseTable_refContainerB->relations(particle);
  if (!rangeB.empty()) {
    info.track = rangeB.begin()->to();
    info.clone = rangeB.size() - 1u;
    info.purity = rangeB.begin()->weight();
  } 
  else{
    info.track = NULL;
    info.clone = 0;
    info.purity = -1.;
  }
  return info; 
}

TrackCheckerTwoBase::InverseRange TrackCheckerTwoBase::reconstructedTracksA(const LHCb::MCParticle* particle) const{
  return m_inverseTableA->relations(particle);
}
TrackCheckerTwoBase::InverseRange TrackCheckerTwoBase::reconstructedTracksB(const LHCb::MCParticle* particle) const{
  return m_inverseTableB->relations(particle);
}


const LHCb::MCParticle* TrackCheckerTwoBase::mcTruth(const LHCb::Track* aTrack) const
{
  const LHCb::MCParticle * particle = NULL;
  if (!m_directTableA) 
  {
    Error( "Track MCParticle table missing. Did you call initializeEvent() ?",StatusCode::FAILURE,0).ignore();
  }
  else
  {
    TrackCheckerTwoBase::DirectRange rangeA = m_directTableA->relations(aTrack);
    if (rangeA.empty() == false) {
      particle = rangeA.begin()->to();
      if( particle && particle->particleID().threeCharge() ==0 ) {
        Warning("Linker table for track contains pointer to particle with zero charge",StatusCode::SUCCESS,0).ignore() ;
        particle = 0 ;
      }
      return particle;
    }
    TrackCheckerTwoBase::DirectRange rangeB = m_directTableB->relations(aTrack);
    if (rangeB.empty() == false) {
      particle = rangeB.begin()->to();
      if( particle && particle->particleID().threeCharge() ==0 ) {
        Warning("Linker table for track contains pointer to particle with zero charge",StatusCode::SUCCESS,0).ignore() ;
        particle = 0 ;
      }
      return particle;
    }
  }
  return particle;
}

bool TrackCheckerTwoBase::bAncestorWithReconstructibleDaughters(const LHCb::MCParticle* mcPart) const
{
  // loop back and see if there is a B in the history
  bool fromB = false;
  const LHCb::MCParticle* mother = mcPart->mother();
  while ( mother !=0 && fromB == false) {
    fromB = mother->particleID().hasBottom();
    if (fromB && !allDaughtersReconstructible(mother))
	return false;
    mother = mother->mother();
  } // loop
  return fromB;
}

bool TrackCheckerTwoBase::bAncestor(const LHCb::MCParticle* mcPart) const
{
  // loop back and see if there is a B in the history
  bool fromB = false;
  const LHCb::MCParticle* mother = mcPart->mother();
  while ( mother !=0 && fromB == false) {
    fromB = mother->particleID().hasBottom();
    mother = mother->mother();
  } // loop
  return fromB;
}

bool TrackCheckerTwoBase::ksLambdaAncestor(const LHCb::MCParticle* mcPart) const
{
  // loop back and see if there is a B in the history
  bool fromKsL = false;
  const LHCb::MCParticle* mother = mcPart->mother();
  while ( mother !=0 && fromKsL == false) {
    if(abs(mother->particleID().pid()) == 310 || 
       abs(mother->particleID().pid()) == 3122)
      fromKsL = true;
    mother = mother->mother();
  } // loop
  return fromKsL;
}

 bool TrackCheckerTwoBase::allDaughtersReconstructible(const LHCb::MCParticle* mcPart) const
{
  const SmartRefVector<LHCb::MCVertex> &vtx = mcPart->endVertices();

  for(SmartRefVector<LHCb::MCVertex>::const_iterator i=vtx.begin();i!=vtx.end();++i){
    const SmartRefVector<LHCb::MCParticle> &ch = (*i)->products();
    for(SmartRefVector<LHCb::MCParticle>::const_iterator j=ch.begin();
	j!=ch.end();++j){
      
      if ((abs((*j)->particleID().pid())==321 ||
	       abs((*j)->particleID().pid())==211 ||
	       abs((*j)->particleID().pid())==13  ||
           abs((*j)->particleID().pid())==11  ||
	       abs((*j)->particleID().pid())==2212)){
	if ( !selected(*j) && 
	     (*j)->mother()->particleID().pid()!=22 &&
	     (*j)->mother()->particleID().pid()!=-99000000 &&
	     (*j)->mother()->particleID().pid()!=130 &&
	     (*j)->mother()->particleID().pid()!=310 &&
	     (*j)->mother()->particleID().pid()!=3122){
	  return false;
	}
      }
      else
	if(!allDaughtersReconstructible(*j))                              
	  return false;
    }
  }
    
  return true;
}



StatusCode TrackCheckerTwoBase::finalize()
{
  return GaudiTupleAlg::finalize();
}


