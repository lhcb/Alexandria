###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
j = Job( application = Brunel( version = 'v44r7' ) )
### assume you have a Boole job i with name "Boole_25ns_nu1.6_md
##  import re
##  j.name = re.sub("Boole","Brunel",i.name)
##  pol = re.sub("Boole.*_m","",i.name)

# otherwise set by hand
pol = "u" # can be "u" or "d"

j.application.optsfile = [File ( '<path/to>/TrackEffMC.py'),
                          File ( '<path/to>/tag_m'+pol+'.py' )]
  
#j.inputdata = LHCbDataset ( files = [ 'PFN:<path/to>/gangadir/workspace/pseyfert/LocalXML/'+str(i.id)+'/output/Boole.digi' ] )
j.inputdata = something
j.outputfiles = [SandboxFile('*.root')]
j.splitter = None
j.merger = None
j.inputsandbox = []

# backend in heidelberg
j.backend=SGE(extraopts='-l hio=0,os=slc5,ujl=20 -masterq fgslc5.q@nu*,fgslc5.q@lambda*,fgslc5.q@alpha*,fgslc5.q@beta*,fgslc5.q@gamma*,fgslc5.q@eta*,fgslc5.q@delta*,fgslc5.q@lhcb-raid03*')
j.prepare()
