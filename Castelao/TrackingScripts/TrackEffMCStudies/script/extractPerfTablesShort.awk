
BEGIN {
    left = 0;
}

/ \*\*\*\*\*\*\*\*\*\*\*\*\*\* / {
    left = 16;
    ghost[$1] = "ghost fraction  " $10 " & " $14;
}

// {
    if (15 == left) {
	eff[$1] = " " $10 " (" $12 ") & " $15 " / " $19;
    } else if (14 == left) {
	effG5[$1] = " " $13 " (" $15 ") & " $18 " / " $22;
    } else if (13 == left) {
	effB[$1] = " " $12 " (" $14 ") & " $17 " / " $21;
    } else if (12 == left) {
	effBG5[$1] = " " $15 " (" $17 ") & " $20 " / " $24;
    } else if (11 == left) {
	effGoodB[$1] = " " $13 " (" $15 ") & " $18 " / " $22;
    } else if (10 == left) {
	effGoodBG5[$1] = " " $17 " (" $19 ") & " $22 " / " $26;
    } else if (9 == left) {
	effKsL[$1] = " " $11 " (" $13 ") & " $16 " / " $20;
    } else if (8 == left) {
  effKsLL[$1] = " " $12 " (" $14 ") & " ;
    } else if (7 == left) {
  effKsLD[$1] = " " $12 " (" $14 ")  & " ;
    } else if (6 == left) {
  effKsB[$1] = " " $12 "  (" $14 ") & " ;
    } else if (5 == left) {
  effKsBL[$1] = " " $13 "  (" $15 ") & " ;
    } else if (4 == left) {
  effKsBD[$1] = " " $13 "  (" $15 ") & " ;
    } else if (3 == left) {
	effKsLG5[$1] = " " $14 " (" $16 ") & " $19 " / " $23;
    }
    
    if (left) --left;
}

END {
    for (i in ghost) {
	fname = i ".tex"
	if ("TTrack" == i && "TTrackTTrack" in ghost) {
	    sub(/^.*[nsV] & /, "", ghost[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", eff[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", effG5[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", effB[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", effBG5[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", effGoodB[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", effGoodBG5[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", effKsL[i "TTrack"]);
	    sub(/^.*[nsV] & /, "", effKsLG5[i "TTrack"]);
	    print "{\\small \\begin{center} \\rowcolors{4}{white}{black!10!white} \\begin{tabular}{l c c c c} %\\toprule" > fname;
	    print "\\rowcolor{black} & \\multicolumn{2}{c}{\\cellcolor{black}\\textcolor{white}{long reconstructible}} & \\multicolumn{2}{c}{\\cellcolor{black}\\textcolor{white}{T reconstructible}} \\\\" >> fname;
	    print "\\rowcolor{black} & \\textcolor{white}{eff. (clone)} & \\textcolor{white}{hit pur./eff.} & \\textcolor{white}{eff. (clone)} & \\textcolor{white}{hit pur./eff.} \\\\ %\\midrule " >> fname;
	    print "\\rowcolor{black} & \\textcolor{white}{[\\%]} & \\textcolor{white}{[\\%]} & \\textcolor{white}{[\\%]} & \\textcolor{white}{[\\%]} \\\\ %\\midrule " >> fname;
	    print eff[i] " & " eff[i "TTrack"] " \\\\" >> fname;
	    print effG5[i] " & " effG5[i "TTrack"] " \\\\ %\\midrule" >> fname;
	    print effB[i] " & " effB[i "TTrack"] " \\\\" >> fname;
	    print effBG5[i] " & " effBG5[i "TTrack"] " \\\\ %\\midrule" >> fname;
	    print effGoodB[i] " & " effGoodB[i "TTrack"] " \\\\" >> fname;
	    print effGoodBG5[i] " & " effGoodBG5[i "TTrack"] " \\\\ %\\midrule" >> fname;
	    print effKsL[i] " & " effKsL[i "TTrack"] " \\\\" >> fname;
	    print effKsLG5[i] " & " effKsLG5[i "TTrack"] " \\\\ %\\midrule" >> fname;
	    print "\\rowcolor{black} & \\textcolor{white}{tr. avg.} & \\textcolor{white}{ev. avg.} & \\textcolor{white}{tr. avg.} & \\textcolor{white}{ev. avg.} \\\\ %\\midrule " >> fname;
	    print ghost[i] " & " ghost[i "TTrack"] " \\\\" >> fname;
	    print "\\toprule \\end{tabular} \\end{center} }" >> fname;
	} else if ("TTrackTTrack" == i) {
        } else {
	    print "{\\small \\rowcolors{3}{black!10!white}{} \\begin{tabular}{c c} %\\toprule" > fname;
	    print "\\rowcolor{black}  \\textcolor{white}{eff. (clone)} & \\textcolor{white}{hit pur./eff.} \\\\ %\\midrule" >> fname;
	    print "\\rowcolor{black}  \\textcolor{white}{[\\%]} & \\textcolor{white}{[\\%]} \\\\ %\\midrule " >> fname;
	    print eff[i] " \\\\" >> fname;
	    print effG5[i] " \\\\ %\\midrule" >> fname;
	    print effB[i] " \\\\" >> fname;
	    print effBG5[i] " \\\\ %\\midrule" >> fname;
	    print effGoodB[i] " \\\\" >> fname;
	    print effGoodBG5[i] " \\\\ %\\midrule" >> fname;
	    print effKsL[i] " \\\\" >> fname;
	    print effKsLG5[i] " \\\\ %\\midrule" >> fname;
      print effKsLL[i] " \\\\" >> fname;
      print effKsLD[i] " \\\\ %\\midrule" >> fname;
      print effKsB[i] " \\\\" >> fname;
      print effKsBL[i] " \\\\ %\\midrule" >> fname;
      print effKsBD[i] " \\\\" >> fname;
	    print "\\rowcolor{black}  \\textcolor{white}{tr. avg.} & \\textcolor{white}{ev. avg.} \\\\ %\\midrule " >> fname;
	    print ghost[i] " \\\\" >> fname;
	    print "\\toprule \\end{tabular} }" >> fname;
	}
    }
}
