/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TTM_EventInfo_H
#define TTM_EventInfo_H

class WMuMuTupleMaker ;

#include "Common.h"

namespace WMuMu
{
  class EventInfo
  {
  public: 
    friend class ::WMuMuTupleMaker ;
    EventInfo() : bxid(0),eventnumber(0),runnumber(0) {}
    //virtual ~EventInfo() {}
    //template<class LHCbEventInfo>
    //EventInfo(const LHCbEventInfo& p) :
  private:
    // some event if info
    unsigned short    bxid ;
    unsigned long int eventnumber ;
    unsigned int      runnumber ;
    // add recsummary info?
    //ClassDef(EventInfo,1)
  } ;
}

#endif
