/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef WMuMuXContainer_H
#define WMuMuXContainer_H

#include "WToMuMuXCandidate.h"


namespace WMuMu
{
  class WToMuMuXContainer
  {
 public:
  WToMuMuXContainer() {}
  const WToMuMuXCandidate* bestSingleJetCandidate() const ;
  const WToMuMuXCandidate* bestDiJetCandidate() const ;
  size_t numSingleCandidate() const ;
  void clear() ;
 public:
  std::vector<WToMuMuXCandidate> candidates ;
} ;
  
}

#endif
