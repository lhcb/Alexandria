/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef __WMUMU_JETPARTICLE_H__
#define __WMUMU_JETPARTICLE_H__

#include "CompositeParticle.h"
#include "Math/PositionVector3D.h"
#include "Math/Cartesian3D.h"
#include "Math/Vector3Dfwd.h"

namespace WMuMu
{

  template<long int Min, long int Max, int bins=256>
    struct LinearPackedFloat
    {
      float operator=( float val ) {
	data = val <= Min ? 0 : (val>=Max ? (bins-1) : int((val-Min)/float(Max-Min)*bins)  ) ;
	return val ;
      }
      float operator=( double val ) {
	return operator=(float(val)) ;
      }
	float val() const { return float(Min) + (data+0.5)*(Max-Min)/float(bins) ; }
      operator float() const { return val() ; } ;
      unsigned char data ;
      //const float delta = (Max-Min)/float(bins) ;
    } ;

  //typedef ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<float>,ROOT::Math::DefaultCoordinateSystemTag> XYZPoint ;
  using Carthesian3DF=ROOT::Math::Cartesian3D<float> ;
  using XYZPoint = ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<float>,ROOT::Math::DefaultCoordinateSystemTag> ;
  class JetVertex
  {
  public:
    JetVertex() {}
    template<class LHCbParticle>
      JetVertex(const LHCbParticle& p) :
    pid{p.particleID().pid()},
    position{p.endVertex()->position()},momentum{p.momentum()},
    Ndaughters{(unsigned char)(p.endVertex()->outgoingParticles().size())},
      vtxchi2{float(p.endVertex()->chi2())} {}
  public:
    int pid{0} ;
    XYZPoint position{} ;
    MomentumVector momentum{} ;
    unsigned char Ndaughters{0} ;
    float vtxchi2{0} ;	
  } ;

  
  class JetParticle : public Particle
  {
  public:
    // default constructor
  JetParticle() : Particle() {}
    // constructor from LHCb particle
    template<class LHCbParticle>
      JetParticle(const LHCbParticle& p) 
      : Particle(p),
      CPF(0),MTF(0),MNF(0),MPT(0),
      JetWidth(0), JetCharge(0), N90(0),NLong(0), NLong5(0), NTrack(0), NWithPVInfo(0), bdt0(0),bdt1(0),mcmotherpid(0) {}
  public:
    float CPF ;
    float MTF ;
    float MNF ;
    float MPT ;
    float JetWidth ;
    float JetCharge ; // jet charge for power of pT = kappa=0.5 (CMS uses 0.6; ATLAS 0.3,0.5,0.7)
    float JetFragmentation ; // fragmentation parameter= sum(pt^2) / (sum(pt))^2
    //LinearPackedFloat<0,1,20> packedCPF ;
    //LinearPackedFloat<0,1,20> packedMTF ;
    //LinearPackedFloat<0,1,20> packedMNF ;
    //unsigned char NSATCALO ; 
    //unsigned char NSATECAL ; 
    //unsigned char NSATHCAL ;
    unsigned char N90 ;
    unsigned char NLong ;
    unsigned char NLong5 ; // Number of charged tracks with pT>0.5
    unsigned char NTrack ;
    unsigned char NWithPVInfo ;
    unsigned char bdt0 ;
    unsigned char bdt1 ;

    float sumVeloPt ;
    float sumLongPt ;
    float sumVeloPtPrompt ;
    float sumLongPtPrompt ;
    float sumVeloIPChi2 ;
    float sumLongIPChi2 ;
    unsigned int mcmotherpid ;
    float sumLongIP ;
    float medianLongIP ;
    float minLongIP ;
    float ptWeightedLongIP ;
    float x{0} ;
    float y{0} ;
    float z{0} ;
    unsigned char Nvertexdaughters{0} ;
    std::vector<JetVertex> vertices ;
  } ;
}

#endif
