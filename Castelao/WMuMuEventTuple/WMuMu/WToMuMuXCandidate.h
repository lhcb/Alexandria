/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef WMuMuXCandidate_H
#define WMuMuXCandidate_H

#include "CompositeParticle.h"

namespace WMuMu
{
  class WToMuMuXCandidate
  {
  public:
    friend class ::WMuMuTupleMaker ;
    template<class LHCbParticle>
      WToMuMuXCandidate(const LHCbParticle& /*p*/) 
      : //Wmuon(-1), Nmuon(-1),Nmuon2(-1),Nelectron(-1),
      Njet1(-1),Njet2(-1),
      decaytype(0),Ndecaylength(0),Ndecaylengtherr(0) {}
  WToMuMuXCandidate() 
    : //Wmuon(-1), Nmuon(-1),Nmuon2(-1),Nelectron(-1),
      Njet1(-1),Njet2(-1),
      decaytype(0),Ndecaylength(0),Ndecaylengtherr(0) {}
    
    enum EDecayType {UnknownDecayType=0, MuJetRS=1, MuJetWS=2, MuDiJetRS=3, MuDiJetWS=4, MuERS=5, MuEWS=6, MuMuMu=7,
		     MuPiRS=8, MuPiWS=9} ;
    
    bool isBestSingleJetCandidate() const ;

    double WconstrainedWMass() const {
      return (W.p4() - N.p4() + Np4Wc.p4()).M() ;
    }

    bool tightSelection() const ;

  public:
    //const Particle* m_Wmuon ;
    CompositeParticle N ;
    CompositeParticle W ;
    CompositeParticle mumu ;
    MomentumVector Np4Wc ;
    signed char Njet1 ;
    signed char Njet2 ;
    unsigned char decaytype ;
    //unsigned char mumuvelooverlap ;
    //unsigned char mumuToverlap ;
    //unsigned char mumuMuonoverlap ;
    //float mumuvtxchi2 ;
    //float mumumass ;
    float Ndecaylength ;
    float Ndecaylengtherr ;
    float Nip ;
    float Nipchi2 ;
  } ;
}

#endif
