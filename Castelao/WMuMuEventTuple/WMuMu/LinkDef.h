/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class WMuMu::Event+;
#pragma link C++ class WMuMu::EventInfo+;
#pragma link C++ class WMuMu::MCEventInfo+;
#pragma link C++ class WMuMu::MCParticle+;
#pragma link C++ class WMuMu::MCVirtualJet+;
#pragma link C++ class WMuMu::MCVirtualDiJet+;
#pragma link C++ class WMuMu::Particle+;
#pragma link C++ class WMuMu::TrackParticle+;
#pragma link C++ class WMuMu::CompositeParticle+;
#pragma link C++ class WMuMu::JetParticle+;
#pragma link C++ class WMuMu::IsolationInfo+;
#pragma link C++ class WMuMu::MomentumVector+;
#pragma link C++ class WMuMu::WToMuMuXCandidate+;
#pragma link C++ class WMuMu::WToMuMuXContainer+;
#pragma link C++ class WMuMu::ParticleTisTos+ ;
#pragma link C++ class WMuMu::TisTosInfo<unsigned int>+ ;
#pragma link C++ class WMuMu::TisTosInfo<ULong64_t>+ ;
#pragma link C++ class WMuMu::LinearPackedFloat<0,1>+ ;
#pragma link C++ class WMuMu::LinearPackedFloat<0,1,64>+ ;
#pragma link C++ class WMuMu::LinearPackedFloat<0,1,3>+ ;
#pragma link C++ class WMuMu::ParticleVector+ ;
//#pragma link C++ class std::array<unsigned char,4>+ ;
#pragma link C++ class WMuMu::Track+ ;
#pragma link C++ class WMuMu::PrimaryVertex+ ;
#pragma link C++ class WMuMu::JetVertex+ ;
//#pragma link C++ class WMuMu::XYZPoint+ ;
#pragma link C++ std::vector<WMuMu::CompositeParticle>+ ;
#endif
