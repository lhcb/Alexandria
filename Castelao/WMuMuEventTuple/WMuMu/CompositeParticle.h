/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef __WMUMU_COMPOSITEPARTICLE_H__
#define __WMUMU_COMPOSITEPARTICLE_H__

#include "Particle.h"
#include <array>

namespace WMuMu
{
  class CompositeParticle : public Particle
  {
  public:
    // default constructor
    CompositeParticle() : Particle(), ndaughters(0), vtxndof(0), vtxchi2(0) {}
    // template constructor thattakes LHCb particle as input
    template<class LHCbParticle>
      CompositeParticle(const LHCbParticle& p) 
      : Particle(p), ndaughters(p.daughters().size()), vtxndof(0),vtxchi2(0.0),
      IPCHI2(0.0),IP(0.0),PVindex(-1),velooverlap(-1),Toverlap(-1),muonoverlap(-1),Merr(-1),cosThetaStarCS(-2)
      {
	dau1=dau2=dau3=dau4=-1 ;
	if(p.endVertex()) {
	  vtxndof = p.endVertex()->nDoF() ;
	  vtxchi2 = p.endVertex()->chi2() ;
	}
      }
 
  public:
    unsigned char ndaughters ;
    signed char dau1 ;
    signed char dau2 ;
    signed char dau3 ;
    signed char dau4 ;    
    signed   char vtxndof ;
    float    vtxchi2 ;
    float    IPCHI2 ;
    float    IP ;
    signed char PVindex ;
    unsigned char velooverlap ;
    unsigned char Toverlap ;
    unsigned char muonoverlap ;
    float Merr ;
    float cosThetaStarCS ;
  } ;

  typedef  std::vector<CompositeParticle> ParticleVector ;
  
}


#endif
