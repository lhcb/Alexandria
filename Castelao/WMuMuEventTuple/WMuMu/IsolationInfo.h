/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TTM_ISOLATION_H
#define TTM_ISOLATION_H

class WMuMuTupleMaker ;

#include "MomentumVector.h"
#include "Common.h"
#include "TObject.h"

namespace WMuMu
{
  
  class IsolationInfo
  {
  public: 
    friend class ::WMuMuTupleMaker ;
  IsolationInfo() : IT1(0),IT3(0),IT5(0),IT5e(0),PFType(0) {}

  public:
    unsigned char IT1 ;
    unsigned char IT3 ;
    unsigned char IT5 ;
    unsigned char IT5e ;
    unsigned short PFType ;
  } ;
}

#endif
