/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef WMUMU_PRIMARYVERTEX
#define WMUMU_PRIMARYVERTEX

namespace WMuMu
{
  class PrimaryVertex
  {
  public:
  PrimaryVertex() : x(0),y(0),z(0),ntrack(0),nbackward(0),nlong(0),sumpt(0) {}
  public:
    float x ;
    float y ;
    float z ;
    unsigned char ntrack ;
    unsigned char nbackward ;
    unsigned char nlong ;
    float sumpt ;
  } ;
}

#endif
