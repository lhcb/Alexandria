/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef __TTM_TrackParticle_H
#define __TTM_TrackParticle_H

#include "Particle.h"
#include "IsolationInfo.h"
#include "Common.h"

namespace WMuMu
{
  
  class TrackParticle : public Particle
  {
  public: 
    friend class ::WMuMuTupleMaker ;
    TrackParticle() {}
    virtual ~TrackParticle() ;
    template<class LHCbParticle>
      TrackParticle(const LHCbParticle& p) : Particle(p) {}
    
    bool defaultWMuonSelection() const {
      return Pt > 20e3 /*&& ProbNNghost<0.5*/ && TRACK_IP <0.2 && Iso.IT5/256. > 0.8 ;
    }
    bool tightWMuonSelection() const {
      return 
	Pt > 20e3 
	/*&& ProbNNghost<0.5 */
	&& TRACK_IP <0.04
	&& Iso.IT5/256. > 0.8 
	&& CaloEcalE/p4().P() < 0.01 
	&& (CaloHcalE+CaloHcalE)/p4().P() < 0.04 ;
    }
    bool defaultNMuonSelection() const {
      return Pt >  5e3 /*&& ProbNNghost<0.5*/ && TRACK_IP <0.2 && ProbNNmu > 0.8 ;
    }
    bool tightNMuonSelection() const {
      return 
	p4().Pt() > 5e3 
	/* && ProbNNghost<0.5 */
	&& ProbNNmu>0.8
	//&& CaloEcalE/P() < 0.02 
	;
    }
    bool defaultElectronSelection() const {
      const float invp = std::abs( TRACK_qOverP ) ;
      return CaloEcalE * invp > 0.1 && CaloHcalE * invp < 0.05 ;
    }

    
    double ptcone() const {
      double iso = Iso.IT5/256. ;
      return p4().Pt()*(1.0/iso - 1.0) ;
    }

  public:
    float TRACK_CHI2NDOF ;
    unsigned char TRACK_NDOF ;
    float TRACK_VeloCHI2NDOF ;
    float TRACK_TCHI2NDOF ;
    float TRACK_MatchCHI2 ;
    float TRACK_GhostProb ;
    unsigned char TRACK_NumVeloPhi   ;
    unsigned char TRACK_NumVeloR     ;
    unsigned char TRACK_NumVeloHoles ;
    unsigned char TRACK_NumTTHits    ;
    unsigned char TRACK_NumITHits    ;
    unsigned char TRACK_NumOTHits    ;
    unsigned char TRACK_NumTLayers   ;
    unsigned char TRACK_NumTHoles    ;
    unsigned char TRACK_NumMuon      ;
    unsigned char TRACK_NumMuonLayers  ;
    bool TRACK_InPV ;
    char TRACK_PVindex ; // index of the closest PV in the PV list (-1 means no PV)
    float TRACK_IP ;
    float TRACK_IPCHI2 ;
    float ProbNNe ;    
    float ProbNNmu ;   
    float ProbNNghost ; 
    bool hasMuon ;    
    bool isMuon ;     
    bool isMuonTight ;
    bool isMuonLoose ;     
    bool hasCalo ;    
    bool InAccEcal ;
    float CaloEcalE ;
    float EcalPIDe  ;
    float EcalPIDmu ;
    bool InAccHcal ;
    float CaloHcalE ;
    IsolationInfo Iso ;
    float TRACK_qOverP ;
    float TRACK_qOverPerr ;
    float TRACK_zFirstHit ;
    float TRACK_VeloCharge ;
    
    //ClassDef(TrackParticle,1) ;
  } ;
}

#endif
