/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef __DECISIONINFO_H__
#define __DECISIONINFO_H__


namespace WMuMu {

  const std::vector<std::string> StrippingLines = {
    "StrippingWMuLineDecision",
    "StrippingZ02MuMuLineDecision",
    "StrippingDY2MuMuLine1Decision",
    "StrippingDY2MuMuLine2Decision",
    "StrippingDY2MuMuLine3Decision",
    "StrippingDY2MuMuLine4Decision",
    "StrippingDY2MuMuLine1HltDecision",
    "StrippingDY2MuMuLine2HltDecision",
    "StrippingA1MuMuLineDecision",
    "StrippingA2MuMuLineDecision",
    "StrippingA1MuMuSameSignLineDecision",
    "StrippingA2MuMuSameSignLineDecision",
    "StrippingLowMultDiMuonLineDecision",
    "StrippingLowMultMuonLineDecision",
    "StrippingExoticaPrmptDiMuonHighMassLineDecision",
    "StrippingExoticaDisplDiMuonNoPointLineDecision",
    "StrippingExoticaQuadMuonNoIPLineDecision",
    "StrippingHltQEEExoticaPrmptDiMuonHighMassLineDecision",
    "StrippingHltQEEJetsDiJetMuMuLineDecision",
    "StrippingHltQEEJetsDiJetSVMuLineDecision",
    "StrippingMuMuSSLine2Decision",
    "StrippingWmuJetsLineDecision",
    "StrippingWMuLowLineDecision",
    "StrippingMuMuSSLine3Decision",
    "StrippingFullDSTDiMuonDiMuonHighMassLineDecision",
    "StrippingA1MuMuA1MuMuSameSignLineDecision",
    "StrippingWeLineDecision",
    "StrippingDY2eeLine3",
    "StrippingDY2eeLine4",
    "StrippingFullDiJetsLineDecision",
    "StrippingHltQEEJetsDiJetLineDecision",
    "StrippingHltQEEJetsDiJetSVLineDecision",
    "StrippingHltQEEJetsDiJetSVSVLineDecision",
    "StrippingMicroDiJetsLineDecision",
    "StrippingTaggedJetsJetPairLineDecision",
    "StrippingDisplVerticesJetSingleHighMassDecision",
    "StrippingDisplVerticesJetSingleLowMassDecision",
    "StrippingTaggedJetsDiJetPairLineDecision",
    "StrippingTaggedJetsJetPairLineExclusiveDiJetDecision",
    "StrippingWeJetsLineDecision"
    //"StrippingStreamBhadronEvent"
  } ;

  const std::vector<std::string> L0Lines = {
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0CALODecision",
    "L0Global",
    "L0DiEM,lowMultDecision",
    "L0DiHadron,lowMultDecision",
    "L0DiMuon,lowMultDecision",
    "L0DiMuonNoSPDDecision",
    "L0Electron,lowMultDecision",
    "L0ElectronHiDecision",
    "L0ElectronNoSPDDecision",
    "L0HadronNoSPDDecision",
    "L0HighSumETJetDecision",
    "L0MUON,minbiasDecision",
    "L0Muon,lowMultDecision",
    "L0MuonNoSPDDecision",
    "L0Photon,lowMultDecision",
    "L0PhotonHiDecision",
    "L0PhotonNoSPDDecision"
  } ;
  
  const std::vector<std::string> Hlt1Lines = {
    "Hlt1SingleMuonHighPTDecision",
    "Hlt1SingleMuonNoIPDecision",
    "Hlt1DiMuonLowMassDecision",
    "Hlt1DiMuonHighMassDecision",
    "Hlt1DiMuonNoIPDecision",
    "Hlt1TrackMuonDecision",
    "Hlt1SingleElectronNoIPDecision",
    "HLT1CEPVeloCutDecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1TrackMVADecision",
    "Hlt1DiProtonDecision",
    "Hlt1CalibHighPTLowMultTrksDecision",
    "Hlt1MBNoBiasDecision",
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackAllL0TightDecision",
    "Hlt1TrackPhotonDecision",
    "Hlt1DiMuonNoL0Decision"
  } ;
  
  const std::vector<std::string> Hlt2Lines = {
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2SingleMuonLowPTDecision",
    "Hlt2SingleMuonVHighPTDecision",
    "Hlt2DiMuonDY1Decision",
    "Hlt2DiMuonDY2Decision",
    "Hlt2DiMuonDY3Decision",
    "Hlt2DiMuonDY4Decision",
    "Hlt2DiMuonZDecision",
    "Hlt2DiMuonJPsiDecision",
    "Hlt2DiMuonPsi2SDecision",
    "Hlt2DiMuonBDecision",
    "Hlt2DiMuonDetachedDecision",
    "Hlt2DiMuonDetachedHeavyDecision",
    "Hlt2DiMuonDetachedJPsiDecision",
    "Hlt2DiMuonDetachedPsi2SDecision",
    "Hlt2DiMuonJPsiHighPTDecision",
    "Hlt2DiMuonPsi2SHighPTDecision",
    "Hlt2DiMuonSoftDecision",
    "Hlt2EWDiMuonDY1Decision"
    "Hlt2TriMuonDetachedDecision",
    "Hlt2TriMuonTauDecision",
    "Hlt2LowMultMuonDecision",
    "Hlt2EWSingleElectronVHighPtDecision",
    "Hlt2EWDiElectronDYDecision",
    "Hlt2EWDiElectronHighMassDecision",
    "Hlt2EWDiMuonDY1Decision",
    "Hlt2MBNoBiasDecision",
    "Hlt2PassThroughDecision",
    "Hlt2TransparentDecision",
    "Hlt2SingleTFElectronDecision",
    "Hlt2SingleTFVHighPtElectronDecision",
    "Hlt2Topo2BodyDecision",
    "Hlt2Topo3BodyDecision",
    "Hlt2Topo4BodyDecision",
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo4BodyBBDTDecision",
    "Hlt2TopoMu2BodyDecision",
    "Hlt2TopoMu3BodyDecision",
    "Hlt2TopoMu4BodyDecision",
    "Hlt2TopoE2BodyDecision",
    "Hlt2TopoE3BodyDecision",
    "Hlt2TopoE4BodyDecision",
    "Hlt2TopoMuE2BodyDecision",
    "Hlt2TopoMuMu2BodyDecision",
    "Hlt2TopoMuMu3BodyDecision",
    "Hlt2TopoMuMuDDDecision",
    "Hlt2DisplVerticesSingleDecision",
    "Hlt2DisplVerticesSingleHighFDDecision",
    "Hlt2DisplVerticesSingleHighMassDecision",
    "Hlt2JetsDiJetDecision",
    "Hlt2JetsDiJetSVDecision",
    "Hlt2JetsDiJetSVLowPtDecision",
    "Hlt2JetsDiJetSVSVDecision",
    "Hlt2JetsDiJetSVMuDecision",
    "Hlt2JetsDiJetMuMuDecision",
    "Hlt2JetsDiJetSVSVLowPtDecision",
    "Hlt2JetsJetLowPtDecision"
  } ; // WARNING: If you add beyond 64 entries, something will go very wrong.

  template<typename T>
  class TisTosInfo
  {
  public:
    typedef T value_type ;
    T tis ;
    T tos ;
    T tus ;
  public:
  TisTosInfo() : tis(0),tos(0),tus(0) {}
  } ;

  using StrippingBits = ULong64_t ;
  using L0Bits = UInt_t ;
  using Hlt1Bits = UInt_t ;
  using Hlt2Bits = ULong64_t ;
  
  class ParticleTisTos
  {
  public:
    TisTosInfo<L0Bits> L0 ;
    TisTosInfo<Hlt1Bits> hlt1 ;
    TisTosInfo<Hlt2Bits> hlt2 ;
  } ;

  // returns the bit corresponding to a certain line. returns lines.size in case the line does not exist
  size_t lineIndex( const std::vector<std::string>& alllines, const std::string& line ) ;
  // creates a mask for a particular trigger line
  ULong64_t createMask( const std::vector<std::string>& lines, const std::string& line ) ;
  // printe the fired lines from a bit mask
  void printFiredLines( const std::vector<std::string>& lines, ULong64_t bits ) ;
}

#endif
