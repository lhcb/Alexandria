/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TrackTupleMaker.cpp,v 1.3 2008/11/03 12:08:01 wouter Exp $
// Include files

// from Gaudi
#include "GaudiKernel/StatusCode.h"
#include <vector>
#include <string>
#include <map>

// add a parser that is missing in Gaudi Parsers, but actually just works:
namespace Gaudi
{
  namespace Parsers
  {
    GAUDI_API StatusCode parse( std::map<int, std::vector<std::string>>& result, const std::string& input ) ;
  }
}

// local
#include "../WMuMu/Event.h"
#include "../WMuMu/DecisionInfo.h"
#include "../WMuMu/WToMuMuXContainer.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IJetTagTool.h"
#include "Kernel/ITriggerTisTos.h"
#include "Kernel/IParticleCombiner.h"
#include "Kernel/PFParticle.h"

#include "Event/ODIN.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/PrimaryVertex.h"
#include "Event/HltDecReports.h"
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/RecSummary.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VeloCluster.h"
//#include "Event/BeamParameters.h"

#include "TrackKernel/TrackVertexUtils.h"
#include "TrackKernel/TrackCloneData.h"
#include "TrackKernel/TrackStateVertex.h"

#include "VeloDet/DeVelo.h"

#include "Event/L0DUReport.h"
#include "Event/HltDecReports.h"

#include "Relations/Relation1D.h"
#include "Relations/IRelation.h"
#include "Kernel/IRelateJets.h"

#include "MCEventFiller.h"

#include "MCInterfaces/IPrintMCDecayTreeTool.h"

// ROOT stuff
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include "TSystem.h"

//#include <boost/lambda/bind.hpp>
//#include <boost/lambda/lambda.hpp>
//using namespace boost::lambda;

#include <stdlib.h>
#include <string>

namespace { 
  ULong64_t fillDecisionBits( const std::vector<std::string>& lines, const LHCb::HltDecReports& decreports )
  {
    ULong64_t rc(0) ;
    unsigned int index(0) ;
    for( const auto& m : lines ) {
      auto it = decreports.find( m ) ;
      if ( it != decreports.end() && it->second.decision() )  rc |= (ULong64_t(1) << index) ;
      ++index ;
    }
    return rc ;
  }

  struct ParticleBranch
  {
    std::vector<std::string> locations ;
    //std::vector< WMuMu::Particle > particles ;
    WMuMu::ParticleVector particles;
  } ;

  struct EqualTrack
  {
    bool operator() (const LHCb::Track* lhs, const LHCb::Track* rhs)
    {
      return (lhs == rhs) || (lhs->lhcbIDs()==rhs->lhcbIDs()) ;
    }
  } ;

  // we better use the loki functor but I cannot find it now!
  uint32_t uniqueTrackID( const LHCb::Track& track )
  {
    // create a 'hash' from the velo IDs on the track
    const auto& lhcbids = track.lhcbIDs() ;
    const uint32_t* begin = reinterpret_cast<const uint32_t*>(&(*(lhcbids.begin()))) ;
    const uint32_t* end   = reinterpret_cast<const uint32_t*>(&(*(lhcbids.end()))) ;
    BloomFilterImpl::HashFNV1a<uint32_t, uint32_t> hashobj;
    // hashing a vector of objects - slightly ugly, since we need to get the
    // initial value of an "empty" hash from deep within the implementation...
    uint32_t hash = BloomFilterImpl::__doFNV1a<uint32_t>::hashinit;
    const uint32_t* it = begin ;
    while( it < end ) { hash = hashobj(*it, hash); ++it; }
    return hash ;
  }
  
  struct EqualTrackParticle
  {
    const LHCb::Particle* lhs ;
    EqualTrackParticle( const LHCb::Particle* _lhs) : lhs(_lhs) {}
    bool operator()( const LHCb::Particle* rhs ) const
    {
      // this is very slow ...
      return lhs->particleID() == rhs->particleID() &&
	( (lhs == rhs) || lhs->proto()==rhs->proto() ||
	  lhs->proto()->track()==rhs->proto()->track() ||
	  lhs->proto()->track()->lhcbIDs()==rhs->proto()->track()->lhcbIDs() ) ;
    }
    bool operator()( const std::pair< const LHCb::Particle*, unsigned int>& rhspair) const {
      return this->operator()(rhspair.first) ;
    }
  } ;


  // compute the IP and IPChi2 properly, by unbiasing the PVs.
  template<class Float, class Int>
  bool computeTrackIP( const LHCb::Track& track,
		       const std::vector<LHCb::PrimaryVertex>& pvs,
		       Float& ip,
		       Float& ipchi2,
		       Int& pvindex )
  {
    bool inPV = false ;
    pvindex = -1 ;
    if( pvs.empty() ) {
      ip = 0 ;
      ipchi2 = 0 ;
    } else {
      std::vector<LHCb::PrimaryVertex::VeloSegmentID> ids = { LHCb::PrimaryVertex::uniqueVeloSegmentID(track)} ;
      ip = 1e9 ;
      ipchi2 = 1e9 ;
      const LHCb::State& state = track.firstState() ;
      for(size_t i=0; i<pvs.size(); ++i) {
	const auto& pv = pvs[i] ;
	// unbiase this PV
	Gaudi::XYZPoint pos ;
	Gaudi::SymMatrix3x3 cov ;
	int nremoved = pv.unbiasedPosition( ids, pos, cov ) ;
	if( nremoved > 0 ) {
	  inPV = true ;
	} else {
	  pos = pv.position() ;
	  cov = pv.covMatrix() ;
	}
	double thisipchi2 = LHCb::TrackVertexUtils::vertexChi2( state, pos, cov ) ;
	double tx = state.tx() ;
	double ty = state.ty() ;
	double dz = pos.z() - state.z() ;
	double dx = state.x() + dz * tx - pos.x() ;
	double dy = state.y() + dz * ty - pos.y() ;
	double thisip = std::sqrt( (dx*dx+dy*dy)/(1.0+tx*tx+ty*ty) ) ;
	if( pvindex <0 || ipchi2 > thisipchi2) ipchi2 = thisipchi2 ;
	if( pvindex <0 || ip > thisip ) {
	  ip=thisip ;
	  pvindex = i ;
	}
      }
    }
    /*
    if( inPV ) {
      std::cout << "Found track in PV" << std::endl ;
    } else {
      std::cout << "Why is this track not in any PV??" << ids[0] << std::endl ;
      // find the closest track in tx and ty ... can we do that? will it help, given that it is velo?
      uint32_t closesttrack(0) ;
      double dtx(0), dty(0) ;
      size_t ntrk(0) ;
      for(size_t i=0; i<pvs.size(); ++i) {
	for( const auto& trk : pvs[i].tracks() ) {
	  ++ntrk ;
	  double adtx = trk.state()[2] - state.tx() ;
	  double adty = trk.state()[3] - state.ty() ;
	  if( closesttrack==0 || (adtx*adtx+adty*adty < dtx*dtx+dty*dty ) ) {
	    closesttrack = trk.id() ;
	    dtx = adtx ;
	    dty = adty ;
	  }
	}
      }
      std::cout << "closest track: " << pvs.size() << " " <<ntrk << " " 
		<< closesttrack << " " << dtx << " " << dty << std::endl ;
    }
    */
    return inPV ;
  }
}

// now the implementation of the parser:
#include "GaudiKernel/ParsersFactory.h"
StatusCode Gaudi::Parsers::parse( std::map<int, std::vector<std::string>>& result, const std::string& input )
{
  return Gaudi::Parsers::parse_( result, input );
}
   
class WMuMuTupleMaker : public GaudiAlgorithm
{
 public:
  using TisTosMap = std::map<int, std::vector<std::string> > ;
  //using NamedTisTosMap = std::map<std::string, std::vector<std::string> > ;
public:
  WMuMuTupleMaker(const std::string& name,ISvcLocator* pSvcLocator) ;
  virtual ~WMuMuTupleMaker() ;
  virtual StatusCode execute() override ;
  virtual StatusCode initialize() override ;
  virtual StatusCode finalize() override ;
  typedef LHCb::Particle::Range PFParticleRange ;
  
  void fillMCEvent() ;
  void fillTrackInfo( const LHCb::Particle& part,
		      WMuMu::TrackParticle& p,
		      const std::vector<LHCb::PrimaryVertex>& pvs,
		      const PFParticleRange& pfparticles) const ;
  void fillCompositeInfo(const LHCb::Particle& part,
			 WMuMu::CompositeParticle& p,
			 const std::vector<LHCb::PrimaryVertex>& pvs,
			 const PFParticleRange& pfparticles ) const ;
  void fillIsolationInfo( const LHCb::Particle& part,
			  WMuMu::IsolationInfo& info,
			  const PFParticleRange& pfparticles ) const ;
  void fillJetInfo( const LHCb::Particle& part,
		    WMuMu::JetParticle& jet,
		    const std::vector<LHCb::PrimaryVertex>& pvs) ;
  void fillTisTosInfo(  const LHCb::Particle& part, WMuMu::Particle& jet) const ;
  void fillPVs( const std::vector<LHCb::PrimaryVertex>& pvs,
		const LHCb::Track::Range& tracks) const ;
  void clearEvent() ;
  unsigned int addTrackParticle( const LHCb::Particle& trackparticle,
				 const std::vector<LHCb::PrimaryVertex>& pvs,
				 const PFParticleRange& pfparticles) const ;
  
private:
  
  // jobOptions
  std::string m_muonContainer; // Input Tracks container location
  std::string m_electronContainer; // Input Tracks container location
  std::string m_jetContainer; // Input Tracks container location
  std::vector<std::string> m_WMuMuXLocations  ;
  std::string m_pfParticleLocation ;
  std::string m_outputfilename ;
  std::map<std::string, std::vector<std::string> > m_particlesToStore ;
  bool m_storeAllEvents ;
  TisTosMap m_L0TisTosLines ;
  TisTosMap m_Hlt1TisTosLines ;
  TisTosMap m_Hlt2TisTosLines ;
  
  double m_minMuonPTForParticleFlow ; // minimal muon pT before we run PF. speeds up the jobs a lot.

  // tools etc
  ToolHandle<IParticleCombiner> m_particleCombiner ;
  ToolHandle<IDistanceCalculator> m_distcalc ;
  ToolHandle<IJetTagTool> m_jetTagger ;
  ToolHandle<ITriggerTisTos> m_L0TisTosTool;
  ToolHandle<ITriggerTisTos> m_TriggerTisTosTool;
  ToolHandle<ITriggerTisTos> m_Hlt1TisTosTool;
  ToolHandle<ITriggerTisTos> m_Hlt2TisTosTool;
  ILHCbMagnetSvc* m_magfieldsvc;
  DeVelo* m_velodet ;

  // data
  TFile* m_file ;
  TTree* m_tree ;
  std::vector<ParticleBranch> m_particleBranches ;
  mutable WMuMu::Event* m_event ;
  mutable WMuMu::WToMuMuXContainer *m_WToMuMuXContainer ;
      
  // cache
  mutable std::vector< std::pair<const LHCb::Particle*, unsigned int> > m_trackToTrackIndex ;
  mutable Gaudi::XYZPoint m_beamline ;
  
  // some data to report at end of job which trigger/stripping lines are missing
  std::map<std::string,int> m_missingstrippinglines ;
  std::map<std::string,int> m_missingtriggerlines ;
} ;


DECLARE_COMPONENT( WMuMuTupleMaker )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
WMuMuTupleMaker::WMuMuTupleMaker( const std::string& name,
				  ISvcLocator* pSvcLocator ) 
:
GaudiAlgorithm( name , pSvcLocator ),
  m_particleCombiner("LoKi::VertexFitter",this),
  m_distcalc("LoKi::DistanceCalculator",this),
  m_jetTagger("LoKi::BDTTag"),
  m_L0TisTosTool("L0TriggerTisTos",this),
  m_TriggerTisTosTool("TriggerTisTos",this),
  m_Hlt1TisTosTool("Hlt1TriggerTisTos",this),
  m_Hlt2TisTosTool("Hlt2TriggerTisTos",this)
{
  declareProperty( "MuonLocation", m_muonContainer );
  declareProperty( "ElectronLocation", m_electronContainer );
  declareProperty( "JetLocation", m_jetContainer );
  declareProperty( "PFLocation", m_pfParticleLocation = "Phys/PFParticles/Particles") ;
  declareProperty( "WMuMuXLocations", m_WMuMuXLocations );
  declareProperty( "OutputFileName", m_outputfilename = "WMuMuXTuple.root" ) ;
  declareProperty( "MinMuonPTForParticleFlow",m_minMuonPTForParticleFlow = 15*Gaudi::Units::GeV ) ;
  declareProperty( "ParticlesToStore", m_particlesToStore ) ;
  declareProperty( "StoreAllEvents", m_storeAllEvents = false ) ;
  declareProperty( "L0TisTosMap", m_L0TisTosLines ) ;
  declareProperty( "Hlt1TisTosMap", m_Hlt1TisTosLines ) ;
  declareProperty( "Hlt2TisTosMap", m_Hlt2TisTosLines ) ;
}

//=============================================================================
// Destructor
//=============================================================================
WMuMuTupleMaker::~WMuMuTupleMaker() {}

//=============================================================================
// ::initialize()
//=============================================================================
StatusCode WMuMuTupleMaker::initialize()
{
  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) { return sc; }

  sc = m_particleCombiner.retrieve() ;
  if( sc.isFailure() ) { return sc ; }

  sc = m_distcalc.retrieve() ;
  if( sc.isFailure() ) { return sc ; }

  m_magfieldsvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  if(! m_magfieldsvc ) return StatusCode::FAILURE ;

  m_velodet = getDet<DeVelo>(  DeVeloLocation::Default ) ;
  
  // TisTosTools
  sc = m_TriggerTisTosTool.retrieve() ;
  if( sc.isFailure() ) { return sc ; }
  sc = m_L0TisTosTool.retrieve() ;
  if( sc.isFailure() ) { return sc ; }
  sc = m_Hlt1TisTosTool.retrieve() ;
  if( sc.isFailure() ) { return sc ; }
  sc = m_Hlt2TisTosTool.retrieve() ;
  if( sc.isFailure() ) { return sc ; }
  
  // copy setting of TriggerTisTos to settings of Hlt1, Hlt2
  m_Hlt1TisTosTool->setTISFrac( m_TriggerTisTosTool->getTISFrac() );
  m_Hlt1TisTosTool->setTOSFrac( m_TriggerTisTosTool->getTOSFrac() );
  m_Hlt2TisTosTool->setTISFrac( m_TriggerTisTosTool->getTISFrac() );
  m_Hlt2TisTosTool->setTOSFrac( m_TriggerTisTosTool->getTOSFrac() );
  
  //sc = m_jetTagger.retrieve() ;
  //if( sc.isFailure() ) { return sc ; }

  const char* dir = getenv("WMUMUEVENTTUPLEROOT") ;
  const char* user = getenv("USER") ;
  const char* tmpdir = getenv("TMPDIR") ;
  if( tmpdir ) {
    gSystem->SetBuildDir( tmpdir ) ;
  } else {
    char usertmpdir[256] ;
    sprintf( usertmpdir, "/tmp/%s", user);
    gSystem->SetBuildDir( usertmpdir ) ;
  }
  if( dir ) {
    char command[256] ;
    sprintf( command, ".L %s/WMuMu/Event.cpp++ ", dir);
    info() << "Loading shared library: " << command << endmsg ;
    gROOT->ProcessLine(command);
  } else {
    Warning("Evironment not properly set: cannot load shared library").ignore() ;
  }

  auto curdir = gDirectory ;
  m_file  = new TFile(m_outputfilename.c_str(),"RECREATE") ;
  m_event = new WMuMu::Event() ;
  m_tree  = new TTree("events","") ;
  //m_tree->Branch("event","WMuMu::Event",&m_event) ;
  m_tree->Branch("event",m_event) ;

  //
  if( !m_WMuMuXLocations.empty() ) {
    info() << "Creating WMuMuX branch" << endmsg ;
    m_WToMuMuXContainer = new WMuMu::WToMuMuXContainer{} ;
    m_tree->Branch("WMuMuX",m_WToMuMuXContainer) ;
  } else {
    m_WToMuMuXContainer = 0 ;
  }
  
  // create branches for all particle lists
  m_particleBranches.reserve( m_particlesToStore.size() ) ;
  for( const auto& l : m_particlesToStore ) {
    m_particleBranches.push_back( ParticleBranch() ) ;
    ParticleBranch& pl = m_particleBranches.back() ;
    pl.locations = l.second ;
    info() << "Creating branch: " << l.first.c_str() << endmsg ;
    m_tree->Branch(l.first.c_str(),&pl.particles) ;
  }
  curdir->cd() ;
  
  return sc ;
}

//=============================================================================
// ::finalize()
//=============================================================================
StatusCode WMuMuTupleMaker::finalize()
{
  m_particleCombiner.release().ignore() ;
  m_distcalc.release().ignore() ;
  m_TriggerTisTosTool.release().ignore() ;
  m_L0TisTosTool.release().ignore() ;
  m_Hlt1TisTosTool.release().ignore() ;
  m_Hlt2TisTosTool.release().ignore() ;

  if(m_file) {
    m_file->Write() ;
    m_file->Close() ;
  }

  info() << "Missing stripping decisions: " << endmsg ;
  for(const auto& it: m_missingstrippinglines)
    info() << it.first << " " << it.second << endmsg ;
  
  info() << "Missing trigger decisions: " << endmsg ;
  for(const auto& it: m_missingtriggerlines)
    info() << it.first << " " << it.second << endmsg ;
  
  return StatusCode::SUCCESS;
}

namespace {
  struct SortDecreasingPT
  {
    bool operator()(const LHCb::Particle* lhs, const LHCb::Particle* rhs) {
      return lhs->momentum().Pt() > rhs->momentum().Pt() ;
    }
  } ;

  template<class Range>  
  std::vector<const LHCb::Particle*> SortedVector( const Range& range) {
    std::vector<const LHCb::Particle*> rc(range.begin(),range.end()) ;
    std::sort(rc.begin(),rc.end(),SortDecreasingPT()) ;
    return rc ;
  }

  template<class T, class Container>
  int indexInList(const T& x, const Container& v) {
    auto it = std::find( v.begin(), v.end(), x ) ;
    return it != v.end() ? int(std::distance(v.begin(),it)) : -2 ;
  }
}

void WMuMuTupleMaker::clearEvent()
{
  m_event->clear() ;
  if( m_WToMuMuXContainer ) m_WToMuMuXContainer->clear() ;
  for( auto& pl : m_particleBranches ) pl.particles.clear() ;
  // clear caches
  m_trackToTrackIndex.clear() ;
}


//=============================================================================
// ::execute()
//=============================================================================
StatusCode WMuMuTupleMaker::execute()
{  
  // clear the lists in the event.
  clearEvent() ;
  m_event->index++ ;

  // update the position of the beamline by taking the velo center
  m_beamline = 0.5*(Gaudi::XYZVector{m_velodet->halfBoxOffset( DeVelo::LeftHalf)} +
		    Gaudi::XYZVector{m_velodet->halfBoxOffset( DeVelo::RightHalf)} ) ;
  m_event->beamlineX = m_beamline.x() ;
  m_event->beamlineY = m_beamline.y() ;

  /*
  auto beamparameters = get<const LHCb::BeamParameters>(LHCb::BeamParametersLocation::Default) ;
  if(!beamparameters) {
    info() << "Cannot find beam parameter object" << endmsg ;
  } else {
    info() << "Beam parameters: " << beamparameters->beamSpot() << endmsg ;
  }
  */
    
  // determine whether or not we will store this event. could have
  // done this with a filter, but need an or between MC truth and
  // strip filter.
  const LHCb::MCHeader* mcheader =  getIfExists<LHCb::MCHeader>(LHCb::MCHeaderLocation::Default) ;
  
  const auto strippingreports = getIfExists<LHCb::HltDecReports>("/Event/Strip/Phys/DecReports") ;
  m_event->stripping = strippingreports ? fillDecisionBits( WMuMu::StrippingLines, *strippingreports ) : 0 ;
  
  //if( !mcheader && !passesStrippingLine) return StatusCode::SUCCESS ;
 
  const LHCb::HltDecReports* hlt1reports = getIfExists<LHCb::HltDecReports>(evtSvc(),LHCb::HltDecReportsLocation::Hlt1Default);
  m_event->hlt1 = hlt1reports ? fillDecisionBits( WMuMu::Hlt1Lines, *hlt1reports ) : 0 ;

  const LHCb::HltDecReports* hlt2reports = getIfExists<LHCb::HltDecReports>(evtSvc(),LHCb::HltDecReportsLocation::Hlt2Default);
  m_event->hlt2 = hlt2reports ? fillDecisionBits( WMuMu::Hlt2Lines, *hlt2reports ) : 0 ;

  //const auto L0report = getIfExists<LHCb::L0DUReport>(evtSvc(),LHCb::L0DUReportLocation::Default);
  const auto L0reports = getIfExists<LHCb::HltDecReports>(evtSvc(),"HltLikeL0/DecReports");
  m_event->L0 = L0reports ? fillDecisionBits( WMuMu::L0Lines, *L0reports ) : 0 ;
  
  const LHCb::L0DUReport* L0DUreport = getIfExists<LHCb::L0DUReport>(evtSvc(),LHCb::L0DUReportLocation::Default);
  if( L0DUreport ) {
    for(int i=0; i<5; ++i) m_event->L0SumET[i] = L0DUreport->sumEt(i-2) ;
    m_event->L0nSPDhits = L0DUreport->dataDigit("Spd(Mult)") ;
  }
  
  // check if there is a particle in any of the 'selected' particle
  // lists. because if there is, we would like to make sure we know
  // how it was triggered.
  size_t numparticles(0) ;
  for( const auto& location : m_WMuMuXLocations ) {
    LHCb::Particle::Range Ws = getIfExists<LHCb::Particle::Range>( location );
    numparticles += Ws.size() ;
  }
  for( auto& pl : m_particleBranches ) {
    // get the list from the event
    for( const auto& loc : pl.locations ) {
      LHCb::Particle::Range particles = getIfExists<LHCb::Particle::Range>( loc );
      numparticles += particles.size() ;
    }
  }

  // Fill info from the odin
  const auto* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);
  if( odin ) {
    m_event->runnumber = odin->runNumber() ;
    m_event->eventnumber = odin->eventNumber() ;
    m_event->bxid   = odin->bunchId() ;
    m_event->bxtype = odin->bunchCrossingType() ;
  }
  
  // Fill the polarity of the field
  m_event->bFieldPolarity = m_magfieldsvc->isDown() ? -1 : +1 ;

  // Fill some info from RecSummary
  const LHCb::RecSummary* recevent = get<LHCb::RecSummary>(LHCb::RecSummaryLocation::Default) ;
  if(recevent) {
    m_event->nLongTracks = recevent->summaryData()[LHCb::RecSummary::nLongTracks] ;
    m_event->nDownstreamTracks = recevent->summaryData()[LHCb::RecSummary::nDownstreamTracks] ;
    m_event->nUpstreamTracks = recevent->summaryData()[LHCb::RecSummary::nUpstreamTracks] ;
    m_event->nVeloTracks = recevent->summaryData()[LHCb::RecSummary::nVeloTracks] ;
    m_event->nTTracks    =  recevent->summaryData()[LHCb::RecSummary::nTTracks] ;
    m_event->nBackTracks = recevent->summaryData()[LHCb::RecSummary::nBackTracks] ;
    m_event->nMuonTracks = recevent->summaryData()[LHCb::RecSummary::nMuonTracks] ;
    m_event->nVeloClusters = recevent->summaryData()[LHCb::RecSummary::nVeloClusters] ;
    m_event->nITClusters = recevent->summaryData()[LHCb::RecSummary::nITClusters] ;
    m_event->nTTClusters = recevent->summaryData()[LHCb::RecSummary::nTTClusters] ;
    m_event->nOTClusters = recevent->summaryData()[LHCb::RecSummary::nOTClusters] ;
    m_event->nSPDhits = recevent->summaryData()[LHCb::RecSummary::nSPDhits] ;
  }
  
  // fill the MC truth, if it exists
  if( mcheader ) fillMCEvent() ;

  // Get the muons. Only fill the rest if we have a muon of a certain pT, to save time and disk space
  std::vector<const LHCb::Particle*> muons ;
  if( !m_muonContainer.empty() ) muons = SortedVector(getIfExists<LHCb::Particle::Range>( m_muonContainer ) ) ;
  // Get the electrons
  std::vector<const LHCb::Particle*> electrons ;
  if( !m_electronContainer.empty() ) 
    electrons = SortedVector(getIfExists<LHCb::Particle::Range>( m_electronContainer )) ;

    
  if( //m_muonContainer.empty() ||
      m_storeAllEvents ||
      numparticles >0 ||
      /* passesStrippingLine || */
      (!muons.empty() && 
       muons.front()->momentum().Pt() > m_minMuonPTForParticleFlow ) ||
      (!electrons.empty() && 
       electrons.front()->momentum().Pt() > m_minMuonPTForParticleFlow )
      ) {

    // Report some warnings if we select an event but miss a stripping/trigger decision
    if( m_event->stripping==0 && strippingreports ) 
      for( const auto& m : strippingreports->decisionNames())
	m_missingstrippinglines[m] += 1 ;
    if( m_event->hlt1==0 && hlt1reports )
      for( const auto& m : *hlt1reports )
	if ( m.second.decision() ) m_missingtriggerlines[m.first] += 1 ;
    if( m_event->hlt2==0 && hlt2reports )
      for( const auto& m : *hlt2reports )
	if ( m.second.decision() ) m_missingtriggerlines[m.first] += 1 ;
    if( m_event->L0==0 && L0reports )
      for( const auto& m : *L0reports )
	if ( m.second.decision() ) m_missingtriggerlines[m.first] += 1 ;
    
    // Get the PF particle list, but only if we actually hav a high pT muon. Otherwise the jobs run too slow.

    LHCb::Particle::Range pfparticles = getIfExists<LHCb::Particle::Range>( m_pfParticleLocation) ;
    //warning() << "pfparticles size: " << pfparticles.size() << endmsg ;

 

    // get the velo hits, if they exist ...
    //const LHCb::VeloLiteCluster::VeloLiteClusters* veloliteclusters =
    //  getIfExists<LHCb::VeloLiteCluster::VeloLiteClusters>(LHCb::VeloLiteClusterLocation::Default) ;
    const LHCb::VeloClusters* veloclusters =
      getIfExists<LHCb::VeloClusters>(LHCb::VeloClusterLocation::Default) ;
    // if(veloclusters)
    //   std::cout << "Number of velo clusters: "
    // 		<< veloclusters->size() << " "
    // 		<< veloliteclusters->size() << std::endl ;
    // else
    //   std::cout << "Cannot find velo clusters" << std::endl ;
    if( !veloclusters ) m_event->hasRawEvent = false ;
    
    // Get the default track list (perhaps better to use PF list?)
    LHCb::Track::Range tracks = get<LHCb::Track::Range>(LHCb::TrackLocation::Default) ;
    // Get the PVs
    LHCb::RecVertex::Range recpvs = getIfExists<LHCb::RecVertex::Range>(LHCb::RecVertexLocation::Primary) ;
    // Convert to class PrimaryVertex
    std::vector<LHCb::PrimaryVertex> pvs ;
    for( const auto& v : recpvs )  pvs.push_back(LHCb::PrimaryVertex(*v)) ;
    // fill all the PV info
    fillPVs( pvs, tracks ) ;

    // Add interesting particles to the particle list
    for( const auto& muon: muons ) 
      addTrackParticle( *muon, pvs, pfparticles ) ;
    for( const auto& electron: electrons) 
      addTrackParticle( *electron, pvs, pfparticles ) ;

    // Get the jets
    const auto jets = SortedVector(getIfExists<LHCb::Particle::Range>( m_jetContainer ));
    for( const auto& jet: jets) {
      m_event->jets.push_back( WMuMu::JetParticle(*jet) ) ;
      fillJetInfo( *jet, m_event->jets.back(),pvs ) ;
    }

     for( const auto& location : m_WMuMuXLocations ) {
      LHCb::Particle::Range Ws = getIfExists<LHCb::Particle::Range>( location );
      for( const auto& W: Ws) {
	m_WToMuMuXContainer->candidates.push_back( WMuMu::WToMuMuXCandidate() ) ;
	WMuMu::WToMuMuXCandidate& cand =  m_WToMuMuXContainer->candidates.back() ;
	// fill W particle info
	cand.W = WMuMu::CompositeParticle( *W ) ;
	fillCompositeInfo(*W,cand.W,pvs,pfparticles) ;

	// fill N particle info
	const LHCb::Particle* N(0), *Wmuon(0) ;
	for( const auto& dau: W->daughters() ) {
	  if( std::abs(dau->particleID().pid()) == 13 ) Wmuon = dau ;
	  else if( dau->daughters().size()>0 ) {//std::abs(dau->particleID().pid())==9900014 ) {
	    cand.N = WMuMu::CompositeParticle( *dau ) ;
	    fillCompositeInfo(*dau,cand.N,pvs,pfparticles) ;
	    N = dau ;
	  }
	}
	// set pointer to muon daughter
	// if(Wmuon) cand.Wmuon = addTrackParticle(*Wmuon,pvs,pfparticles) ;
	
	// this needs to be made a bit more safe:-)
	if( N ) {
	  const LHCb::Particle* Nelectron(0) ;
	  std::vector<const LHCb::Particle*> Nmuons ;
	  std::vector<const LHCb::Particle*> Njets ;
	  std::vector<const LHCb::Particle*> Npions ;
	  for( const auto& dau : N->daughters() ) 
	    if(      std::abs(dau->particleID().pid()) == 13 ) Nmuons.push_back(dau) ;
	    else if( std::abs(dau->particleID().pid()) == 11 ) Nelectron = dau ;
	    else if( dau->proto() && dau->proto()->track() )   Npions.push_back(dau) ;
	    else Njets.push_back(dau) ;
	  if( Njets.size()>0 ) {
	    cand.Njet1 = indexInList(Njets[0],jets) ;
	    if( Njets.size()>1) 
	      cand.Njet2 = indexInList(Njets[1],jets) ;
	  }
	  
	  if( Nmuons.size() > 0 && Wmuon) {
	    const LHCb::Particle* Nmuon = Nmuons[0] ;
	    // create a flag for the type of decay
	    bool rightsign = Wmuon->particleID().pid() * Nmuon->particleID().pid() < 0 ;
	    if(     Nelectron &&        rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuERS ;
	    else if(Nelectron &&       !rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuEWS ;
	    else if(Njets.size()==1 &&  rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuJetRS ;
	    else if(Njets.size()==1 && !rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuJetWS ;
	    else if(Njets.size()==2 &&  rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuDiJetRS ;
	    else if(Njets.size()==2 && !rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuDiJetWS ;
	    else if(Nmuons.size()==2)               cand.decaytype = WMuMu::WToMuMuXCandidate::MuMuMu ;
	    else if(!Npions.empty() &&  rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuPiRS ;
	    else if(!Npions.empty() && !rightsign ) cand.decaytype = WMuMu::WToMuMuXCandidate::MuPiWS ;
	    else                                    cand.decaytype = WMuMu::WToMuMuXCandidate::UnknownDecayType ;
	    
	    // temporarily create a dimuon candidate
	    LHCb::Particle mumuparticle=LHCb::Particle(LHCb::ParticleID(20)) ;
	    LHCb::Vertex mumuvertex ;
	    LHCb::Particle::ConstVector mumudaughters={ Wmuon, Nmuon} ;
	    m_particleCombiner->combine( mumudaughters,mumuparticle,mumuvertex ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
	    cand.mumu = WMuMu::CompositeParticle( mumuparticle ) ;
	    fillCompositeInfo( mumuparticle,cand.mumu,pvs,pfparticles) ;
	    
	    // compute kinematics with W mass constraint
	    const double Wmass = 80385. * Gaudi::Units::MeV ;
	    const Gaudi::LorentzVector& nmuonp4 = Nmuon->momentum() ;
	    const Gaudi::LorentzVector& wmuonp4 = Wmuon->momentum() ;
	    Gaudi::LorentzVector np4constrained ;
	    if ( Njets.size()>0 ) {
	      // W mass constraint is applied by scaling jet energy
	      Gaudi::LorentzVector jetp4;
	      for( const auto& j : Njets ) jetp4 += j->momentum() ;
	      const Gaudi::LorentzVector mumup4 = nmuonp4 + wmuonp4 ;
	      double jetmass2 = jetp4.M2() ;
	      double mumup4dotjetp4 = mumup4.Dot( jetp4 ) ;
	      const double dM2 = Wmass*Wmass - mumup4.M2() ;
	      double alpha = 1 ;
	      if(  dM2 > 0 ) 
		alpha = (std::sqrt( dM2*jetmass2 + mumup4dotjetp4 * mumup4dotjetp4 ) - mumup4dotjetp4 ) / jetmass2 ;
	      np4constrained = nmuonp4 + alpha * jetp4 ;
	    } else {
	      // W mass constraint is applied by computing the missing
	      // neutrino energy, assuming that it is parallel to the
	      // observed N momentum. this gives the minimal mass of the
	      // N system consistent with the W mass.
	      Gaudi::LorentzVector Np4;
	      for( const auto& dau : N->daughters() ) Np4 += dau->momentum() ;
	      const Gaudi::LorentzVector Wp4 = Np4 + wmuonp4 ;
	      const double dM2 = Wmass*Wmass - Wp4.M2() ;
	      if( dM2 > 0 ) {
		const Gaudi::XYZVector dir = Np4.Vect().Unit() ;
		double E = 0.5 * dM2 / (  Wp4.E() - Wp4.Vect().Dot(dir) ) ;
		const Gaudi::LorentzVector neutrinop4( dir.x()*E, dir.y()*E, dir.z()*E, E) ;
		np4constrained = Np4 + neutrinop4 ;
	      }
	    }
	      //const auto np4constrained = nxp4constrained + nmuonp4 ;
	    cand.Np4Wc = WMuMu::MomentumVector(np4constrained) ;
	    
	    // we could add here some stuff about decay angles.
	    
	    // to compute the lifetime, we actually need the PV of the muon
	    if( cand.W.dau1 >= 0 ) {
	      int pvindex = m_event->tracks[ cand.W.dau1 ].TRACK_PVindex ;
	      if( pvindex >= 0 ) {
		const LHCb::PrimaryVertex& pv =  pvs[pvindex] ;
		// we can compute this ourselves:
		const Gaudi::XYZVector Ndir = N->momentum().Vect().Unit() ;
		cand.Ndecaylength = (N->endVertex()->position() - pv.position()).Dot(Ndir) ;
		Gaudi::Vector3 jac ; 
		jac(0) = Ndir.x() ; jac(1) = Ndir.y(); jac(2) = Ndir.z() ;
		double decaylengthcov = ROOT::Math::Similarity( jac, pv.covMatrix() + N->endVertex()->covMatrix() ) ;
		cand.Ndecaylengtherr = std::sqrt( decaylengthcov ) ;
		// or just use distance calculator
		//double dist(0), disterr(0) ;
		//m_distcalc->projectedDistance( N, &pv, dist, disterr ) ;	      
		
		double ip(9999), ipchi2(9999) ;
		m_distcalc->distance( N, &pv, ip, ipchi2 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
		cand.Nip = ip ;
		cand.Nipchi2 = ipchi2 ;
	      }	      
	    }
	    
	    /*
	    // Another note: do we really want the W to fly? it just leads to
	    // strongly correlated lifetimes.  
	    // get the PV of the muon. note that the PV is strongly biased
	    // ... we probably want to refit it!
	    
	    // what information do we want:
	    
	    // 1. chi2 of the Neutrino vertex. if X is a jet, then collect all long
	    // tracks and make a vertex with that. do we really want to ignore
	    // velo? let's make two chi2s.
	    
	    //- extract all the tracks with velo info
	    //- extract all the long tracks
	    //- combine with the muon and make a vertex. which tool? particles or tracks? with or without Tukey weighting?
	    //- it may be useful to have some MC information
	    //  - note that we already solved this problem at least once: can we reuse the jet vertex tool?
	    
	    // 2. decaylength of the Neutrino. We know that the final state is
	    // not complete, either because it has a jet, or because it has an
	    // extra neutrino. It is going to have a poor impact parameter. So,
	    // maybe a DTF does not make much sense. Better just compute the
	    // distance between the two vertices, and then project the momentum
	    // onto it.
	    
	    // 3. At some point, we want to use the IP to correct the mass. Then
	    // it makes sense to introduce a missing particle and do a decay
	    // tree fit.
	    
	    // FIXME: DTF doesn't deal very well with the jet contents that are
	    // not simple tracks or calo clusters.
	    const LHCb::VertexBase* bpv = m_dva->bestVertex( Wmuon );
	    const ITrackStateProvider* stateprovider = ( m_stateprovider.empty() ? NULL : &(*m_stateprovider) );
	    DecayTreeFitter::Fitter* fitter(0) ;
	    if( bpv != 0 ) {
	    fitter = new DecayTreeFitter::Fitter( *part, *bpv, stateprovider ) ;
	    } else {
	    fitter = new DecayTreeFitter::Fitter( *part, stateprovider ) ;
	    } 
	    fitter->fit() ;
	    //fitter->print() ;
	    auto params = fitter->fitParams( part ) ;
	    auto Nparams = fitter->fitParams( N ) ;
	    tuple->column( prefix + "DTF_chi2", fitter->chiSquare() ) ;
	    tuple->column( prefix + "DTF_M", params->momentum().M() ) ;
	    //tuple->column( prefix + "Wdecaylen", params->decayLength().value() ) ;
	    //tuple->column( prefix + "Wdecaylenerr", params->decayLength().error() ) ;
	    tuple->column( prefix + "DTF_Ndecaylen", Nparams->decayLength().value() ) ;
	    tuple->column( prefix + "DTF_Ndecaylenerr", Nparams->decayLength().error() ) ;
	    tuple->column( prefix + "haspv", bpv != 0 ) ;
	    */
	  }
	}
      }
    }

    // new: fill particle lists
    for( auto& pl : m_particleBranches ) {
      // get the list from the event
      for( const auto& loc : pl.locations ) {
	LHCb::Particle::Range particles = getIfExists<LHCb::Particle::Range>( loc );
	pl.particles.reserve( particles.size() ) ;
	// fill the particles (very simple for now)
	for( const auto& part : particles ) {
	  auto p = WMuMu::CompositeParticle( *part ) ;
	  fillCompositeInfo( *part,p,pvs,pfparticles) ;
	  pl.particles.push_back( p ) ;
	}
      }
    }
  
    // Add up the total p4 of a few things in the particle
    // flow.
    std::set<uint32_t> hashids ;
    Gaudi::LorentzVector p4tot, p4track ;
    for( const auto& p : pfparticles ) {
      // exclude tracks with 'infinite' momentum
      //if(  p->PFType() != LHCb::PFParticle::ChargedInfMomentum ) {
      if( int(p->info( LHCb::PFParticle::Type, LHCb::PFParticle::Unknown)) != LHCb::PFParticle::ChargedInfMomentum ) {
	p4tot += p->momentum() ;
	// FIXME: does this also account for V0?
	if( (p->proto() && p->proto()->track() &&
	     (p->proto()->track()->type() == LHCb::Track::Long ||
	      p->proto()->track()->type() == LHCb::Track::Downstream) )||
	    (p->daughters().size()==2 && p->daughters()[0]->proto() && p->daughters()[0]->proto()->track() ) )
	  p4track += p->momentum() ;
	if( p->proto() && p->proto()->track() )
	  hashids.insert( uniqueTrackID( *(p->proto()->track()) ) ) ;
      }
    }
    // Sometimes we miss particles, because these have been excluded
    // in the particle flow. Those should be in the list that we keep
    // in the event, so we add them here.
    for( const auto& it : m_trackToTrackIndex ) {
      const LHCb::Track* trk = it.first->proto() ? it.first->proto()->track() : 0 ;
      if( trk ) {
	uint32_t id = uniqueTrackID( *trk ) ;
	if( hashids.find(id) == hashids.end() ) {
	  auto p4 = it.first->momentum() ;
	  p4tot += p4 ;
	  p4track += p4 ;
	  hashids.insert( id ) ;
	}
      }
    }
    m_event->p4tot = WMuMu::MomentumVector( p4tot ) ;
    m_event->p4track = WMuMu::MomentumVector( p4track ) ;
    /*
    if( m_event->p4track.Pz > 5e6 ) {
      warning() << "Found event with a bit too much momentum!" << endmsg ;
      for( const auto& p : pfparticles ) {
	// FIXME: does this also account for V0?
	if( (p->proto() && p->proto()->track() &&
	     (p->proto()->track()->type() == LHCb::Track::Long ||
	      p->proto()->track()->type() == LHCb::Track::Downstream) ) )
	  std::cout 
		    << p->proto()->track()->type() << " "
		    << LHCb::PrimaryVertex::uniqueVeloSegmentID(*(p->proto()->track())) << " "
		    << p->proto()->track()->chi2PerDoF() << " "
		    << p->momentum() << " "
		    << p->proto()->track()->type() << p->momentum().Pt() << " "
		    << p->info(LHCb::PFParticle::Type, LHCb::PFParticle::Unknown) << " " 
		    << p->proto()->track()->firstState().qOverP() << " "
		    << p->proto()->track()->firstState().qOverP()/std::sqrt(p->proto()->track()->firstState().covariance()(4,4)) << std::endl ;	
      }
    }
    */
    m_tree->Fill() ;
  } else if(mcheader) { // always fill the event on MC
    m_tree->Fill() ;
  }
  
  //auto mcjets = getIfExists<LHCb::Particles>("Phys/MCJetMakerJets/Particles") ;
  
  return StatusCode::SUCCESS ;  
}

namespace {
  void findAllInMCTree( const LHCb::MCParticle& p, int depth,
			const std::vector<int>& abspids,
			std::vector<std::pair<int,const LHCb::MCParticle*> >& selection)
  {
    for( const auto& v : p.endVertices() ) {
      for( const auto& dau : v->products() ) {
	if( std::find(abspids.begin(),abspids.end(),std::abs(dau->particleID().pid()))!=abspids.end() )
	  selection.emplace_back( depth, dau ) ;
	else
	  findAllInMCTree(*dau,depth+1,abspids,selection) ;
      }
    }
  }
}

void WMuMuTupleMaker::fillMCEvent()
{
  // get the MC event header
  const LHCb::MCHeader* mcheader =  getIfExists<LHCb::MCHeader>(LHCb::MCHeaderLocation::Default) ;
  // get all the MC particles
  const LHCb::MCParticles* mcparticles = getIfExists<LHCb::MCParticles>(LHCb::MCParticleLocation::Default) ;
  WMuMu::MCEventInfo& mceventinfo = m_event->mctruth ;
  mceventinfo = WMuMu::MCEventInfo() ;
  fillMCVirtualJetInfo( *mcparticles, mceventinfo ) ;
  if(mcheader)
    mceventinfo.NumPV = mcheader->numOfPrimaryVertices() ;
  if( mcparticles ) {
    // locate the W and the neutrino, the W
    const LHCb::MCParticle *W(0),*Wmuon(0),*neutrino(0),*Nmuon(0) ;
    // let's make this work for Z->mumu and W->munu. Rather
    // errorprone but we need a quick solution.
    std::vector<const LHCb::MCParticle*> bosons ;
    for( const auto& mcp : *mcparticles)
      if( std::abs(mcp->particleID().pid())==23 ||
	  std::abs(mcp->particleID().pid())==24 )
	bosons.push_back( mcp ) ;
    
    if( bosons.size()>0 ) {
      W = bosons.front() ;
      // collect all lepton daughters, including heavy neutrinos
      std::vector<const LHCb::MCParticle*> leptons ;
      for( const auto& v : W->endVertices() )
	for( const auto& dau : v->products() ) {
	  //std::cout << "W daughter: " <<  dau->particleID().pid() << std::endl ;
	  const int abspid = std::abs( dau->particleID().pid() ) ;
	  if( (abspid >=11 && abspid <=16) || abspid == 66 || abspid == 9900014)
	    leptons.push_back( dau ) ;
	}
      //	std::cout << "Number of leptons: " << leptons.size() << std::endl ;
      if( leptons.size()>=2 ) {
	// sort leptons in pT such that muons appear before electrons in a Z->mumu(gamma/ee) event.
	std::sort( leptons.begin(),leptons.end(),
		   [](const LHCb::MCParticle* a, const LHCb::MCParticle* b) -> bool { 
		     return a->momentum().Pt() > b->momentum().Pt() ; 
		   }
		   ) ;
	Wmuon   = leptons[0] ;
	neutrino = leptons[1] ;
	if( (std::abs( neutrino->particleID().pid() )-10)%2 )
	  std::swap( Wmuon, neutrino ) ;
	if(neutrino) {
	  // find a muon among the neutrino daughters. the following
	  // routine collects all leptons, including their 'depth' in
	  // the decay tree.
	  std::vector<std::pair<int,const LHCb::MCParticle*> > neutrinoleptondaughters ;
	  findAllInMCTree(*neutrino,0,std::vector<int>{11,13},neutrinoleptondaughters) ;
	  // take the one with the smallest depth in the decay tree. if ambiguous take the one with the highest pT.
	  if( !neutrinoleptondaughters.empty() ) {
	    std::sort( neutrinoleptondaughters.begin(),neutrinoleptondaughters.end(),
		       [](std::pair<int,const LHCb::MCParticle*> a, std::pair<int,const LHCb::MCParticle*> b) -> bool {
			 // first compare depth, then pT. what a mess.
			 return a.first < b.first ||
					  (a.first == b.first && 	    
					   a.second->momentum().Pt() > b.second->momentum().Pt()) ; 
		       }
		       ) ;
	    Nmuon = neutrinoleptondaughters.front().second ;
	  }
	}
      }
    }
    if(W)        mceventinfo.W = WMuMu::MCParticle(*W) ;
    if(Wmuon)    mceventinfo.Wmuon = WMuMu::MCParticle(*Wmuon) ;
    if(Nmuon)    mceventinfo.Nmuon = WMuMu::MCParticle(*Nmuon) ;
    if(neutrino) {
      mceventinfo.N = WMuMu::MCParticle(*neutrino) ;
      // charm, strangeness and bottom in the direct daughters
      for( const auto& v : neutrino->endVertices() ) {
	for( const auto& dau : v->products() ) {
	  if( dau->particleID().hasStrange() )
	    ++mceventinfo.NNumStrange ;
	  if( dau->particleID().hasCharm() ) 
	    ++mceventinfo.NNumCharm ;
	  if( dau->particleID().hasBottom() ) 
	    ++mceventinfo.NNumBottom ;
	}
      }
    }
  }
}

#include "Kernel/HitPattern.h"
#include "LoKi/PhysKinematics.h"

void WMuMuTupleMaker::fillTrackInfo( const LHCb::Particle& part,
				     WMuMu::TrackParticle& p,
				     const std::vector<LHCb::PrimaryVertex>& pvs,
				     const PFParticleRange& pfparticles ) const
{
  const LHCb::ProtoParticle* proto = part.proto();
  if ( !part.isBasicParticle() || !proto || !proto->track() ) return ;
  
  const LHCb::Track* track = proto->track();
  
  /* All the stuff from TrackTool */
  p.TRACK_CHI2NDOF = track->chi2PerDoF() ;
  p.TRACK_NDOF = track->nDoF() ;
  p.TRACK_VeloCHI2NDOF = track->info(LHCb::Track::FitVeloChi2, 1.)/track->info(LHCb::Track::FitVeloNDoF, -1.) ;
  p.TRACK_TCHI2NDOF = track->info(LHCb::Track::FitTChi2, 1.)/ track->info(LHCb::Track::FitTNDoF, -1.) ;
  p.TRACK_MatchCHI2 = track->info(LHCb::Track::FitMatchChi2,-1) ;
  p.TRACK_GhostProb = track->ghostProbability() ;
  p.TRACK_qOverP = track->firstState().qOverP() ;
  p.TRACK_qOverPerr = track->firstState().covariance()(4,4) > 0 ? std::sqrt( track->firstState().covariance()(4,4) ) : -1 ;
  const LHCb::State* stateFirstMeasurement = track->stateAt( LHCb::State::FirstMeasurement) ;

  p.TRACK_zFirstHit = stateFirstMeasurement ? stateFirstMeasurement->z() : 0 ;
  
  LHCb::HitPattern hp( track->lhcbIDs() ) ;
  /*
  p.TRACK_NumVeloPhi   = hp.numVeloPhi() ;
  p.TRACK_NumVeloR     = hp.numVeloR() ;
  p.TRACK_NumVeloHoles = hp.numVeloHoles() ;
  p.TRACK_NumTTHits    = hp.numTTHits() ;
  p.TRACK_NumITHits    = hp.numITHits() ;
  p.TRACK_NumOTHits    = hp.numOTHits() ;
  p.TRACK_NumTLayers   = hp.numTLayers() ;
  p.TRACK_NumTHoles    = hp.numTHoles() ;
  p.TRACK_NumMuon       = 0 ;
  p.TRACK_NumMuonLayers = 0 ;
  */
  p.TRACK_InPV = computeTrackIP( *track, pvs, p.TRACK_IP, p.TRACK_IPCHI2, p.TRACK_PVindex) ;
  
  p.ProbNNe = proto->info(LHCb::ProtoParticle::ProbNNe,-1000) ;
  //p.ProbNNk = proto->info(LHCb::ProtoParticle::ProbNNk,-1000) ;
  //p.ProbNNp = proto->info(LHCb::ProtoParticle::ProbNNp,-1000) ;
  //p.ProbNNpi = proto->info(LHCb::ProtoParticle::ProbNNpi,-1000) ;
  p.ProbNNmu = proto->info(LHCb::ProtoParticle::ProbNNmu,-1000) ;
  p.ProbNNghost = proto->info(LHCb::ProtoParticle::ProbNNghost,-1000) ;

  // Muon
  p.hasMuon = p.isMuon = p.isMuonLoose = p.isMuonTight = false ;
  const LHCb::MuonPID * muonpid = proto->muonPID();

  if( muonpid ) {
    p.hasMuon = true ;
    p.isMuon  = muonpid->IsMuon() ;
    p.isMuonTight = muonpid->IsMuonTight() ;
    p.isMuonLoose = muonpid->IsMuonLoose() ;
    if(  muonpid->muonTrack() ) {
      p.TRACK_NumMuon       = muonpid->muonTrack()->lhcbIDs().size() ;
      LHCb::HitPattern muonhp( muonpid->muonTrack()->lhcbIDs() ) ;
      p.TRACK_NumMuonLayers = muonhp.muon().count() ;
    }
  }
  
  // CALO
  p.hasCalo = !proto->calo().empty();
  
  // Muon variables
  //p.InAccMuon   = proto->info(LHCb::ProtoParticle::InAccMuon,false)>0.5 ;
  //p.isMuonLoose = muonPID ? muonPID->IsMuonLoose() : false ;
  //p.isMuonTight = muonPID ? muonPID->IsMuonTight() : false ;
  
  //test &= tuple->column( prefix+"_MuonMuLL",    muonPID ? muonPID->MuonLLMu()    : -1000 );
  //test &= tuple->column( prefix+"_MuonBkgLL",   muonPID ? muonPID->MuonLLBg()    : -1000 );
  //test &= tuple->column( prefix+"_MuonNShared", muonPID ? muonPID->nShared()     : -1    );

  // ECAL variables
  p.InAccEcal = proto->info(LHCb::ProtoParticle::InAccEcal,false)>0.5 ;
  p.CaloEcalE = proto->info(LHCb::ProtoParticle::CaloEcalE,0.0) ;
  p.EcalPIDe  = proto->info(LHCb::ProtoParticle::EcalPIDe,-10.) ;
  p.EcalPIDmu = proto->info(LHCb::ProtoParticle::EcalPIDmu,-10.) ;
  
  // HCAL
  p.InAccHcal = proto->info(LHCb::ProtoParticle::InAccHcal,false)>0.5 ;
  p.CaloHcalE = proto->info(LHCb::ProtoParticle::CaloHcalE,0.0) ;
  // test &= tuple->column( prefix+"_HcalPIDe", proto->info(LHCb::ProtoParticle::HcalPIDe,-10000.));
  // test &= tuple->column( prefix+"_HcalPIDmu", proto->info(LHCb::ProtoParticle::HcalPIDmu,-10000.));

  fillIsolationInfo( part, p.Iso, pfparticles ) ;

  // fill also the total charge in the velo
  // p.TRACK_VeloCharge = 0 ;
  // if( veloclusters ) {
  //   for( const auto& lhcbid : track->lhcbIDs() ) {
  //     if( lhcbid.isVelo() ) {
  // 	const auto& cluster = veloclusters->object( lhcbid.veloID() ) ;
  // 	if( cluster ) p.TRACK_VeloCharge += cluster->totalCharge() ;
  //     }
  //   }
  // }
  p.TRACK_VeloCharge = part.proto()->info(LHCb::ProtoParticle::VeloCharge , -1.0) ;
}

namespace {
  void extractVeloSegmentIDs( const LHCb::Particle& p,
			      std::vector<LHCb::PrimaryVertex::VeloSegmentID>& ids )
  {
    if( p.proto() && p.proto()->track() && p.proto()->track()->hasVelo() )
      ids.push_back(  LHCb::PrimaryVertex::uniqueVeloSegmentID(*(p.proto()->track())) ) ;
    for( const auto& dau : p.daughters() )
      extractVeloSegmentIDs(*dau,ids) ;
  }
}


void WMuMuTupleMaker::fillCompositeInfo( const LHCb::Particle& part,
					 WMuMu::CompositeParticle& p,
					 const std::vector<LHCb::PrimaryVertex>& pvs,
					 const PFParticleRange& pfparticles) const
{
  fillTisTosInfo( part, p ) ;

  /* fill the error on the invariant mass */
  const auto&p4 = part.momentum() ;
  const double m = p4.M() ;
  Gaudi::Vector4 H = { -p4.Px() / m, -p4.Py()/m, -p4.Pz()/m, p4.E()/m } ;
  p.Merr = std::sqrt( ROOT::Math::Similarity( H, part.momCovMatrix() ) ) ;
  
  /* copied from AToMuMuTuples. let's hope it is right:-) */
  p.PVindex = -1 ;
  if( pvs.empty() ) {
    p.IP = 0 ;
    p.IPCHI2 = 0 ;
  } else {
    p.IP = 9999 ;
    p.IPCHI2 = 9999 ;
    std::vector<LHCb::PrimaryVertex::VeloSegmentID> ids ;
    extractVeloSegmentIDs( part, ids ) ;
    for(size_t i=0; i<pvs.size(); ++i) {
      const auto& pv = pvs[i] ;
      // unbiase this PV with respect to the tracks in the composite
      Gaudi::XYZPoint pos ;
      Gaudi::SymMatrix3x3 cov ;
      int nremoved = pv.unbiasedPosition( ids, pos, cov ) ;
      if( nremoved <= 0 ) {
	pos = pv.position() ;
	cov = pv.covMatrix() ;
      } 
      LHCb::VertexBase pvvertex ;
      pvvertex.setPosition(pos) ;
      pvvertex.setCovMatrix(cov) ;
      
      double ip, ipchi2 ;
      StatusCode sc = m_distcalc->distance(&part, &pvvertex, ip, ipchi2) ;
      if( sc.isSuccess() ) {
	if( p.PVindex <0 || p.IP > ip ) {
	  p.IP=ip ;
	  p.PVindex = i ;
	}
	if( p.IPCHI2 > ipchi2) p.IPCHI2= ipchi2 ;
      }
    }
  }
  
  // set pointers to the daughters. this doesn't yet work for composite daughters
  unsigned int idau=0 ;
  for( const auto& dau : part.daughters() )
    if( idau < 4 ) {
      if( dau->proto() && dau->proto()->track() ) {
	*(&p.dau1 + idau) = addTrackParticle( *dau, pvs, pfparticles) ;
	++idau ;
      }
    }
  if( p.dau2>=0 )
    p.cosThetaStarCS = m_event->cosThetaStarCS( p.dau1, p.dau2 ) ;
  
  // for now, only look at the first two tracks. better: take maximum overlap.
  p.velooverlap = p.Toverlap = p.muonoverlap = -1 ;
  if(part.daughters().size() >=2 ) {
    const auto dau1 = part.daughters()[0] ;
    const auto dau2 = part.daughters()[1] ;
    const LHCb::Track* track1 = dau1->proto() ? dau1->proto()->track() : 0 ;
    const LHCb::Track* track2 = dau2->proto() ? dau2->proto()->track() : 0 ;
    if( track1 && track2 ) {
      // let's count overlap
      typedef LHCb::TrackCloneData<false,false> MyTrackCloneData ;
      MyTrackCloneData clonedata1(const_cast<LHCb::Track*>(track1)) ; 
      MyTrackCloneData clonedata2(const_cast<LHCb::Track*>(track2)) ; 
      const unsigned char veloRoverlap12 = clonedata1.nCommon( clonedata2, MyTrackCloneData::VeloR) ;
      const unsigned char veloPhioverlap12 = clonedata1.nCommon( clonedata2, MyTrackCloneData::VeloPhi) ;
      p.velooverlap = veloRoverlap12 + veloPhioverlap12 ;
      p.Toverlap = clonedata1.nCommon( clonedata2, MyTrackCloneData::T) ;
    }
    // also for the muon segments
    const LHCb::Track* muontrack1 = dau1->proto() && dau1->proto()->muonPID()  ? dau1->proto()->muonPID()->muonTrack() : 0 ;
    const LHCb::Track* muontrack2 = dau2->proto() && dau2->proto()->muonPID()  ? dau2->proto()->muonPID()->muonTrack() : 0 ;
    if( muontrack1 && muontrack2 ) {
      p.muonoverlap = muontrack1->nCommonLhcbIDs( *muontrack2 ) ;
    }
  }
}

  
/*****************************************************************************/

unsigned char floatToChar(double f)
{
  int value= int(f*256);
  if( value > 255 ) value = 255 ;
  if( value < 0 ) value = 0 ;
  return (value > 255) ? 255 : ( (value < 0 ) ? 0 : value ) ;
} 

void WMuMuTupleMaker::fillIsolationInfo( const LHCb::Particle& part,
					 WMuMu::IsolationInfo& info,
					 const PFParticleRange& pfparticles ) const
{
  info.IT1 = info.IT3 = info.IT5 = info.IT5e = info.PFType = 0 ;
  if( !pfparticles.empty() ) {
    // just sum p4 in a variety of cones
    Gaudi::LorentzVector p4_1, p4_3, p4_5 ;
    unsigned short n_1(0), n_3(0), n_5(0) ;
    const auto& p4 = part.momentum() ;
    double phi = part.momentum().Phi() ;
    double eta = part.momentum().Eta() ;
    for( const auto& pfp : pfparticles ) 
      if( part.proto() != pfp->proto() ) {
	const auto& ip4 = pfp->momentum() ;
	double deta = ip4.Eta() - eta ;
	double dphi = ip4.Phi() - phi ;
	if(      dphi >  M_PI ) dphi -= 2*M_PI ;
	else if( dphi < -M_PI ) dphi += 2*M_PI ; 
	double dR2 = deta*deta+dphi*dphi ;
	if( dR2 < 0.5*0.5 ) {
	  p4_5 += ip4 ;
	  ++n_5 ;
	  if( dR2 < 0.3*0.3 ) {
	    p4_3 += ip4 ;
	    ++n_3 ;
	    if( dR2 < 0.1*0.1 ) {
	      p4_1 += ip4 ;
	      ++n_1 ;
	    }
	  }
	}
      } else {
	info.PFType = int(pfp->info( LHCb::PFParticle::Type,0 )) ;
      }
    //info.I1 = floatToChar( p4.E() / ( p4 + p4_1 ).E() ) ;
    //info.I3 = floatToChar( p4.E() / ( p4 + p4_3 ).E() ) ;
    //info.I5 = floatToChar( p4.E() / ( p4 + p4_5 ).E() ) ;
    //info.I5e = floatToChar( p4.E() / ( p4 + p4_5 - p4_1 ).E() ) ;
    info.IT1 = floatToChar( p4.Pt() / ( p4 + p4_1 ).Pt() ) ;
    info.IT3 = floatToChar( p4.Pt() / ( p4 + p4_3 ).Pt() ) ;
    info.IT5 = floatToChar( p4.Pt() / ( p4 + p4_5 ).Pt() ) ;
    info.IT5e = floatToChar( p4.Pt() / ( p4 + p4_5 - p4_1 ).Pt() ) ;
  }
}


/*****************************************************************************/

#include "Kernel/JetEnums.h"
namespace {
  void collectTracksWithVelo( const LHCb::Particle& p,
			      std::vector<const LHCb::Particle*>& tracks) {
    if(p.proto() && p.proto()->track() && p.proto()->track()->hasVelo() ) {
      tracks.push_back(&p) ;
    } else {
      for( const auto& dau : p.daughters() )
	collectTracksWithVelo( *dau, tracks ) ;
    }
  }

  template<typename S>
  void countJetVertexDaughters( const LHCb::Particle& p,
				S& nvertices,
				S& ntracksinvertices )
  {
    if( p.proto() && p.proto()->track() )
      ++ntracksinvertices ;
    else if( p.endVertex() ) {
      ++nvertices ;
      for( const auto& d : p.endVertex()->outgoingParticles() )
	countJetVertexDaughters(*d,nvertices,ntracksinvertices) ;
    }
  }
}

//#include "LoKi/ParticleProperties.h"
//#include "Kernel/ParticleProperty.h"
namespace {
  const LHCb::MCParticle* findHeadOfTree( const LHCb::MCParticle& p )
  {
    const LHCb::MCParticle* rc = &p ;
    if( p.mother() )
      rc = findHeadOfTree( *(p.mother()) ) ;
    return rc ;
  }
}

//#include "LoKi/IHybridFactory.h"

void WMuMuTupleMaker::fillJetInfo( const LHCb::Particle& part,
				   WMuMu::JetParticle& jet,
				   const std::vector<LHCb::PrimaryVertex>& pvs)
{
  fillTisTosInfo(part,jet) ;
  jet.CPF = part.info(LHCb::JetIDInfo::CPF , -1.) ;
  jet.MTF = part.info(LHCb::JetIDInfo::MTF , -1.) ;
  jet.MNF = part.info(LHCb::JetIDInfo::MNF , -1.) ;
  //jet.packedCPF = part.info(LHCb::JetIDInfo::CPF , -1.)  ;
  //jet.packedMTF = part.info(LHCb::JetIDInfo::MTF , -1.)  ;
  //jet.packedMNF = part.info(LHCb::JetIDInfo::MNF , -1.)  ;
  jet.MPT = part.info(LHCb::JetIDInfo::MPT , -1.) ;
  jet.JetWidth = part.info(LHCb::JetIDInfo::JetWidth , -1.) ;
  jet.N90      = part.info(LHCb::JetIDInfo::N90 , 0  ) ;
  jet.NTrack   = part.info(LHCb::JetIDInfo::Ntracks , 0  ) ;
  jet.NWithPVInfo = part.info(LHCb::JetIDInfo::NHasPV , 0  ) ;
  jet.NLong = jet.NLong5 = 0 ;
  jet.JetCharge = 0 ;
  jet.JetFragmentation = 0 ;
  
  double sumTrackPt(0), sumLongPt(0), sumLongPt5(0), sumLongPt2(0) ;
  double sumTrackPtPrompt(0), sumLongPtPrompt(0) ;
  double sumTrackIPChi2(0), sumLongIPChi2(0), sumLongIP(0) ;
  double sumPtWeightedLongIP(0) ;
  std::vector<double> longIP ;
  
  // collect all daughters with a trackm recursively
  std::vector<const LHCb::Particle*> daughters ;
  collectTracksWithVelo( part, daughters ) ;
  jet.NWithPVInfo = daughters.size() ;
  longIP.reserve( jet.NWithPVInfo ) ;
  std::vector<const LHCb::State*> states ;
  for( const auto& dau: daughters ) {
    const LHCb::Track* track = dau->proto()->track() ;
    bool isLong = track->type() == LHCb::Track::Long ;
        
    // compute the IPchi2 and IP
    float ip(1e6), ipchi2(1e6) ;
    int pvindex ;
    const bool inPV = computeTrackIP( *track,pvs,ip,ipchi2,pvindex) ;
    const bool prompt = inPV || ipchi2 < 25 ;
    sumTrackIPChi2 += ipchi2 ;
    const double pt = dau->pt() ;//momentum().Pt() ;
    sumTrackPt += pt ;
    if(prompt) sumTrackPtPrompt += pt ;
    
    if(isLong) {
      jet.NLong += 1 ;
      sumLongIPChi2 += ipchi2 ;
      sumLongIP += ip ;
      sumLongPt += pt ;
      sumPtWeightedLongIP += pt*ip ;
      longIP.push_back(ip) ;
      if(prompt) sumLongPtPrompt += pt ;
      double w = std::sqrt( pt ) ;
      jet.JetCharge += w*track->charge() ;
      sumLongPt5 += w ;
      if(pt>0.5*Gaudi::Units::GeV) ++jet.NLong5 ;
      sumLongPt2 += pt*pt ;  
      states.push_back( &(dau->proto()->track()->firstState()) ) ;
    }
  }

  jet.medianLongIP = 0 ;
  jet.minLongIP = 0 ;
  if( !longIP.empty() ) {
    size_t size = longIP.size() ;
    std::sort(longIP.begin(), longIP.end()) ;
    jet.minLongIP = longIP[0] ;
    if (size % 2 == 0) {
      jet.medianLongIP = (longIP[size / 2 - 1] + longIP[size / 2]) / 2 ;
    } else {
      jet.medianLongIP = longIP[size / 2];
    }
  }
    
  jet.JetCharge = sumLongPt>0? jet.JetCharge /  sumLongPt5 : 0 ;
  jet.JetFragmentation = sumLongPt>0 ?  sumLongPt2 /(sumLongPt*sumLongPt) : 0 ;
  jet.sumVeloPt = sumTrackPt ;
  jet.sumLongPt = sumLongPt ;
  jet.sumVeloPtPrompt = sumTrackPtPrompt ;
  jet.sumLongPtPrompt = sumLongPtPrompt ;
  jet.sumVeloIPChi2 = sumTrackIPChi2 ;
  jet.sumLongIPChi2 = sumLongIPChi2 ;  
  jet.sumLongIP = sumLongIP ;
  jet.ptWeightedLongIP = sumLongPt>0 ? sumPtWeightedLongIP / sumLongPt : 0 ;

  // use the factory
  //static LoKi::BasicFunctors<const LHCb::Particle*>::PredicateFromPredicate m_cut ;
  // for inspiration see PHYS/Phys_master/Phys/LoKiArrayFunctors/src/Components/TupleTool.cpp
  /*
  static LoKi::PhysTypes::Fun m_fun(LoKi::BasicFunctors<const LHCb::Particle*>::Constant ( -1.e+10 )) ;
  static bool isinitialized(false) ;
  if( !isinitialized ) {
    isinitialized = true ;
    // locate the factory
    LoKi::IHybridFactory* myfactory = tool<LoKi::IHybridFactory> ( "LoKi::Hybrid::Tool/HybridFactory:PUBLIC", this ) ;
    // use the factory
    StatusCode sc = myfactory -> get ( "SUMTREE( PT, ISBASIC & HASTRACK & ISLONG & (25 < MIPCHI2DV(PRIMARY)) )/SUMTREE( PT, ISBASIC & HASTRACK & ISLONG )" ,
				      m_fun , "from LoKiPhys.decorators import *") ;
    if ( sc.isFailure() ) { warning() << "Cannot decode functor" << endmsg ; }
    //
    release ( myfactory ) ;
  }
  if( jet.sumLongPt > 0 ) {
    info() << "jet displacement: "
	   << jet.sumLongPtPrompt / jet.sumLongPtPrompt
	   << " " << m_fun(&part) << endmsg ;
  }
  */
  
  // temporary: create one vertex from all tracks in the jet and
  // perform a vertex fit. will replace this with the 'average' SV
  // once know how to get it
  jet.x = jet.y = jet.z = 0 ;
  if( part.endVertex() ) {
    jet.x = part.endVertex()->position().x() ;
    jet.y = part.endVertex()->position().y() ;
    jet.z = part.endVertex()->position().z() ;
    jet.Nvertexdaughters = part.endVertex()->outgoingParticles().size() ;
    for( const auto& dau : part.daughters() ) {
      if( dau->endVertex() )
	jet.vertices.push_back( WMuMu::JetVertex{*dau} ) ;
      //countJetVertexDaughters(*dau,jet.Nvertices,jet.Ndaughtersinvertices) ;
    }
  }
  
  /*
  if( states.size()>=2 ) {
    LHCb::TrackStateVertex vertex( states ) ;
    vertex.fit() ;
    auto position = vertex.position() ;
    if( LHCb::TrackStateVertex::FitSuccess == vertex.fitAdaptive( 25 ) )
      position = vertex.position() ;
    jet.x = vertex.position().x() ;
    jet.y = vertex.position().y() ;
    jet.z = vertex.position().z() ;
  }
  */
  
  // So the problem is that I cannot yet get the jet tagger tool:
  // WARNING: cannot load libJetTagging.so for factory LoKi::BDTTag
  // WARNING: libNeuroBayesExpertCPP.so: cannot open shared object file: No such file or directory
  // ToolSvc             ERROR Cannot create tool LoKi::BDTTag (No factory found)
  std::map<std::string,double> property;
  m_jetTagger->calculateJetProperty(&part, property);
  // info() << "Jet properties: " << property.size() << endmsg ;
  // for( const auto& it : property) {
  //   info() << it.first <<" " << it.second << endmsg ;
  // }
  if( property.find("Tag0_bdt0")!=property.end() ) {
    jet.bdt0 = floatToChar(0.5*(property["Tag0_bdt0"]+1)) ;
    jet.bdt1 = floatToChar(0.5*(property["Tag0_bdt1"]+1)) ;
  } else {
    jet.bdt0 = 0 ;
    jet.bdt1 = 0 ;
  }

  // temporarily: match this jet to an MC truth particle:
  // - first match it to an MCJet
  // - then match that jet to an MC particle
  // let's see what happens :-)
  /*
  typedef LHCb::Relation1D< LHCb::Particle, LHCb::MCParticle > MCJetToMCParticleTable ;
  typedef LHCb::Relation1D< LHCb::Particle, LHCb::Particle > JetToMCJetMCTable ;
  const auto* mcjettomcparticletable = getIfExists<MCJetToMCParticleTable>("Relations/Phys/MCJets2MCParticles") ;
  //const auto* jettomcjettable = getIfExists<JetToMCJetMCTable>("Relations/Phys/Jets2MCJets") ;
  //typedef IRelationWeighted<LHCb::Particle,LHCb::Particle,double> P2PW;
  const auto* jettomcjettable = getIfExists<IRelateJets::Table>( "Relations/Phys/Jets2MCJets" );
  //info() << "Relations: " << mcjettomcparticletable << " " << jettomcjettable << endreq ;
  
  // first simple implementation: take the particle most up in the
  // decay chain. count the pT associated to this. In the end, choose
  // the one with most pT.
  jet.mcmotherpid = 0 ;
  if( jettomcjettable && mcjettomcparticletable ) {
    // info() << "NUmber of entries in tables: "
    // 	   << jettomcjettable->relations().size() << " "
    // 	   << mcjettomcparticletable->relations().size() << endmsg ;
    double pttot(0) ;
    auto relations = jettomcjettable->relations( &part) ;
    //info() << "Number of matched mc jets: " << relations.size() << endreq ;
    std::map< const LHCb::MCParticle*, double> mcmothers ;
    for( const auto& mcjet : relations ) {
      // find the corresponding MC particles
      for( const auto& mcpart : mcjettomcparticletable->relations( mcjet.to() ) ) {
	const LHCb::MCParticle* p =  mcpart.to() ;
	const LHCb::MCParticle* head = findHeadOfTree( *p ) ;
	double pt = p->momentum().Pt() ;
	if( head ) mcmothers[head] += pt ;
	pttot += pt ;
      }
    }
    
    if( !mcmothers.empty() ) {
      auto bestmatch = mcmothers.end() ;
      for( auto it = mcmothers.begin(); it != mcmothers.end(); ++it) 
	if( bestmatch == mcmothers.end() ||
	    bestmatch->second < it->second ) bestmatch = it ;
      jet.mcmotherpid = bestmatch->first->particleID().pid() ;
    }
  }
  */

  // another method: take the linker container for the tracks. then do the matching based on those.
  //Link/Rec/Track/Best
  
  // this works very well: just match to our 'virtual dijets'
  if(m_event->mctruth.virtualdijets.size()>0) {
    // find the closest jet. I hope this will somehow work.
    double bestdR = 0.5 ;
    jet.mcmotherpid = 0 ;
    for( const auto& dijet: m_event->mctruth.virtualdijets ) {
      double dR1 = dijet.jet1.deltaR( jet ) ;
      double dR2 = dijet.jet2.deltaR( jet ) ;
      if( dR1 < bestdR ) {
	bestdR = dR1 ;
	jet.mcmotherpid = dijet.pid ;
      }
      if( dR2 < bestdR ) {
	bestdR = dR2 ;
	jet.mcmotherpid = dijet.pid ;
      }
    }
  }
}


namespace {
  template<typename TisTosInfo>
  void fillMyTisTosInfo( ITriggerTisTos& tistostool,
			 const WMuMuTupleMaker::TisTosMap& tistosmap,
			 const std::vector<std::string>& lines,
			 const LHCb::Particle& particle,
			 TisTosInfo& tistos )
  {
    // so ... not thread safe
    tistostool.setOfflineInput(particle);
    // std::cout<< "Tis-tossing Particle: "
    // 	     << particle.particleID().pid() << " "
    // 	     << particle.daughters().size() << " "
    // 	     << particle.proto() << std::endl ; ;
    
    // get the result
    ITriggerTisTos::TisTosTob classifiedDec;
    auto it = tistosmap.find( std::abs(particle.particleID().pid()) ) ;
    if( it != tistosmap.end() ) {
      // two options: if there is only one string "ALL", then we
      // process them all. If not, we only process the active ones.
      if( it->second.size()==1 &&
	  it->second.front() == std::string("ALL") ) {
	size_t index(0) ;
	for( const auto& l : lines ) {
	  tistostool.setTriggerInput(l);
	  classifiedDec = tistostool.tisTosTobTrigger();
	  typename TisTosInfo::value_type bit = ULong64_t(1) << index ;
	  if( classifiedDec.tis() ) tistos.tis |= bit ;
	  if( classifiedDec.tos() ) tistos.tos |= bit ;
	  if( classifiedDec.tus() ) tistos.tus |= bit ;
	  ++index ;
	}
      } else {	
	for( const auto& l : it->second ) {
	  tistostool.setTriggerInput(l);
	  classifiedDec = tistostool.tisTosTobTrigger();
	  typename TisTosInfo::value_type bit = WMuMu::createMask( lines, l ) ;
	  if(!bit) {
	    std::cout << "Error: cannot set tistosbit for line " << l << std::endl ;
	    std::cout << lines.front() << " " << l << std::endl ;
	  }
	  if( classifiedDec.tis() ) tistos.tis |= bit ;
	  if( classifiedDec.tos() ) tistos.tos |= bit ;
	  if( classifiedDec.tus() ) tistos.tus |= bit ;
	}
      }
    } else {
      //std::cout << "Cannot find tis tos lines for particle: "
      // << particle.particleID().pid() << std::endl ;
    }
  }
}

void WMuMuTupleMaker::fillTisTosInfo( const LHCb::Particle& part,WMuMu::Particle& p) const
{
  fillMyTisTosInfo( const_cast<ITriggerTisTos&>(*m_L0TisTosTool),
		    m_L0TisTosLines,WMuMu::L0Lines, part, p.decinfo.L0 ) ;
  fillMyTisTosInfo( const_cast<ITriggerTisTos&>(*m_Hlt1TisTosTool),
		    m_Hlt1TisTosLines,WMuMu::Hlt1Lines, part, p.decinfo.hlt1 ) ;
  fillMyTisTosInfo( const_cast<ITriggerTisTos&>(*m_Hlt2TisTosTool),
		    m_Hlt2TisTosLines,WMuMu::Hlt2Lines, part, p.decinfo.hlt2 ) ;
}

unsigned int WMuMuTupleMaker::addTrackParticle( const LHCb::Particle& p,
						const std::vector<LHCb::PrimaryVertex>& pvs,
						const PFParticleRange& pfparticles) const
{
  unsigned int rc(255) ;
  if( p.proto() && p.proto()->track() ) {
    const auto it = std::find_if(m_trackToTrackIndex.begin(), m_trackToTrackIndex.end(), EqualTrackParticle(&p) ) ;
    if( it != m_trackToTrackIndex.end() ) {
      rc = it->second ;
    } else {
      rc = m_event->tracks.size() ;
      m_trackToTrackIndex.push_back( std::make_pair(&p,rc) ) ;
      // now fill all the track info. need to learn how to do this properly
      WMuMu::TrackParticle trk(p) ;
      fillTrackInfo( p, trk, pvs, pfparticles ) ;
      fillTisTosInfo( p, trk ) ;
      m_event->tracks.push_back( trk ) ;
    }
  }
  return rc ;
}

void WMuMuTupleMaker::fillPVs( const std::vector<LHCb::PrimaryVertex>& pvs,
			       const LHCb::Track::Range& tracks) const
{
  // compute the velo ID for all tracks in the best track list. then
  // match to the first one? or shall we call the standard matching
  // algorithm and use that? sounds better to me ...

  // This is copied from Event/PackedEvents/PVToRecConverter.cpp
  
  // create a map from veloIDs to tracks in the best track list.
  std::map< LHCb::PrimaryVertex::VeloSegmentID, std::vector<const LHCb::Track*> > idToTracks ;
  for( const auto& tr: tracks ) {
    auto id = LHCb::PrimaryVertex::uniqueVeloSegmentID( *tr ) ;
    idToTracks[id].push_back( tr ) ;
  }
  for( const auto& pv : pvs ) {
    WMuMu::PrimaryVertex apv ;
    apv.x = pv.position().x() ;
    apv.y = pv.position().y() ;
    apv.z = pv.position().z() ;
    apv.ntrack = pv.tracks().size() ;
    for( const auto& itrk : pv.tracks() ) {
      auto it = idToTracks.find( itrk.id() ) ;
      if( it != idToTracks.end() ) {
	if(it->second.front()->type() == LHCb::Track::Long ) {
	  ++apv.nlong ;
	  const LHCb::State& state = it->second.front()->firstState() ;
	  //check that the curvature is at least 5 sigma
	  if( state.qOverP()*state.qOverP() / state.covariance()(4,4) > 25 )
	    apv.sumpt += state.pt() ;
	}
	if(it->second.front()->type() == int(LHCb::Track::Backward) ) {
	  ++apv.nbackward ;
	}
      }
    }
    m_event->PVs.push_back( apv ) ;
  }
}


/*
unsigned int WMuMuTupleMaker::addParticle( const LHCb::Particle& p,
					   const std::vector<LHCb::PrimaryVertex>& pvs)
{
  WMuMu::Particle part( p ) ;
  // filltistosinfo, IP info etc
} ;
*/
