#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys
import os

from glob import glob
from distutils.core import setup
from distutils.command.install import INSTALL_SCHEMES 

import doxypy

if os.name == "posix":
	if sys.prefix == "/usr":
		sys.prefix = "/usr/local/"

	for scheme in INSTALL_SCHEMES.values():
		scheme['data'] = scheme['purelib'] 

setup(
	name=doxypy.__applicationName__,
	version=doxypy.__version__,
	author='Philippe Neumann & Gina Haeussge',
	author_email='doxypy@demod.org',
	url=doxypy.__website__,
	description=doxypy.__blurb__,
	license=doxypy.__licenseName__,
	
	classifiers=[
		"Programming Language :: Python",
		"Intended Audience :: Developers",
		"License :: OSI Approved :: GNU General Public License (GPL)",
		"Operating System :: OS Independent",
		"Topic :: Software Development :: Documentation"
	],
	scripts=['doxypy.py']
)
