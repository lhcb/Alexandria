# The ALEXANDRIA repository

Alexandria is a repository to preserve packages from [Urania](https://gitlab.cern.ch/lhcb/Urania) and [Castelao](https://gitlab.cern.ch/lhcb/Castelao) which are important and not maintained anymore. Some packages have a residual CMakeLists.txt file which allows to compile them together with Urania or Castelao, however, a successful build is not ensured since their code is not reviewed or updated anymore.

## How to run these packages

In principle, it should be feasible to run these packages by:

```
git clone ssh://git@gitlab.cern.ch:7999/lhcb/Alexandria.git
lb-dev Urania/latest
cp -r Alexandria/Urania/<your package> UraniaDev_XXX/
cd UraniaDev_XXX
make install
```

Some of the packages might need updates to the ```CMakeLists.txt```, or might not run at all (please bear in mind that these packages are not maintained anymore). Hence, further fixes to these code packages might be needed in order to make them run.

For packages based in Castelao, same logic applies as above, but changing Urania with Castelao.

## About 

The Great Library of Alexandria in Alexandria, Egypt, was one of the largest and most significant libraries of the ancient world. The Library was part of a larger research institution called the Mouseion, which was dedicated to the Muses, the nine goddesses of the arts. ([+info](https://en.wikipedia.org/wiki/Library_of_Alexandria)). 
