#!/usr/bin/env python

from ROOT import *
from math import *
from numpy import zeros as z

### Define a binning that has an equal number of signal events per bin for PID studies 

f = TFile.Open("/eos/lhcb/wg/semileptonic/Bs2DsX/ProcessedTrees/TupleRDS_Sim09b_DsMuNu.root")
t = f.Get("B2DsMuNuTuple/DecayTree")
entries = t.GetEntries()

bins = 1000
tracks = TH1F("nTracks","nTracks",bins,0,550)
p = TH1F("p","p",bins,0,600000)
pt = TH1F("pt","pt",bins,0,40000)

Mcorr = z(1, dtype=float)
flag = z(1, dtype=int)
trigger = z(1, dtype=int)
pid = z(1, dtype=float)
DsM = z(1, dtype=float)
iso = z(1, dtype=float)
gamma_mult = z(1, dtype=int)
gamma_PX = z(1, dtype=float)
gamma_PY = z(1, dtype=float)

for i in range(entries):
  t.GetEntry(i)
  t.SetBranchAddress('Bs_0_MCORR', Mcorr)
  t.SetBranchAddress('resFlag', flag)
  t.SetBranchAddress('Bs_0_Hlt2XcMuXForTauB2XcMuDecision_TOS', trigger)
  t.SetBranchAddress('mu_PIDmu', pid)
  t.SetBranchAddress('Ds_MM', DsM)
  t.SetBranchAddress('mu_iso_MinBDT_Long', iso)
  t.SetBranchAddress('Ds_0.40_nc_mult', gamma_mult)
  t.SetBranchAddress('Ds_0.40_nc_maxPt_PX', gamma_PX)
  t.SetBranchAddress('Ds_0.40_nc_maxPt_PY', gamma_PY)

  muP = t.mu_P
  muPT = t.mu_PT
  nTracks = t.nTracks
  ## some cuts to align with selection
  if (Mcorr[0]<8000 and Mcorr[0]>3000 and flag[0]<2 and trigger[0]==1 and pid[0]>0 and DsM[0]>1940 and DsM[0]<2000 and iso[0]>-0.5 and gamma_mult[0]>0 and sqrt(gamma_PX[0]**2 + gamma_PY[0]**2) > 0):
    tracks.Fill(nTracks)
    p.Fill(muP)
    pt.Fill(muPT)

stepTracks = tracks.Integral()/4
stepP = p.Integral()/10
stepPT = pt.Integral()/10
bordersTracks = []
bordersP = []
bordersPT = []
for i in range(5):
  bordersTracks.append(0+i*stepTracks)
for i in range(11):
  bordersP.append(0+i*stepP)
  bordersPT.append(0+i*stepPT)

print bordersP
print bordersPT
print bordersTracks

cutsTracks = []
cutsP = []
cutsPT = []

for i in range(bins):
  entriesTracks = tracks.Integral(0,i)
  entriesplusTracks = tracks.Integral(0,i+1)
  for j in range(len(bordersTracks)):
    if entriesTracks<bordersTracks[j] and entriesplusTracks>bordersTracks[j]:
      cutsTracks.append((550./bins)*i)

  entriesP = p.Integral(0,i)
  entriesplusP = p.Integral(0,i+1)
  for j in range(len(bordersP)):
    if entriesP<bordersP[j] and entriesplusP>bordersP[j]:
      cutsP.append((600000./bins)*i)

  entriesPT = pt.Integral(0,i)
  entriesplusPT = pt.Integral(0,i+1)
  for j in range(len(bordersPT)):
    if entriesPT<bordersPT[j] and entriesplusPT>bordersPT[j]:
      cutsPT.append((40000./bins)*i)
  
print cutsTracks
print cutsP
print cutsPT

