#!/usr/bin/env python

from ROOT import *

f = TFile.Open("PerfHists_Mu_Turbo15_FF.root")

num_histo = f.Get("PassedHist_Mu_DLLmu>0.0_All__Mu_P_Mu_PT_nTracks_Brunel")
den_histo = f.Get("TotalHist_Mu_DLLmu>0.0_All__Mu_P_Mu_PT_nTracks_Brunel")

nbinsX = num_histo.GetXaxis().GetNbins()
nbinsY = num_histo.GetYaxis().GetNbins()
nbinsZ = num_histo.GetZaxis().GetNbins()

total = 0

borderTrack = [0, 88, 126, 173, 550]
borderP = [0.0, 7800.0, 11400.0, 15600.0, 20400.0, 25800.0, 33000.0, 43200.0, 57000.0, 81000.0, 600000.0]
borderPT = [0.0, 760.0, 1160.0, 1560.0, 1960.0, 2440.0, 3000.0, 3680.0, 4640.0, 6320.0, 40000.0]

for i in range(nbinsX):
  for j in range(nbinsY):
    for l in range(nbinsZ):
      if den_histo.GetBinContent(i+1,j+1,l+1) != 0:
        print "P [ %s %s], PT [%s %s], Tracks [%s %s], eff= %s +/- %s" %(borderP[i],borderP[i+1],borderPT[j],borderPT[j+1],borderTrack[l],borderTrack[l+1],num_histo.GetBinContent(i+1,j+1,l+1)/den_histo.GetBinContent(i+1,j+1,l+1), num_histo.GetBinError(i+1,j+1,l+1)/den_histo.GetBinError(i+1,j+1,l+1))
      else:
        print "P [ %s %s], PT [%s %s], Tracks [%s %s], eff= 0 +/- 0" %(borderP[i],borderP[i+1],borderPT[j],borderPT[j+1],borderTrack[l],borderTrack[l+1])
