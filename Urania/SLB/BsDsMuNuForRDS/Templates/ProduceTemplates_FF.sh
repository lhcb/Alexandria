#********
#S24
#********

localpath="/afs/cern.ch/user/r/rvazquez/RDS/DaVinciDev_v42r6p1/Phys/BsDsMuNuForRDS/src"

## double charm
./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 1 &> output/BdDstDs_NorBIsoPIDCut.log
./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 2 &> output/BdDstDs_NorBIsoPIDCut.log
./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 3 &> output/BdDstDs_NorBIsoPIDCut.log
./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 4 &> output/BdDstDs_NorBIsoPIDCut.log
./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 5 &> output/BdDstDs_NorBIsoPIDCut.log
./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 6 &> output/BdDstDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 7 &> output/BdDstDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 8 &> output/BdDstDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 9 &> output/BdDstDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 10 &> output/BdDstDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BdDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBdDstDs -mc -DD -ff 11 &> output/BdDstDs_NorBIsoPIDCut.log

./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 1 &> output/BuD0Ds_NorBIsoPIDCut.log
./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 2 &> output/BuD0Ds_NorBIsoPIDCut.log
./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 3 &> output/BuD0Ds_NorBIsoPIDCut.log
./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 4 &> output/BuD0Ds_NorBIsoPIDCut.log
./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 5 &> output/BuD0Ds_NorBIsoPIDCut.log
./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 6 &> output/BuD0Ds_NorBIsoPIDCut.log
#./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 7 &> output/BuD0Ds_NorBIsoPIDCut.log
#./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 8 &> output/BuD0Ds_NorBIsoPIDCut.log
#./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 9 &> output/BuD0Ds_NorBIsoPIDCut.log
#./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 10 &> output/BuD0Ds_NorBIsoPIDCut.log
#./Templater -in $localpath/BuDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBuD0Ds -mc -DD -ff 11 &> output/BuD0Ds_NorBIsoPIDCut.log

./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 1 &> output/LbLcDs_NorBIsoPIDCut.log
./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 2 &> output/LbLcDs_NorBIsoPIDCut.log
./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 3 &> output/LbLcDs_NorBIsoPIDCut.log
./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 4 &> output/LbLcDs_NorBIsoPIDCut.log
./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 5 &> output/LbLcDs_NorBIsoPIDCut.log
./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 6 &> output/LbLcDs_NorBIsoPIDCut.log
#./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 7 &> output/LbLcDs_NorBIsoPIDCut.log
#./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 8 &> output/LbLcDs_NorBIsoPIDCut.log
#./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 9 &> output/LbLcDs_NorBIsoPIDCut.log
#./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 10 &> output/LbLcDs_NorBIsoPIDCut.log
#./Templater -in $localpath/LbDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutLbLcDs -mc -DD -ff 11 &> output/LbLcDs_NorBIsoPIDCut.log

./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 1 &> output/BsDsDs_NorBIsoPIDCut.log
./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 2 &> output/BsDsDs_NorBIsoPIDCut.log
./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 3 &> output/BsDsDs_NorBIsoPIDCut.log
./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 4 &> output/BsDsDs_NorBIsoPIDCut.log
./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 5 &> output/BsDsDs_NorBIsoPIDCut.log
./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 6 &> output/BsDsDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 7 &> output/BsDsDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 8 &> output/BsDsDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 9 &> output/BsDsDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 10 &> output/BsDsDs_NorBIsoPIDCut.log
#./Templater -in $localpath/BsDD_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutBsDsDs -mc -DD -ff 11 &> output/BsDsDs_NorBIsoPIDCut.log

## MuNu MC
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 1 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 2 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 3 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 4 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 5 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 6 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 7 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 8 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 9 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 10 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaMu -mc -mat 2 -ff 11 &> output/Signal_Bs0DstaMu_NorBIsoPIDCut.log

## Ds2460Mu
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 1 &> output/BsDs2460Mu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 2 &> output/BsDs2460Mu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 3 &> output/BsDs2460Mu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 4 &> output/BsDs2460Mu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 5 &> output/BsDs2460Mu_NorBIsoPIDCut.log
./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 6 &> output/BsDs2460Mu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 7 &> output/BsDs2460Mu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 8 &> output/BsDs2460Mu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 9 &> output/BsDs2460Mu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 10 &> output/BsDs2460Mu_NorBIsoPIDCut.log
#./Templater -in $localpath/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0Ds2460Mu -mc -mat 3 -ff 11 &> output/BsDs2460Mu_NorBIsoPIDCut.log

## TauNu MC
./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 1 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 2 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 3 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 4 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 5 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 6 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
#./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 7 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
#./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 8 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
#./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 9 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
#./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 10 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log
#./Templater -tau -in $localpath/sigtau_SWeighted_Dsst_WeightPID_Sim09b.root -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCutSignal_Bs0DstaTau -mc -mat 2 -ff 11 &> output/Signal_Bs0DstaTau_NorBIsoPIDCut.log

## Same Sign data
./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 1 &> output/SameSign_NorBIsoPIDCut.log
./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 2 &> output/SameSign_NorBIsoPIDCut.log
./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 3 &> output/SameSign_NorBIsoPIDCut.log
./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 4 &> output/SameSign_NorBIsoPIDCut.log
./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 5 &> output/SameSign_NorBIsoPIDCut.log
./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 6 &> output/SameSign_NorBIsoPIDCut.log
#./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 7 &> output/SameSign_NorBIsoPIDCut.log
#./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 8 &> output/SameSign_NorBIsoPIDCut.log
#./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 9 &> output/SameSign_NorBIsoPIDCut.log
#./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 10 &> output/SameSign_NorBIsoPIDCut.log
#./Templater -in $localpath/SS_SWeighted_Dsst_WeightPID_Sim09b.root -ss -conf config/config_NorBin_IsoPIDCut_Q2.txt -flag NorBIsoPIDCut_SameSign -ff 11 &> output/SameSign_NorBIsoPIDCut.log
