#!/usr/bin/env python

from ROOT import TFile, TH1F
from math import *
from numpy import zeros as z

Ds_0_40_nc_maxPt_PX = z(1)
Ds_0_40_nc_maxPt_PY = z(1)
Ds_0_40_nc_maxPt_PZ = z(1)

f = TFile("../src/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root")
t = f.Get("DecayTree")

entries = t.GetEntries()
print "Total number of entries = %s" %(entries)
histoMasses = {}
for name in ['Full', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'g7', 'g8', 'g9']:
  histoMasses[name] = TH1F("h"+name, "h"+name, 100, 0, 10000)

total = 0
for i in range(entries):
  t.GetEntry(i)
  t.SetBranchAddress('Ds_0.40_nc_maxPt_PX',Ds_0_40_nc_maxPt_PX)
  t.SetBranchAddress('Ds_0.40_nc_maxPt_PY',Ds_0_40_nc_maxPt_PY)
  t.SetBranchAddress('Ds_0.40_nc_maxPt_PZ',Ds_0_40_nc_maxPt_PZ)
  w = t.sigw
  gP = sqrt(Ds_0_40_nc_maxPt_PX**2 + Ds_0_40_nc_maxPt_PY**2 + Ds_0_40_nc_maxPt_PZ**2)
  gPT = sqrt(Ds_0_40_nc_maxPt_PX**2 + Ds_0_40_nc_maxPt_PY**2)
  m = t.McorrDsst
  mDs = t.Ds_MM
  sq = t.sq
  iso = t.mu_iso_MinBDT_Long
  trig = t.Bs_0_Hlt2XcMuXForTauB2XcMuDecision_TOS
  if ( m > 3500 and m < 5366.82 and mDs > 1940 and mDs < 2000 and sq > 0 and iso > -0.5):
    total += 1
    histoMasses['Full'].Fill(gPT,w)
    if gP < 3800:
      if gPT < 400:
        histoMasses['g1'].Fill(gPT,w)
      elif (gPT > 400 and gPT < 600):
        histoMasses['g2'].Fill(gPT,w)
      else: 
        histoMasses['g3'].Fill(gPT,w)
    elif (gP > 3800 and gP < 6500):
      if gPT < 400:
        histoMasses['g4'].Fill(gPT,w)
      elif (gPT > 400 and gPT < 600):
        histoMasses['g5'].Fill(gPT,w)
      else:
        histoMasses['g6'].Fill(gPT,w)
    else :
      if gPT < 400:
        histoMasses['g7'].Fill(gPT,w)
      elif (gPT > 400 and gPT < 600): 
        histoMasses['g8'].Fill(gPT,w)
      else:
        histoMasses['g9'].Fill(gPT,w)

print total
print histoMasses['Full'].Integral()  
print "Global efficiency = %s" %(histoMasses['Full'].Integral()/entries)
for i in range(1,10):
  if histoMasses['g'+str(i)].GetEntries() != 0:
    num = histoMasses['g'+str(i)].Integral()
    den = histoMasses['g'+str(i)].GetEntries()
    val = num/den
    errDen = sqrt(den)
    if num > 0:
      errNum = sqrt(num)
      err = val*sqrt(1/num+1/den)
    else: 
      err = 0
    print "bin %s = %4.2f+/-%4.2f, based on num = %4.2f, den = %4.2f" %(i, val, err, num, den)
  else:
    print "Zero division"



