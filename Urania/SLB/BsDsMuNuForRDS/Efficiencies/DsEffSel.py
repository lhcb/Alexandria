#!/usr/bin/env python

from ROOT import *
from math import *

def getPtBin(pt):
  if pt<3000:
    mybin = 0
  if pt>3000 and pt<4000:
    mybin = 1
  if pt>4000 and pt<5000:
    mybin = 2
  if pt>5000 and pt<6000:
    mybin = 3
  if pt>6000 and pt<7000:
    mybin = 4
  if pt>7000 and pt<8000:
    mybin = 5
  if pt>8000 and pt<10000:
    mybin = 6
  if pt>10000:
    mybin = 7
  return mybin 


beforeSel = TFile("/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/BsDsPi/MC/BsDsPi_kinematics_MC15.root")
afterSel = TFile("/eos/lhcb/wg/semileptonic/Bs2DsX/ProcessedTrees/BsDsPi_kinematics_MC15.root")

tBef = beforeSel.Get("B2DsMuNuTuple/DecayTree")
tAft = afterSel.Get("B2DsMuNuTuple/DecayTree")

MCTruthCuts = "( abs(Bs_0_TRUEID) == 531 && abs(Ds_TRUEID) == 431 && abs(piFromB_TRUEID)==211 && abs(Kmi_TRUEID)==321 && abs(Kpl_TRUEID)==321 && abs(pi_TRUEID)==211 && abs(Ds_MC_MOTHER_ID)==531 && abs(piFromB_MC_MOTHER_ID)==531 && abs(Kmi_MC_MOTHER_ID)==431 && abs(Kpl_MC_MOTHER_ID)==431 && abs(pi_MC_MOTHER_ID)==431 )"
DsMassCuts = "&& ( Ds_MM>1940 && Ds_MM<2000 )"

#nBef = float(tBef.GetEntries(MCTruthCuts+DsMassCuts))
#nAft = float(tAft.GetEntries(MCTruthCuts+DsMassCuts))

nBef = [0,0,0,0,0,0,0,0]
nAft = [0,0,0,0,0,0,0,0]

for i in range(tBef.GetEntries()):
  tBef.GetEntry(i)
  Ds_PT = tBef.Ds_PT
  BsID = tBef.Bs_0_TRUEID
  DsID = tBef.Ds_TRUEID 
  piFromBID = tBef.piFromB_TRUEID
  KmiID = tBef.Kmi_TRUEID
  KplID = tBef.Kpl_TRUEID
  piID = tBef.pi_TRUEID
  DsMomID = tBef.Ds_MC_MOTHER_ID
  piFromBMomID = tBef.piFromB_MC_MOTHER_ID
  KmiMomID = tBef.Kmi_MC_MOTHER_ID
  KplMomID = tBef.Kpl_MC_MOTHER_ID
  piMomID = tBef.pi_MC_MOTHER_ID
  Ds_MM = tBef.Ds_MM
  if (Ds_MM>1940 and Ds_MM<2000 and abs(BsID)==531 and abs(DsID)==431 and abs(piFromBID)==211 and abs(KmiID)==321 and abs(KplID)==321 and abs(piID)==211 and abs(DsMomID)==531 and abs(piFromBMomID)==531 and abs(KmiMomID)==431 and abs(KplMomID)==431 and abs(piMomID)==431):
    if Ds_PT<3000:
      nBef[0] += 1
    if (Ds_PT>3000 and Ds_PT<4000):
      nBef[1] += 1
    if (Ds_PT>4000 and Ds_PT<5000):
      nBef[2] += 1
    if (Ds_PT>5000 and Ds_PT<6000):
      nBef[3] += 1
    if (Ds_PT>6000 and Ds_PT<7000):
      nBef[4] += 1
    if (Ds_PT>7000 and Ds_PT<8000):
      nBef[5] += 1
    if (Ds_PT>8000 and Ds_PT<10000):
      nBef[6] += 1
    if Ds_PT>10000:
      nBef[7] += 1

for i in range(tAft.GetEntries()):
  tAft.GetEntry(i)
  Ds_PT = tAft.Ds_PT
  BsID = tAft.Bs_0_TRUEID
  DsID = tAft.Ds_TRUEID
  piFromBID = tAft.piFromB_TRUEID
  KmiID = tAft.Kmi_TRUEID
  KplID = tAft.Kpl_TRUEID
  piID = tAft.pi_TRUEID
  DsMomID = tAft.Ds_MC_MOTHER_ID
  piFromBMomID = tAft.piFromB_MC_MOTHER_ID
  KmiMomID = tAft.Kmi_MC_MOTHER_ID
  KplMomID = tAft.Kpl_MC_MOTHER_ID
  piMomID = tAft.pi_MC_MOTHER_ID
  Ds_MM = tAft.Ds_MM
  if (Ds_MM>1940 and Ds_MM<2000 and abs(BsID)==531 and abs(DsID)==431 and abs(piFromBID)==211 and abs(KmiID)==321 and abs(KplID)==321 and abs(piID)==211 and abs(DsMomID)==531 and abs(piFromBMomID)==531 and abs(KmiMomID)==431 and abs(KplMomID)==431 and abs(piMomID)==431):
    if Ds_PT<3000:
      nAft[0] += 1
    if (Ds_PT>3000 and Ds_PT<4000):
      nAft[1] += 1
    if (Ds_PT>4000 and Ds_PT<5000):
      nAft[2] += 1
    if (Ds_PT>5000 and Ds_PT<6000):
      nAft[3] += 1
    if (Ds_PT>6000 and Ds_PT<7000):
      nAft[4] += 1
    if (Ds_PT>7000 and Ds_PT<8000):
      nAft[5] += 1
    if (Ds_PT>8000 and Ds_PT<10000):
      nAft[6] += 1
    if Ds_PT>10000:
      nAft[7] += 1

pt = [1500, 3500, 4500, 5500, 6500, 7500, 9000, 20000]
ptErr = [1500, 500, 500, 500, 500, 500, 1000, 10000]
Eff = []
Err = []
for i in range(len(nBef)):
  print "Eff = %4.3f +/- %4.3f" %(float(nAft[i])/float(nBef[i]) , float(nAft[i])/float(nBef[i])*sqrt(1./nAft[i]+1./nBef[i]))
  Eff.append(float(nAft[i])/float(nBef[i]))
  Err.append(float(nAft[i])/float(nBef[i])*sqrt(1./nAft[i]+1./nBef[i]))

fileSig = TFile("/eos/lhcb/wg/semileptonic/Bs2DsX/WeightedNtuples/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root")
treeSig = fileSig.Get("DecayTree")
hQ2After = TH1F("hQ2After", "hQ2After", 11, 0, 11000000)
hQ2Before = TH1F("hQ2Before", "hQ2Before", 11, 0, 11000000)
for i in range(treeSig.GetEntries()):
  treeSig.GetEntry(i)
  q2 = treeSig.q2Reg
  dspt = treeSig.Ds_PT
  w = treeSig.sigw
  ptBin = getPtBin(dspt)
  myEff = Eff[ptBin]
  myErr = Err[ptBin]
  hQ2After.Fill(q2,w*myEff)  
  hQ2Before.Fill(q2,w)

gStyle.SetOptStat(0)
c2 = TCanvas()
hQ2 = hQ2After.Clone()
hQ2.Divide(hQ2Before)
hQ2.SetTitle("Ds selection efficiency")
hQ2.GetXaxis().SetTitle("q^{2} [MeV^{2}]")
hQ2.GetYaxis().SetTitle("Efficiency")
hQ2.Draw()
c2.SaveAs("Q2WeightedDsEff.pdf")

hGr = TGraphErrors()
for i in range(len(Eff)):
  hGr.SetPoint(i,pt[i],Eff[i]*100)
  hGr.SetPointError(i,ptErr[i],Err[i]*100)

c1 = TCanvas()
hGr.SetMarkerStyle(8)
hGr.GetYaxis().SetTitle("Ds selection efficiency (%)")
hGr.GetXaxis().SetTitle("Ds PT (MeV)")
hGr.SetMinimum(0)
hGr.SetMaximum(75)
hGr.Draw("AP")
c1.SaveAs("DsSelection_efficiency.pdf")
