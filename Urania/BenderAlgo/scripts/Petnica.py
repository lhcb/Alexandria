#! /usr/bin/env python
from ROOT import *
import sys, os
from Gaudi.Configuration import NTupleSvc
from Configurables import GaudiSequencer, DaVinci, L0Conf, SelDSTWriter, FilterDesktop

bsmumuCounterKeys = ["MuMuCouplesAnalyzed","EVT", "weird"]

################################### Some General Options. Remember to check it

INTERACTIVE = 0000

MC_INFO = 1
DST = 0
TUP = 1
TRIGGER = 0
REDOTRIGGER = 0
VERSION = "KW"
DOLUMI = False
DebugAlgos = 0

N = 2000000

Njobmax = 1.e10  ### info only, not real limit. Used to set an offset in the evt no. label

SIMULATION =  0

DataType = "2012"
print DataType, SIMULATION*"MONTE CARLO"

#########################
### ALGORITHMS TO ADD ###
#########################

__Ks2PiPi = 1  ### Will also do the Lambda0's
__Jpsi2MuMu = 1
__phi2KK = 1
__D02KK = 1
__HighMass = 1
__HighMassMu = 1

### datacards
#datacards = os.environ["HOME"] + "/KW/Ksmm_TC.py"
#datacards = os.environ["HOME"] + "/KW/Ksmm_08c.py"
datacards = os.environ["HOME"] + "/vol5/minbias_Petnica.py"
#datacards = os.environ["HOME"] + "/KW/Ksmmmm.py"


###########################
### FOR BATCH MODE ONLY ###
###########################

string = ""
args = sys.argv
for arg in args[1:]:
    if arg in ["-b","-b-"]: continue
    string += "_" + arg

N0 = string.replace("_","")
N0 = int(max(N0,"0"))


####################################
### Names for OUTPUT NTuple, Dst ###
####################################
main_name = "Petnica"#= bool(__K24MuDD + __K24MuLL)*"K24Mu" + bool(__KsMuMuDD + __KsMuMuLL)*"Ksmm"
 
TUPLE_FILE = "~/vol5/KW/" + main_name + VERSION + SIMULATION*"MC" + string+".root"
HISTO_FILE = "~/vol5/KW/" + main_name + VERSION + SIMULATION*"MC" + "_H_"+ string+".root"

#HISTo_FILE = TUPLE_FILE

if TUP:
    NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )

from BenderAlgo.BenderV0 import *
from BenderAlgo.BenderB2Xprongs import *
from BenderAlgo.extrafunctions import *
from ROOT import *

import GaudiPython

import Gaudi
from Gaudi.Configuration import *
    
def configure():
   
    DaVinci().EvtMax = 0    
    DaVinci().DataType = DataType 
    DaVinci().Simulation = SIMULATION
    #DaVinci().L0 = bool(TRIGGER)

    DaVinci().UserAlgorithms = []
    #DaVinci().Hlt = bool(TRIGGER)
    DaVinci().HistogramFile = HISTO_FILE
    DaVinci().Lumi = DOLUMI
    importOptions(os.environ["BENDERALGOROOT"] + "/options/V0HM2MuMu.py")
    importOptions(os.environ["BENDERALGOROOT"] + "/options/K2MuMu.py")
    #importOptions(os.environ["HOME"] + "/KW/StdDownMuons.py")
    #if (__KsMuMuLL or __KsMuMuDD) : importOptions(os.environ["HOME"] + "/KW/K2MuMu.py")
    #if (__K24MuLL or __K24MuDD) : importOptions(os.environ["HOME"] + "/KW/K24Mu.py")
                                                  
    importOptions(datacards)  ### Be sure you DO NOT import any other options file after this one. You can overwrite eventselector input.

    ################## END IMPORTING OPTIONS
    DaVinci().applyConf()   
    gaudi = appMgr()
        
    algos = []
    
    if __Ks2PiPi:
        algLL = B2QQ('Ks2PiPiLL')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algLL.LookIn =  "Phys/StdLooseKs2MuMuLL"
        algLL.decayname = "Ks-->pipi (LL)"
        algLL.COUNTER = {}
        algLL.COUNTER["Bender(evts) " + algLL.decayname] = 0
    
        algLL.Sel_Above_GL = 0
        algLL.extraFunctions = [DTF_M, massHypo]#trackHits]#, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algLL.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algLL.COUNTER[key] = 0
        algos.append(algLL)
        gaudi.addAlgorithm(algLL)
      
        algLL.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 

    if __Jpsi2MuMu:
        algJpsi = B2QQ('Jpsi2MuMu')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algJpsi.LookIn =  "Phys/StdLooseJpsi2MuMu"
        algJpsi.decayname = "Jpsi-->mumu "
        algJpsi.COUNTER = {}
        algJpsi.COUNTER["Bender(evts) " + algJpsi.decayname] = 0
    
        algJpsi.Sel_Above_GL = 0
        algJpsi.extraFunctions = [DTF_M, massHypo]#trackHits]#, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algJpsi.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algJpsi.COUNTER[key] = 0
        algos.append(algJpsi)
        gaudi.addAlgorithm(algJpsi)
      
        algJpsi.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 

    if __phi2KK:
        algPhi = B2QQ('phi2KK')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algPhi.LookIn =  "Phys/StdTightPhi2KK"
        algPhi.decayname = "phi"
        algPhi.COUNTER = {}
        algPhi.COUNTER["Bender(evts) " + algPhi.decayname] = 0
    
        algPhi.Sel_Above_GL = 0
        algPhi.extraFunctions = [DTF_M, massHypo]#trackHits]#, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algPhi.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algPhi.COUNTER[key] = 0
        algos.append(algPhi)
        gaudi.addAlgorithm(algPhi)
      
        algPhi.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 

    if __D02KK:
        algD0 = B2QQ('D02KK')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algD0.LookIn =  "Phys/StdLooseD02KK"
        algD0.decayname = "D0-->Kpi"
        algD0.COUNTER = {}
        algD0.COUNTER["Bender(evts) " + algD0.decayname] = 0
    
        algD0.Sel_Above_GL = 0
        algD0.extraFunctions = [DTF_M, massHypo]#trackHits]#, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algD0.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algD0.COUNTER[key] = 0
        algos.append(algD0)
        gaudi.addAlgorithm(algD0)
      
        algD0.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 

    if __HighMass:
        algH = B2QQ('HighMass')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algH.LookIn =  "Phys/StdLooseV0HM2Things"
        algH.decayname = "HighMassV0"
        algH.COUNTER = {}
        algH.COUNTER["Bender(evts) " + algH.decayname] = 0
    
        algH.Sel_Above_GL = 0
        algH.extraFunctions = [DTF_M, massHypo]#trackHits]#, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algH.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algH.COUNTER[key] = 0
        algos.append(algH)
        gaudi.addAlgorithm(algH)
      
        algH.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 

    if __HighMassMu:
        algHmu = B2QQ('HighMassMu')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algHmu.LookIn =  "Phys/StdLooseV0HM2MuMuLL"
        algHmu.decayname = "HighMassV0Mu"
        algHmu.COUNTER = {}
        algHmu.COUNTER["Bender(evts) " + algHmu.decayname] = 0
    
        algHmu.Sel_Above_GL = 0
        algHmu.extraFunctions = [DTF_M, massHypo]#trackHits]#, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algHmu.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algHmu.COUNTER[key] = 0
        algos.append(algHmu)
        gaudi.addAlgorithm(algHmu)
      
        algHmu.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 

  

 

    gaudi.initialize()
    from BenderAlgo import PIDcalTools
 
    ########################
    ##COMMON ATRIBUTES ####
    ######################
   
    for algo in algos:
        algo.MC_INFO = SIMULATION*MC_INFO
        algo.TRIGGER = TRIGGER
        algo.PIDcalTools = PIDcalTools
        algo.NTupleLUN = "T"
        algo.addedKeys = []
        #algo.runinfo = runinfo
        algo.DST = DST
        algo.TUP = TUP
        algo.COUNTER['negSq'] = 0
        algo.COUNTER["weird"] = 0
        algo.DEBUG = DebugAlgos ### Do not define global DEBUG
       
        algo.evt_of = Njobmax*N0

        algo.l0BankDecoder=algo.tool(cpp.IL0DUFromRawTool,'L0DUFromRawTool')
        algo.rawBankDecoder=algo.tool(cpp.IOTRawBankDecoder,'OTRawBankDecoder')

        algo.PVRefitter = algo.tool(cpp.IPVReFitter,"AdaptivePVReFitter")
        algo.LifeFitter = algo.tool(cpp.ILifetimeFitter,"PropertimeFitter")
        algo.Geom =  algo.tool(cpp.IDistanceCalculator,"LoKi::DistanceCalculator")

        if TRIGGER:
            algo.tistostool = algo.tool( cpp.ITriggerTisTos , 'TriggerTisTos')
            algo.tisTosToolL0= algo.tool(cpp.ITriggerTisTos , 'L0TriggerTisTos')
            algo.extraFunctions.append(triggerBlock)
            
            import Bs2MuMu.triggerDecisionLists as trigList
            algo.l0List = trigList.l0List_2011
            algo.hlt1List = trigList.hlt1List_2011
            algo.hlt2List = trigList.hlt2List_2011
            
configure()

gaudi = appMgr()

if INTERACTIVE:
    for i in range(INTERACTIVE):
        gaudi.run(1)
        TES = gaudi.evtsvc()
       
else:
        
    gaudi.run(N)
    gaudi.stop()
    #if DST: gaudi.service( 'PoolRootKeyEvtCnvSvc').finalize() # This line prevents from loosing your output dst if something doesn't know how to finalize...
    gaudi.finalize()
#
#len(TES["Phys/B2HHFilterSel/Particles"])
