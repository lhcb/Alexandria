#! /usr/bin/env python
import sys, os
from Gaudi.Configuration import NTupleSvc
from Configurables import GaudiSequencer, DaVinci, L0Conf, SelDSTWriter, FilterDesktop

bsmumuCounterKeys = ["MuMuCouplesAnalyzed","EVT", "weird"]

################################### Some General Options. Remember to check it

INTERACTIVE = 0
MC_INFO = 1
DST = 0
TUP = 1
TRIGGER = 1
REDOTRIGGER = 0
DebugAlgos = 0

N = -1

Njobmax = 1.e10  ### info only, not real limit. Used to set an offset in the evt no. label

SIMULATION =  1
VERSION = "mc10d"#"stp13"
#VERSION = "stp13"
DataDic = {"stp13":"2011","stp13_May13":"2011","stp12":"2010","mc10d":"2010","mc10u":"2010"}
DataType = DataDic[VERSION]  ##### To pass to DV

print DataType, SIMULATION*"MONTE CARLO"

#########################
### ALGORITHMS TO ADD ###
#########################

__BdJPsiKstar = 1
__B24Mu = 1


###########################
### FOR BATCH MODE ONLY ###
###########################
string = ""
args = sys.argv
for arg in args[1:]:
    if arg in ["-b","-b-"]: continue
    string += "_" + arg

N0 = string.replace("_","")
N0 = int(max(N0,"0"))
    
####################################
### Names for OUTPUT NTuple, Dst ###
####################################


TUPLE_FILE = "B24Mu.root" #os.environ["HOME"] + "/scratch0/NTuples/stp15B24" + SIMULATION*("MC" + VERSION) + string+".root"
HISTO_FILE = "wwwwu.root"  #stp11"+ SIMULATION*"MC" + "_H_"+ string+".root"

#HISTo_FILE = TUPLE_FILE

if TUP:
    
#    NTupleSvc( Output = [ 'T DATAFILE=\'castor:'+os.environ["CASTOR_HOME"]+"/muons/mb" + SIMULATION*"MC"+ string +'.root\' TYP=\'ROOT\' OPT=\'NEW\'' ]) ## To save the NTuple in Castor (a la Elias)
    NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )


#################
### DATACARDS ###
#################

#datacards = os.environ["HOME"] + "/cmtuser/Erasmus_v5r1p1/Data/Bs2MuMuDataCards/python/mc10/Bu2JPsiPi"+ string + ".py"

#datacards = "bdjpsikstmc.py"

from BenderAlgo.BenderBdJPsiKst import *
from BenderAlgo.BenderB2Xprongs import *
from BenderAlgo.extrafunctions import *

from ROOT import *

import GaudiPython
import Gaudi
from Gaudi.Configuration import *

MCtags = {"sim01reco01":['head-20100624','sim-20100831-vc-md100'],"sim03reco03":['head-20100624','sim-20100831-vc-md100'],"sim07reco06":['head-20101003','sim-20100831-vc-md100'],"mc10d":['head-20101206','sim-20101210-vc-md100'],"mc10u":['head-20101206','sim-20101210-vc-mu100']}
dataTags = {"stp13":['head-20110303','head-20110512'], "stp13_May13":['head-20110303','head-20110505'],"stp12":['head-20101026','head-20101112'],"stp11":['head-20101003','head-20101010']}
  


def configure():
    
    ################### IMPORTING OPTIONS

    DaVinci().EvtMax = 0    
    DaVinci().DataType = DataType 
    DaVinci().Simulation = SIMULATION
    #DaVinci().L0 = bool(TRIGGER)

    DaVinci().UserAlgorithms = []
    #DaVinci().Hlt = bool(TRIGGER)
    DaVinci().HistogramFile = HISTO_FILE
    DaVinci().Lumi = True
    #DaVinci().InputType = "MDST"

    if REDOTRIGGER:
        from Configurables import L0Conf, HltConf
        #first, redo L0 and HLT
        DaVinci().L0 = True

        DaVinci().ReplaceL0BanksWithEmulated=True
        #HltConf().ThresholdSettings = 'Physics_HighNu_80Vis_30L0_5Hlt1_CoreHlt2_Jun10'

        L0Conf().TCK = '0x002a'
 	HltConf().ForceSingleL0Configuration = False


        from Configurables import L0Conf
        L0Conf().IgnoreL0MuonCondDB = True
        from L0DU.L0Algs import emulateL0Muon
        l0muon = emulateL0Muon()
        l0muon.IgnoreCondDB = True
        l0muon.FoiXSize =[6,5,0,4,8]
        l0muon.FoiYSize =[0,0,0,1,1]
        l0muon.OutputLevel=2
        
    if TRIGGER:
        
        L0SelRepSeq = GaudiSequencer("L0SelRepSeq")
        L0SelRepSeq.MeasureTime = True
        from Configurables import L0SelReportsMaker, L0DecReportsMaker
        L0SelRepSeq.Members += [ L0DecReportsMaker(), L0SelReportsMaker() ]
        DaVinci().UserAlgorithms += [ L0SelRepSeq ]
    


    from Configurables import RawBankToSTClusterAlg 
    createITClusters = RawBankToSTClusterAlg("CreateITClusters") 
    createITClusters.DetType     = "IT"

    if SIMULATION:
        DaVinci().DDDBtag = MCtags[VERSION][0]
        DaVinci().CondDBtag = MCtags[VERSION][1]
      

        
    else: ## MagUp
         from Configurables import CondDB
         CondDB(UseOracle = True)
         
         DaVinci().DDDBtag = dataTags[VERSION][0]
         DaVinci().CondDBtag =  dataTags[VERSION][1]

    
    importOptions("$STDOPTS/PreloadUnits.opts")

    
    

    if __BdJPsiKstar:
        importOptions("$BSMUMUPYOPTS/DetachedJPsi.py")
        DaVinci().UserAlgorithms += [GaudiSequencer("SeqDetJpsi")]
        importOptions("testKstar.py")
        DaVinci().UserAlgorithms += [GaudiSequencer("SeqtestKstar")]
        importOptions( "$BSMUMUPYOPTS/DVPreselBd2JPsiKstar_TrChi2.py" )
        DaVinci().UserAlgorithms += [GaudiSequencer("SeqPreselBd2JPsiKstCommon") ]


           
    if DST:
        from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence
        bmm = FilterDesktop('bmm')
        bmm.Code = "ALL"

        MyBmm=DataOnDemand('/Event/Phys/PreselBs2MuMu')
        SelMyBmm=Selection('SelMyBmm',Algorithm=bmm, RequiredSelections = [MyBmm])
        SeqBmm = SelectionSequence('SeqBmm', TopSelection = SelMyBmm)

        dstWriter = SelDSTWriter("BmmDSTWriter",
                                 SelectionSequences = [SeqBmm],
                                 SaveCandidates = True,
                                 CopyMCTruth = SIMULATION,
                                 WriteFSR = True,
                                 OutputFileSuffix = "Test"
                                 )

        DaVinci().UserAlgorithms += [dstWriter.sequence()]

    #importOptions("MT_DiM.py")  ### Be sure you DO NOT import any other options file after this one. You can overwrite eventselector input.
    ################## END IMPORTING OPTIONS
    DaVinci().applyConf()   
    gaudi = appMgr()
        
    algos = []
  
    if __BdJPsiKstar:
        bd = BdJPsiKstar("Bd2JPsiKstar")
        bd.COUNTER= {"EVT":0,"No Online Tracks":0, "No Linked Tracks":0, "Sel":0}
        bd.LookIn = "Phys/PreselBd2JPsiKstCommon"
        bd.extraFunctions = [more_muon_things, globalEvtVars]
        algos.append(bd)
        gaudi.addAlgorithm(bd)
        bd.decayname = "Bd --> J/Psi K*"
        bd.InputLocations = []

        
    if __B24Mu:
        bs1 = B2Xprong("B24mu")
        bs1.Nprong = 4   ### number of bodies in the final state!
        bs1.COUNTER= {"EVT":0,"No Online Tracks":0, "No Linked Tracks":0, "Sel":0,"CandidatesAnalyzed":0}
        bs1.LookIn = "Dimuon/Phys/B2MuMuMuMuLinesLine"#"Dimuon/Phys/Bs2MuMuMuMuLine"   ### Container in the TES where to find the candidates !
        bs1.extraFunctions = [more_muon_things, globalEvtVars]
        algos.append(bs1)
        gaudi.addAlgorithm(bs1)
        bs1.decayname = "B --> 4mu"
        bs1.InputLocations = []

    

    gaudi.initialize()
    from BenderAlgo import PIDcalTools
 
    ########################
    ##COMMON ATRIBUTES ####
    ######################
   
    for algo in algos:
        algo.MC_INFO = SIMULATION*MC_INFO
        algo.TRIGGER = TRIGGER
        algo.PIDcalTools = PIDcalTools
        algo.NTupleLUN = "T"
        algo.addedKeys = []
        algo.DST = DST
        algo.TUP = TUP
        algo.COUNTER['negSq'] = 0
        algo.COUNTER["weird"] = 0
        algo.DEBUG = DebugAlgos ### Do not define global DEBUG
       
        algo.evt_of = Njobmax*N0

        algo.l0BankDecoder=algo.tool(cpp.IL0DUFromRawTool,'L0DUFromRawTool')
        algo.rawBankDecoder=algo.tool(cpp.IOTRawBankDecoder,'OTRawBankDecoder')
        
        algo.PVRefitter = algo.tool(cpp.IPVReFitter,"AdaptivePVReFitter")
        algo.Geom =  algo.tool(cpp.IDistanceCalculator,"LoKi::DistanceCalculator")

        if TRIGGER:
            algo.tistostool = algo.tool( cpp.ITriggerTisTos , 'TriggerTisTos')
            algo.tisTosToolL0= algo.tool(cpp.ITriggerTisTos , 'L0TriggerTisTos')
            algo.extraFunctions.append(triggerBlock)
            
            import Bs2MuMu.triggerDecisionLists as trigList
            algo.l0List = trigList.l0List_2011
            algo.hlt1List = trigList.hlt1List_2011
            algo.hlt2List = trigList.hlt2List_2011

       
            
configure()

gaudi = appMgr()

if INTERACTIVE: gaudi.run(INTERACTIVE)
    
#gaudi.run(N)
else:
        
    gaudi.run(N)
    gaudi.stop()
    #if DST: gaudi.service( 'PoolRootKeyEvtCnvSvc').finalize() # This line prevents from loosing your output dst if something doesn't know how to finalize...
    gaudi.finalize()
#
