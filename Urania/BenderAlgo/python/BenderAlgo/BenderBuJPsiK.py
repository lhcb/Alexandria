#! /usr/bin/env python

import climbing, isolation
from SomeUtils.alyabar import *   
from LinkerInstances.eventassoc import * 
from Bender.MainMC import *                

import GaudiPython , GaudiKernel
from select import selectVertexMin
#selectVertexMin = LoKi.SelectVertex.selectMin
from ROOT import *
###Defining some variables
GBL = GaudiPython.gbl.LHCb
KMASS = 493.677
MJPsi = 3096.920
units = GaudiKernel.SystemOfUnits
c_light = 2.99792458e+8 * units.m /units.s
light_cte = 1000./c_light

longTracksName = "longTracks"

class BuJPsiK(AlgoMC):            
    def analyse(self):
        self.COUNTER["EVT"] += 1
        TES = appMgr().evtsvc()
        if self.DEBUG: print self.decayname, self.COUNTER["EVT"]
         
        bloop = TES["/Event/"+self.LookIn+"/Particles"]
        if not bloop :
            if self.DEBUG: print "Nothing to do", self.name()
            return SUCCESS
        
        bloop = bloop.containedObjects()
        if not bloop.size(): return SUCCESS
        
        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the bsloop
        Done["TES"] = TES
        pvs_ = self.vselect("pvs_", ISPRIMARY)
        if not pvs_.size(): return SUCCESS
      
        if self.MC_INFO:
            Done["link"]= linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")
            #BU = climbing.climbing_bujpsik(TES["MC/Particles"].containedObjects(), MCID)
       
        ips2cuter = MIPCHI2(pvs_,self.geo())
        odin = TES["DAQ/ODIN"]
        runNum,evtNum = odin.runNumber(), odin.eventNumber()
        Done[longTracksName] = []
        tracks = TES['Rec/Track/Best']

        for track in tracks:
            if track.type() == 3 and track.chi2PerDoF() < 5 : Done[longTracksName].append(track)
        
        if self.MC_INFO or not ("bunchCrossingType") in dir(odin):
            ###print "oooooo xtype = -1 "
            xtype = -1.
            ptype = TES["Gen/Header"].collisions()[0].processType()
            mcpv = TES["Gen/Header"].numOfCollisions()
            bid = -1
            
            
        else:
            bid = float(odin.bunchId())
            xtype = float(odin.bunchCrossingType())
            ptype = -1.
            mcpv = -1.
            
        isoProtos = []
        for proto in TES["/Event/Phys/StdNoPIDsPions/Particles"]:
            #Done[longTracksName].append(proto.proto().track())
            isoProtos.append(proto)
              
        for b in bloop:
            if abs(b.particleID().pid())!=521: continue
            if "target" in dir(b.daughters().at(0)): par1= b.daughters().at(0).target()
            else: par1=b.daughters().at(0)
            if "target" in dir(b.daughters().at(1)): par2 = b.daughters().at(1).target() # Bu Daughters
            else: par2 = b.daughters().at(1) # Bu Daughters
            if ABSID(par1) == 321: kaon, jpsi = par1, par2
            else: kaon, jpsi = par2, par1
           
            if kaon.proto().track().type() != 3:
                print "Something suspicious in kaon track ", self.name()
                continue
            
            mu1, mu2 = jpsi.daughters().at(0), jpsi.daughters().at(1) # muons
            key1 = mu1.proto().track().key()
            key2 = mu2.proto().track().key()
            keyk = kaon.proto().track().key()
            if keyk == key1: continue
            if keyk == key2: continue
           
            PVips2 = VIPCHI2( b, self.geo())
            JPsiIps2 = VIPCHI2(jpsi, self.geo())
            
            #PV = self.bestVertex ( b )
            #PVj = self.bestVertex ( jpsi ) 
            PV = selectVertexMin(pvs_, PVips2, (PVips2 >= 0. ))

           
            PVj = selectVertexMin(pvs_, JPsiIps2, (JPsiIps2 >= 0. ))
            
            if not PV:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            if not PVj:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            Done["refittedPV"] = PV.clone()
            self.PVRefitter.remove(b, Done["refittedPV"])
            Done["refittedPVj"] = PVj.clone()
            self.PVRefitter.remove(jpsi, Done["refittedPVj"])

            
            Dis2 = VDCHI2( PV ) ### To PV which minimizes B IPS
            Dis2j = VDCHI2( PVj ) ### """ JPsi IPS
            Dis2_r = VDCHI2(Done["refittedPV"])
            Dis2j_r = VDCHI2(Done["refittedPVj"])
            
            
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(b.endVertex()), VY(b.endVertex()), VZ(b.endVertex()) )
            rSV = SVvec - PVvec
            ## if rSV[2] <0:
##                 if self.DEBUG: "negative DZ"
##                 self.COUNTER["weird"] +=1
##                 continue
        
            Bp = vector(PX(b), PY(b), PZ(b))
            Jp = vector(PX(jpsi),PY(jpsi),PZ(jpsi))
            kp = vector(PX(kaon),PY(kaon),PZ(kaon))
       
            Bpt = vtmod(Bp)
            Bips2 = PVips2(PV)
            JPips2 = JPsiIps2(PV)
            f_jpsip = dpr(PVvec,SVvec, Jp)
            Bip = dpr(PVvec, SVvec, Bp)
            JPips = psqrt(JPips2)
            fIPS = Bip*JPips/f_jpsip ## fake IPS for systematic Studies
            mu1ips2, mu2ips2, kips2 = ips2cuter(mu1), ips2cuter(mu2), ips2cuter(kaon)
            sigDOFS = Dis2(b)
            if self.put_more_cuts:
                if PIDK(kaon) < 15: continue
                if PIDmu(mu1) < 0: continue
                if PIDmu(mu2) < 0: continue
                if kips2 < 1: continue
                
            dDsig = Dis2(jpsi)
            ipscheck = min(sigDOFS, mu1ips2, mu2ips2, Bips2,kips2,dDsig)
            
            if ipscheck < 0:
                if self.DEBUG: print "error on IPS calculations"
                self.COUNTER["negSq"] +=1
                ### continue, Let's decide what to do afterwards...
            
         
            sigDOFS = psqrt(sigDOFS)
            Bips = psqrt(Bips2)
            dDsig = psqrt(dDsig)
            jpsi_dis = psqrt(Dis2j(jpsi))
            
            
            track1 = mu1.proto().track()
            track2 = mu2.proto().track()
            
            o1, o2 = track1.position(), track2.position()
            B1 = [ E(jpsi),Jp]
            B2 = [ E(kaon),kp]
            
            if "target" in dir(mu1): muDOCA = CLAPP (mu1.target(), self.geo())
            else: muDOCA = CLAPP (mu1, self.geo())
            muonsDOCA_ = muDOCA(mu2)
            ctau = CTAU (PV)
            ctau_r = CTAU (Done["refittedPV"])
            Blife_ = ctau(b)*light_cte
            mippvs = MIP( pvs_, self.geo() )
            iptoPV = IP (PV, self.geo())


            mu1ippvs_ = mippvs (mu1) # IP
            mu2ippvs_ = mippvs (mu2)
            mu1ip_ = iptoPV (mu1)
            mu2ip_ = iptoPV (mu2)
            
           
            isolations, isolations5 = isolation.compatibleVertices2([mu1,mu2, kaon ], isoProtos,  PVvec, pvs_, SVvec,self,ips2cuter)
            
            Done["Candidate"], Done["mu1"], Done["mu2"], Done['kaon'], Done["PV"] = b, mu1, mu2, kaon, PV
            #Done['longTracks'] = TES['Rec/Track/Best']
            CandidateInfo = {}
            CandidateInfo["evt"] = self.COUNTER["EVT"] + self.evt_of
            CandidateInfo["DOCA"] = muonsDOCA_
            CandidateInfo["mu1iso"] = isolations[0]
            CandidateInfo["mu2iso"] = isolations[1]
            CandidateInfo["mu1iso5"] = isolations5[0]
            CandidateInfo["mu2iso5"] = isolations5[1]
            CandidateInfo["kiso"] = isolations[2]
            CandidateInfo["kiso5"] = isolations5[2]
            CandidateInfo["Bip_r"], CandidateInfo["Bips_r"] = Double(0.), Double(0.)
            self.Geom.distance(b, Done["refittedPV"],CandidateInfo["Bip_r"], CandidateInfo["Bips_r"])
            CandidateInfo["Bips_r"] = float(psqrt(CandidateInfo["Bips_r"]))
            CandidateInfo["Bip_r"] = float(CandidateInfo["Bip_r"])
            CandidateInfo["kaon_eta"] = ETA(kaon)
            CandidateInfo["ipscheck"] = ipscheck
            CandidateInfo["JPsiChi2"] = jpsi.endVertex().chi2()        
            CandidateInfo["Vchi2"] = VCHI2(b.endVertex())
            CandidateInfo["k1ips"] = psqrt(kips2)
            CandidateInfo["mu1ips"] = psqrt(mu1ips2)
            CandidateInfo["mu2ips"] = psqrt(mu2ips2)
            CandidateInfo["JPsiMass"] = M(jpsi)
            CandidateInfo["JPsi_Dsig"] = jpsi_dis
            CandidateInfo["Bmass"] = M(b)
            CandidateInfo["Bmass_JC"] = DTF_FUN(M,True,"J/psi(1S)")(b)
            CandidateInfo["Bpt"] = Bpt
            CandidateInfo["mu1mip"] = mu1ippvs_
            CandidateInfo["mu2mip"] = mu2ippvs_
            CandidateInfo["mu1ip"] = mu1ip_
            CandidateInfo["mu2ip"] = mu2ip_
            CandidateInfo["longTracks"] = len(Done[longTracksName])
            
            CandidateInfo["mu1ismu"] = ISMUON(mu1)
            CandidateInfo["mu2ismu"] = ISMUON(mu2)
            CandidateInfo["Blife_ps"] = Blife_
            CandidateInfo["Blife_ps_r"] = ctau_r(b)*light_cte
            CandidateInfo["Bdissig_r"] = psqrt(Dis2_r(b))
            CandidateInfo["JPsi_Dsig_r"] = psqrt(Dis2j_r(jpsi))
            CandidateInfo["mu1p1"] = PX(mu1)
            CandidateInfo["mu1p2"] = PY(mu1)
            CandidateInfo["mu1p3"] = PZ(mu1)
            CandidateInfo["mu1pt"] = PT(mu1)
            CandidateInfo["mu2pt"] = PT(mu2)
            CandidateInfo["mu2p1"] = PX(mu2)
            CandidateInfo["mu2p2"] = PY(mu2)       
            CandidateInfo["mu2p3"] = PZ(mu2)
            CandidateInfo["mu1o1"] = o1.x()
            CandidateInfo["mu1o2"] = o1.y()
            CandidateInfo["mu1o3"] = o1.z()
            CandidateInfo["mu2o1"] = o2.x()
            CandidateInfo["mu2o2"] = o2.y()
            CandidateInfo["mu2o3"] = o2.z()
            CandidateInfo["k1p1"] = PX(kaon)
            CandidateInfo["k1p2"] = PY(kaon)
            CandidateInfo["k1p3"] = PZ(kaon)
            CandidateInfo["k1ptot"] = P(kaon)
            CandidateInfo["k1_eta"] = ETA(kaon)
            CandidateInfo["QK"] = Q(kaon)          
            CandidateInfo["PIDmu1"] = PIDmu(mu1)   
            CandidateInfo["PIDmu2"] = PIDmu(mu2)   
            CandidateInfo["PIDk"] = PIDK(kaon)    
            CandidateInfo["PIDkp"] = PIDK(kaon) - PIDp(kaon)
            CandidateInfo["SV1"] = VX(b.endVertex())           
            CandidateInfo["SV2"] = VY(b.endVertex())           
            CandidateInfo["SV3"] = VZ(b.endVertex())          
            CandidateInfo["evtNum"] = TES["Rec/Header"].evtNumber()
            CandidateInfo["runNum"] = TES["Rec/Header"].runNumber()          
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["Bips"] = Bips
            CandidateInfo["fIPS"] = fIPS
            CandidateInfo["Bip" ] =  Bip
            CandidateInfo["Bdissig"]= sigDOFS
            CandidateInfo["dDsig"] = dDsig
            CandidateInfo["dDsig_r"] = psqrt(Dis2_r(jpsi))
            CandidateInfo["BmassAlt"] =  sqrt(IM2(Jp,kp,MJPsi,KMASS))
            CandidateInfo["C_angle"] =  angleToflight(B1, B2)
            CandidateInfo["mu1_track_Chi2"] = TRCHI2 (mu1)
            CandidateInfo["mu2_track_Chi2"] = TRCHI2 (mu2)

            CandidateInfo["mu1_track_Chi2DoF"] = TRCHI2DOF (mu1)
            CandidateInfo["mu2_track_Chi2DoF"] = TRCHI2DOF (mu2)

            CandidateInfo["kaon_track_Chi2"] = TRCHI2 (kaon)
            CandidateInfo["kaon_track_Chi2DoF"] = TRCHI2DOF (kaon)

            KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
            CandidateInfo["mu1_track_CloneDist"] = KULLBACK(mu1)
            CandidateInfo["mu2_track_CloneDist"] = KULLBACK(mu2)
            CandidateInfo["kaon_track_CloneDist"] = KULLBACK(kaon)
           
            CandidateInfo["xtype"] = xtype
            CandidateInfo["bid"] = bid
            CandidateInfo["NPV"] = pvs_.size()
            CandidateInfo["lessIPSmu"] = min(CandidateInfo["mu1ips"], CandidateInfo["mu2ips"])
           
            CandidateInfo["LF_time"], CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] = Double(0.), Double(0.), Double(0.)
            sc = self.LifeFitter.fit ( Done["refittedPV"], b , CandidateInfo["LF_time"],  CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] )

            CandidateInfo["LF_time"] = CandidateInfo["LF_time"]*1000
            CandidateInfo["LF_terr"] = CandidateInfo["LF_terr"]*1000
            theDira = DIRA(Done["refittedPV"])
            CandidateInfo["DIRA"] = theDira(b)


            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
                      
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    tup.column(key,CandidateInfo[key])
                tup.write()
                
            if self.DST:
                self.addedKeys = CandidateInfo.keys()
                self.addedKeys.sort()
                for i in range(len(self.addedKeys)):
                    j = 500 + i
                    b.addInfo(j,CandidateInfo[self.addedKeys[i]])
            if self.DEBUG: print "Bender has found a "+ self.decayname + " candidate"
            
        self.setFilterPassed( True )
        self.COUNTER["Sel"] += 1
        return SUCCESS
    
    def finalize(self):
        print "B+ --> J/psi K+ counter. \n -----------"
        print self.COUNTER
        if self.DST:
            print "[||] Keys for added Info", self.name()
            for i in range(len(self.addedKeys)):
                print 500+i, self.addedKeys[i]
        print "../../../../../"
        return AlgoMC.finalize(self)
