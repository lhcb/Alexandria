from SomeUtils.alyabar import *
from Bender.MainMC import *
from math import sqrt as s_q_r_t
DOCATHRESHOLD = 0.15  # for conus
MU = 105.6583567
s3097 = 3097. * 3097.
def pointer1(x,p1,p2):
   """Returns the pointer used actually in charged-trigger,a normalized var
iable in which the
   true vertices are close to origin, and minimum bias is distributed rando
mly in range (0,1)
   """
   pt=vtmod(p1)+vtmod(p2)
   p=p1+p2
   num=vmod(p)*sin(ACO(x,p))
   den=(num+pt)

   return num/den

def pointer2(x,p1,p2):
    """Returns the pointer used actually in charged-trigger,a normalized variabl
e in which the
    true vertices are close to origin, and minimum bias is distributed randomly
in range (0,1)
    """
    pt=vtmod(p1)+vtmod(p2)
    p=p1+p2
    num=vmod(p)*ACO(x,p)
    den=(num+pt)

    return num/den

def InCono(track,aSV,rSV,angulo):

    stb = track.states().at(0).ptr()
    x = vector(stb.x(),stb.y(),stb.z())
    direc = vector(stb.tx(),stb.ty(),1)
    doca_tv = dpr(aSV,x,unit)
    alpha = ACO(unit,rSV)
    return doca_tv < DOCATHRESHOLD and alpha < angulo, x, direc

    
def IsHltGood(o,momentum,mu1point,mu1pVec,PV):
     hhh = closest_point(o,momentum,mu1point,mu1pVec)
     out = {}
     pete = vtmod(momentum)
     if not hhh: return False, False, False
     rv = hhh[0] - PV
     
     DOCA_b = vmod(hhh[1] - hhh[2])
     ZZ = rv[2]
     fc = pointer2(rv,momentum,mu1pVec)
     a = DOCA_b < 0.2
     return a, a and ZZ > 0 and ZZ<30 and fc <0.4 and pete > 2., rv  ## TWo booleans


def lookInCono(cono, tracks, prefix = ""):
    out = {}
    out[prefix + "tracks"] = 0
    out[prefix + "ipTracks"] = 0
    out[prefix+ "ipLongTracks"] = 0
    out[prefix + "pt"] = float(0)
    
    for track in tracks:
        st0 = track.states().at(0).ptr()
        x = vector(st0.x(),st0.y(),st0.z())
        v = vector(st0.tx(),st0.ty(),1.)
        bul = cono.acceptTrack(x,v)
        if not bul or isinstance(bul,str): continue
        IP = dpr(cono.PV,x,v)
        hasIP = IP > 0.1
        isL = track.isLong()
        pete = 0.
        if "p" in dir(st0):
            p = vunit(v)*st0.p()
            pete = vtmod(p)
        c1 = hasIP*isL
        out[prefix + "tracks"] += 1
        out[prefix +"ipTracks"] += int(hasIP)
        out[prefix+ "ipLongTracks"] += int(c1)
        out[prefix + "pt"] +=pete*c1
    return out
            
def scanConos(o1,p1,o2,p2,PV, tracks,list, prefix = ""):
    out = {}
    conos = {}
    for angle in list:
        aux = str(angle)
        out[prefix + "tracks_"+ aux] = 0
        out[prefix + "ipTracks_"+aux] = 0
        out[prefix + "ipLongTracks_"+aux] = 0
        out[prefix + "pt_" + aux] = float(0)
        conos[angle] =  Cono(o1,p1, o2,p2,angle,PV)
    
    for track in tracks:
        st0 = track.states().at(0).ptr()
        x = vector(st0.x(),st0.y(),st0.z())
        v = vector(st0.tx(),st0.ty(),1.)
        for angle in list:
            aux = str(angle)
            bul = conos[angle].acceptTrack(x,v)
            if not bul or isinstance(bul,str): continue
            IP = dpr(conos[angle].PV,x,v)
            hasIP = IP > 0.1
            isL = track.isLong()
            pete = 0.
            if "p" in dir(st0):
                p = vunit(v)*st0.p()
                pete = vtmod(p)

            c1 = hasIP*isL
            out[prefix + "tracks_" + aux] += 1
            out[prefix +"ipTracks_"+aux] += int(hasIP)
            out[prefix+ "ipLongTracks_"+aux] += int(c1)
            out[prefix + "pt_"+aux] +=pete*c1
    return out
    

def compatibleVertices(particles, tracksInEvent, PV, opt = 'longTracks'):
    out = len(particles)* [0.]
    lines = []
    for particle in particles:
        track = particle.proto().track()
        o = track.position()
        p = track.momentum()
        pos = vector(o.x(), o.y(), o.z())
        mom = vector(p.x(), p.y(), p.z())


        lines.append([pos,mom])
    
    for track in tracksInEvent:
        if track.type() != 3 and opt == 'longTracks': continue
        o = track.position()
        p = track.momentum()
        pos_2 = vector(o.x(), o.y(), o.z())
        mom_2 = vector(p.x(), p.y(), p.z())

        isInList = 0
        pt = vtmod(mom_2)
        for line in lines:
            if ratio001(pt, vtmod(line[1])) < 0.0001:
                isInList = 1
                break
        if isInList: continue
        for i in range(len(lines)):
            good, hltgood, rV = IsHltGood(pos_2,mom_2, lines[i][0], lines[i][1] ,PV)
#            print "hltgood ", hltgood
            out[i] += hltgood

    return out


def compatibleVerticesByTrack(particles, tracksInEvent, PVS):
    out = len(particles)* [{1:0, 2:0, 3:0, 4:0, 5:0}]
    mypvs = []
    for PV in PVS: mypvs.append(vector(PV.position().x(), PV.position().y(), PV.position().z()))
    lines = []
    for particle in particles:
        track = particle.proto().track()
        o = track.position()
        p = track.momentum()
        pos = vector(o.x(), o.y(), o.z())
        mom = vector(p.x(), p.y(), p.z())


        lines.append([pos,mom])
    
    for track in tracksInEvent:
        if track.type() > 5: continue
        o = track.position()
        p = track.momentum()
        o1 = vector(o.x(), o.y(), o.z())
        p1 = vector(p.x(), p.y(), p.z())
        iplist = []
        for pv in mypvs: iplist.append(dpr(pv, o1, p1))
        iplist.sort()
        mip = iplist[0]
        if mip < 0.3: continue
        isInList = 0
        pt = vtmod(p1)
        for line in lines:
            if ratio001(pt, vtmod(line[1])) < 0.0001:
                isInList = 1
                break
        if isInList: continue
        for i in range(len(lines)):
           o2, p2 = lines[i][0], lines[i][1]
           SV, DOCA, alpha = InCone(o1,p1,o2,p2)
           hhh = closest_point(o1,p1,vector(0,0,0), vector(0,0,1))
           DOCAZ = vmod(hhh[0] - hhh[1])
           if DOCA<0.15 and vtmod(SV)>5 and vtmod(SV)<15 and SV[2] < 600 and SV[2] > -100: out[i][track.type()] += 1              
              

    return out

MPI = 139.57061
def compatibleKs(mu1, mu2,SV, tracksInEvent, mypvs):
    outK0 = [0.,0.]
    outKplus = 0.
    
    lines = []
    for particle in [mu1,mu2]:
        track = particle.proto().track()
        o = track.position()
        p = track.momentum()
        pos = vector(o.x(), o.y(), o.z())
        mom = vector(p.x(), p.y(), p.z())
        lines.append([pos,mom])
    pV0 = lines[0][1] + lines[1][1]
    Mpipi =sqrt(IM2(lines[0][1],lines[1][1], MPI, MPI))
    for track in tracksInEvent:
        if track.type() !=3 : continue
        o = track.position()
        p = track.momentum()
        o1 = vector(o.x(), o.y(), o.z())
        p1 = vector(p.x(), p.y(), p.z())
        
        isInList = 0
        pt = vtmod(p1)
        iplist = []
        for pv in mypvs: iplist.append(dpr(pv, o1, p1))
        iplist.sort()
        mip = iplist[0]
        if mip < 0.2: continue

       
        
        for line in lines:
            if ratio001(pt, vtmod(line[1])) < 0.0001:
                isInList = 1
                break
        if isInList: continue

        pKplus = p1 + pV0
        SV, DOCA, alpha = InCone(o1,p1,SV,pV0)
        iplist = []
        for pv in mypvs: iplist.append(dpr(pv, SV, pKplus))
        iplist.sort()
        Kplusip = iplist[0]
        MK3pi = sqrt(IM2(pV0,p1,Mpipi,MPI))
        if DOCA < 0.4 and Kplusip <0.4 and MK3pi > 450 and MK3pi < 520: outKplus += 1
        
        for i in range(len(lines)):
           o2, p2 = lines[i][0], lines[i][1]
           SV, DOCA, alpha = InCone(o1,p1,o2,p2)
           #hhh = closest_point(o1,p1,vector(0,0,0), vector(0,0,1))
           #DOCAZ = vmod(hhh[0] - hhh[1])
           if DOCA>0.4: continue
           mass = sqrt(IM2(p1,p2, MPI,MPI))
           if mass < 470 or mass > 530: continue
           px = p1+p2
           iplist = []
           for pv in mypvs: iplist.append(dpr(pv, SV, px))
           iplist.sort()
           if iplist[0] > 0.5: continue
           outK0[i] += 1
        

    return outK0, outKplus
 


def compatibleVertices2(particles, isoProtos, PV, pvs, SV, self,ips2cuter):
    out = len(particles)* [0.]
    out5 = len(particles)* [0.]
    lines = []
    for particle in particles:
        track = particle.proto().track()
        o = track.position()
        p = track.momentum()
        q = track.charge()
        pos = vector(o.x(), o.y(), o.z())
        mom = vector(p.x(), p.y(), p.z())

        lines.append([pos,mom,q,particle])
        
    for proto in isoProtos:
        track = proto.proto().track()
        o = track.position()
        p = track.momentum()
        q = track.charge()
        pos_2 = vector(o.x(), o.y(), o.z())
        mom_2 = vector(p.x(), p.y(), p.z())
        isInList = 0
        pt = vtmod(mom_2)
        for line in lines:
            if ratio001(pt, vtmod(line[1])) < 0.0001:
                isInList = 1
                break
        if isInList: continue
        for i in range(len(lines)):
            good, hltgood, hltgood1, rV, fc, fc1 = IsHltGood2(pos_2,mom_2, lines[i][0], lines[i][1] ,PV)
            if (track.type()==3):
                out[i] += hltgood
#                if (hltgood):
#                    print "isisold", i, out[i]
            incone = InCone(lines[i][0], lines[i][1], pos_2, mom_2)
            vtx = incone[0]
            doca = incone[1]
            angle = incone[2]
            
            ips = psqrt(ips2cuter(proto))   #### Will be negative if the squared is neg, thus it will not be selected
          
            pvdis = (vtx[2]-PV[2])/abs(vtx[2]-PV[2])*dis(vtx,PV)
            svdis = (vtx[2]-SV[2])/abs(vtx[2]-SV[2])*dis(vtx,SV)    
#            print "angle ", angle 
#            print "doca ", doca 
#            print "fc ", fc 
#            print "ips ", ips 
#            print "svdis ", svdis 
#            print "pvdis ", pvdis 
            if (angle <0.27 and fc<0.60 and doca<0.13 and ips>3.0 and svdis>-0.15 and svdis<30. and pvdis>0.5 and pvdis<40. and track.type()==3) :
#                print "isiso ", i, " ", out5[i]
                out5[i] += 1
#    print "out  ", out[0], out[1]            
#    print "out5 ", out5[0], out5[1]            
    return out, out5

def compatibleVerticesK(particles, isoProtos, PV, pvs, SV, self,ips2cuter):
    #out = len(particles)* [0.]
    out5 = len(particles)* [0.]
    lines = []
    for particle in particles:
        track = particle.proto().track()
        o = track.position()
        p = track.momentum()
        q = track.charge()
        pos = vector(o.x(), o.y(), o.z())
        mom = vector(p.x(), p.y(), p.z())

        lines.append([pos,mom,q,particle])
        
    for proto in isoProtos:
        track = proto.proto().track()
        o = track.position()
        p = track.momentum()
        q = track.charge()
        pos_2 = vector(o.x(), o.y(), o.z())
        mom_2 = vector(p.x(), p.y(), p.z())
        isInList = 0
        pt = vtmod(mom_2)
        for line in lines:
            if ratio001(pt, vtmod(line[1])) < 0.0001:
                isInList = 1
                break
        if isInList: continue
        for i in range(len(lines)):  ## lines are the daughters, eg, [mu1, mu2]
            #good, hltgood, hltgood1, rV, fc, fc1 = IsHltGood2(pos_2,mom_2, lines[i][0], lines[i][1] ,PV)
            #if (track.type()==3):
            #    out[i] += hltgood
#                if (hltgood):
#                    print "isisold", i, out[i]
            incone = InCone(lines[i][0], lines[i][1], pos_2, mom_2)
            vtx = incone[0]
            doca = incone[1]
            angle = incone[2]
            
            ips = psqrt(ips2cuter(proto))   #### Will be negative if the squared is neg, thus it will not be selected
          
            pvdis = (vtx[2]-PV[2])/abs(vtx[2]-PV[2])*dis(vtx,PV)
            svdis = (vtx[2]-SV[2])/abs(vtx[2]-SV[2])*dis(vtx,SV)    
#            print "angle ", angle 
#            print "doca ", doca 
#            print "fc ", fc 
#            print "ips ", ips 
#            print "svdis ", svdis 
#            print "pvdis ", pvdis 
            if (doca<0.2 and ips>5.0 and vtmod(SV) > 5. and track.type()==3) :
#                print "isiso ", i, " ", out5[i]
                out5[i] += 1
#    print "out  ", out[0], out[1]            
#    print "out5 ", out5[0], out5[1]            
    return out5

def InCone(o1,p1,o2,p2):
    kkk =vector(0.,0.,0.)
    hhh = closest_point(o1,p1,o2,p2)
    if not hhh: return kkk, -1., -1.
    DOCA = vmod(hhh[1] - hhh[2])
    alpha = ACO(p1,p2)
    return hhh[0], DOCA, alpha


def IsHltGood2(o,momentum,mu1point,mu1pVec,PV):
    hhh = closest_point(o,momentum,mu1point,mu1pVec)
    out = {}
    pete = vtmod(momentum)
    if not hhh: return False, False, False, PV,0.,0.
    rv = hhh[0] - PV
    DOCA_b = vmod(hhh[1] - hhh[2])
    ZZ = rv[2]
    # pointer2 is the old pointer (without the cosine)
    fc = pointer1(rv,momentum,mu1pVec)
    # pointer1 is the pointer with the cosine
    fc1 = pointer1(rv,momentum,mu1pVec)
#    print "fc ", fc
#    print "DOCA_b ", DOCA_b
#    print "ZZ ", ZZ
#    print "pete ", pete
    
    a = DOCA_b < 0.2
    return a, a and ZZ > 0 and ZZ<30 and fc <0.4 and pete > 2.,  a and ZZ > 0 and ZZ<30 and fc1 <0.5 and pete > 2., rv, fc, fc1  ## TWo booleans


def anotherMuon(particles,Bs):
    mu1, mu2 = Bs(1).proto().track(), Bs(2).proto().track()
    sv = Bs.vertex().position()
    sv = vector(sv.x(), sv.y(), sv.z())
    pt1, pt2 = mu1.pt(), mu2.pt()
    p1,p2 = mu1.momentum(), mu2.momentum()

    p1 = vector(p1.x(), p1.y(), p1.z())
    p2 = vector(p2.x(), p2.y(), p2.z())
    out, ptout, jpsi = 0, [0.], [9999.]
    
    for particle in particles:
        track = particle.proto().track()
        pt = track.pt()
        if min(ratio001(pt1,pt), ratio001(pt2,pt)) < 0.0001: continue
        o, p = track.position(), track.momentum()
        o = vector(o.x(), o.y(), o.z())
        p = vector(p.x(), p.y(), p.z())

        if dpr(sv, o, p) < 0.2:  ## IP 2 secondary vertex
            out += 1
            jpsi.append(sqrt(min(abs(IM2(p,p1, MU, MU) - s3097), abs(IM2(p,p2, MU, MU)-s3097)))) ### s3097 is a variable defined on the top
            ptout.append(pt)
    return  out, ptout, jpsi

def anotherMuon_v2(particles,Bs):
    mu1, mu2 = Bs.daughters().at(0).proto().track(), Bs.daughters().at(1).proto().track()
    sv = Bs.endVertex().position()
    sv = vector(sv.x(), sv.y(), sv.z())
    pt1, pt2 = mu1.pt(), mu2.pt()
    p1,p2 = mu1.momentum(), mu2.momentum()

    p1 = vector(p1.x(), p1.y(), p1.z())
    p2 = vector(p2.x(), p2.y(), p2.z())
    out, ptout, jpsi = 0, [0.], [9999.]
    
    for particle in particles:
        track = particle.proto().track()
        pt = track.pt()
        if min(ratio001(pt1,pt), ratio001(pt2,pt)) < 0.0001: continue
        o, p = track.position(), track.momentum()
        o = vector(o.x(), o.y(), o.z())
        p = vector(p.x(), p.y(), p.z())

        if dpr(sv, o, p) < 0.2:  ## IP 2 secondary vertex
            out += 1
            jpsi.append(sqrt(min(abs(IM2(p,p1, MU, MU) - s3097), abs(IM2(p,p2, MU, MU)-s3097)))) ### s3097 is a variable defined on the top
            ptout.append(pt)
    return  out, ptout, jpsi

def dis(o1,o2):
    """
    Returns the distance between two 'points'
    """
    d=o1-o2
    m = vmod(d)
    return m
