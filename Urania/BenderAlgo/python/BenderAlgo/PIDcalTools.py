######
##USC

import GaudiPython
LHCbID = GaudiPython.gbl.LHCb.LHCbID
MuonCoord = GaudiPython.gbl.LHCb.MuonCoord
from SomeUtils.alyabar import *

gaudi = GaudiPython.AppMgr()

gaudi.initialize()
TES = gaudi.evtsvc()

T_extrapolator = gaudi.toolsvc().create("TrackMasterExtrapolator",interface="ITrackExtrapolator")
T_measProvider = gaudi.toolsvc().create( "MeasurementProvider",interface="IMeasurementProvider");

det = gaudi.detsvc()
MUONC = det["/dd/Structure/LHCb/DownstreamRegion/Muon"]

def PrintMuonCoords(muon):
    muid= muon.proto().muonPID()
    if not muid:
        print "it is not a muon, I guess"
        return "Vaite a merda"
    lhcbids = muid.muonTrack().lhcbIDs()
    out = []
    for id in lhcbids:
        #thing = {}
        print "x = ",T_measProvider.measurement(id,0).measure()," +- ",T_measProvider.measurement(id,0).errMeasure()
#        thing["ymeas"] = T_measProvider.measurement(id,1)
        print "y = ",T_measProvider.measurement(id,1).measure()," +- ",T_measProvider.measurement(id,1).errMeasure()
        print "z = ",T_measProvider.measurement(id,2).measure()," +- ",T_measProvider.measurement(id,2).errMeasure()
        #out.append(thing)
def SaveMuonCoords(muon,algo, name):
    tup = algo.nTuple( name )
    muid= muon.proto().muonPID()
    if not muid:
        print "it is not a muon, I guess"
        return "Vaite a merda"
    lhcbids = muid.muonTrack().lhcbIDs()
    out = []
    for id in lhcbids:
        #thing = {}
        tup.column("x",T_measProvider.measurement(id,0).measure())#," +- ",T_measProvider.measurement(id,0).errMeasure()
        tup.column("y",T_measProvider.measurement(id,1).measure())#," +- ",T_measProvider.measurement(id,1).errMeasure()
        tup.column("z",T_measProvider.measurement(id,2).measure())#," +- ",T_measProvider.measurement(id,2).errMeasure()
        tup.write()
        #out.append(thing)        

## def HasL0Friend(mu, TES):
##     muid= mu.proto().muonPID()
##     if not muid:
##         print "it is not a muon, I guess"
##         return "Vaite a merda"
##     lhcbids = muid.muonTrack().lhcbIDs()
    
##     chanIds = map(lambda x: x.channelID(),lhcbids)
##     l0s = TES["Hlt/Track/L0AllMuons"]
##     for l0 in l0s:
##         ids = l0.lhcbIDs()
##         for id in ids:
##             if id.channelID() in chanIds : return True ### Just one hit
##     return False

def CommonIDs(thing1,thing2):
    ids1 = thing1.lhcbIDs()
    ids2 = thing2.lhcbIDs()
    chanIds1 = map(lambda x: x.channelID(),ids1)
    chanIds2 = map(lambda x: x.channelID(),ids2)
    out = []
    for id in chanIds2:
        if id in chanIds1: out.append(id)
    return out   
    
def OnlStatus(mu, TES, path = "Hlt/Track/L0AllMuons"):  #segments are in MuonSegmentForL0Single?
    muid= mu.proto().muonPID()
    if not muid:
        print "it is not a muon, I guess"
        return 0. ##"What are you doing?"
    mutrack = muid.muonTrack()
    onls = TES[path]
    if not onls:
        return 0.
    out = 0.
    for onl in onls:
        ids = onl.lhcbIDs()
        com = CommonIDs(mutrack,onl)
        if len(com) == len(ids): return 1.
        if len(com): out = 0.5
    return out


def getMuonHits(TES,tm =T_measProvider):
    """get all hits in muon chambers
    """
    coords = TES["Raw/Muon/Coords"]
    coords = coords.containedObjects()
    MuonProvider={}

    for coord in coords:

        tile=coord.key()
        id = LHCbID(tile)
        
        ## for each station get hits and measurements: [xmeas,ymeas],id
        istation = tile.station()
        
        if istation!=0:
            xmeas = T_measProvider.measurement(id,0)
            ymeas = T_measProvider.measurement(id,1)
           
            if istation not in MuonProvider.keys(): MuonProvider[istation]=[[[xmeas,ymeas],id]]
            else: MuonProvider[istation].append([[xmeas,ymeas],id])

    return MuonProvider ### This is a dictionary !!!

## def my_track(track):
#     ids=track.lhcbIDs()
##     mids=[]
##     for i in ids:
##         if i.isMuon(): mids.append(i)
##     stations=map(lambda x: x.muonID().station(),mids)
##    
##     st1=lhcbID1.muonID().station()
##     MuonProvider=getMuonHits(TES,T_measProvider)
##     for m in MuonProvider[st1]:
##         if lhcbID1.channelID()==m[1].channelID():
##             xmeas=m[0][0]
##             ymeas=m[0][0]
##     xpos=xmeas.measure()
##     xerr=xmeas.errMeasure()
    
## 
def makeStates(track,tm = T_extrapolator,mnc = MUONC):            
    states={}
    for st in range(5):
        muState=track.firstState()
        muStateC=muState.clone()

        code=T_extrapolator.propagate(muStateC,MUONC.getStationZ(st))
        if code.isFailure(): continue
        else: states[st]=muStateC

    return states

def getPointAtZ(track, z):
    """Just the coordinates of the extrapolation to a given Z"""
    
    st1 = track.firstState()
    st = st1.clone()
    code = T_extrapolator.propagate(st,z)
    pos = st.position()
    return pos.x(), pos.y(), pos.z()

def getTheClosest(track, othertracks, z):
    """The distance to the closest track at a given z, using extrapolation"""
    x, y , z = getPointAtZ(track,z)
    point1 = vector(x,y,z)
    l = []
    for tr in othertracks:
        if ratio001(track.pt(),tr.pt()) < 0.001: continue
        x2, y2 , z2 = getPointAtZ(tr,z)
        point2 = vector(x2,y2,z2)
        l.append(vmod(point1-point2))
    l.sort()
    if not l: return -1
    return l[0]

