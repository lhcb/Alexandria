#! /usr/bin/env python

from Bender.MainMC import *                

class RunInfo(AlgoMC):
    
    def analyse(self):
        
        self.COUNTER["EVT"] += 1
        self.setFilterPassed( False )
        
        TES = appMgr().evtsvc()
        
        odin = TES["DAQ/ODIN"]
        runNum,evtNum = odin.runNumber(), odin.eventNumber()
        bid = float(odin.bunchId())
        xtype = float(odin.bunchCrossingType())
       
        tup = self.nTuple( self.name() )
        
        tup.column("evt", self.COUNTER["EVT"] + + self.evt_of)
        tup.column("runNum", runNum)
        tup.column("evtNum", evtNum)
        tup.column("bcid", bid)
        tup.column("xtype", xtype)
        tup.write()
        
        return SUCCESS

    def finalize(self):
        print self.name()
        print self.COUNTER
        return AlgoMC.finalize(self)

