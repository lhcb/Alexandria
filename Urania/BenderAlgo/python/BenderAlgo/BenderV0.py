#! /usr/bin/env python
import isolation
from SomeUtils.alyabar import *   
from LinkerInstances.eventassoc import * 
from Bender.MainMC import *                
from scipy import random as rnd
import GaudiPython
import GaudiKernel
import cPickle
from select import selectVertexMin
from ROOT import *
import miguelito

#import BenderTools.TisTosMC
import BenderTools.TisTos
##from extrafunctions import PromptKsCounter ##only when used here
from BenderAlgo.extrafunctions import PromptKsCounter
#selectVertexMin = LoKi.SelectVertex.selectMin
##prueba = Loki.Hybrid.TupleTool
GBL = GaudiPython.gbl.LHCb
units = GaudiKernel.SystemOfUnits
c_light = 2.99792458e+8 * units.m /units.s
light_cte = 1000./c_light

longTracksName = "longTracks"

class B2QQ(Algo): # used to be AlgoMC
    def initialize ( self ) :

        sc = Algo.initialize ( self )# used to be AlgoMC ## initialize the base class
#        if not self.TRIGGER: return SUCCESS
        if sc.isFailure() : return sc

        #
        ## container to collect trigger information, e.g. list of fired lines 
        #
        triggers = {}
        triggers ['KS0'] = {} ## slot to keep information for KS
        triggers ['Miguel'] = {} ## slot to keep information for KS
        triggers ['RnS'] = {}
        #
        ## the lines to be investigated in details
        #
        lines    = {}
        lines    [ "KS0" ] = {} ## trigger lines for KS
        lines    [ "Miguel" ] = {} ## trigger lines for KS
        lines    [ "RnS"] = {}
        #
        ## six mandatory keys:
        #
        lines    [ "KS0" ][   'L0TOS' ] = 'L0.*Decision'
        lines    [ "KS0" ][   'L0TIS' ] = 'L0.*Decision'
        lines    [ "KS0" ][ 'Hlt1TOS' ] = 'Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision'
        lines    [ "KS0" ][ 'Hlt1TIS' ] = 'Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision'
        lines    [ "KS0" ][ 'Hlt2TOS' ] = 'Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Transparent)(?!PassThrough).*Decision'
        lines    [ "KS0" ][ 'Hlt2TIS' ] = 'Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Transparent)(?!PassThrough).*Decision'
        lines    [ "Miguel" ][   'L0TOS' ] = 'L0.*Decision'
        lines    [ "Miguel" ][   'L0TIS' ] = 'L0.*Decision'
        lines    [ "Miguel" ][ 'Hlt1TOS' ] = miguelito.HLT1
        lines    [ "Miguel" ][ 'Hlt1TIS' ] = miguelito.HLT1
        lines    [ "Miguel" ][ 'Hlt2TOS' ] = miguelito.HLT2
        lines    [ "Miguel" ][ 'Hlt2TIS' ] = miguelito.HLT2
        lines    [ "RnS" ][   'L0TOS' ] = 'L0.*Decision'
        lines    [ "RnS" ][   'L0TIS' ] = 'L0.*Decision'
        lines    [ "RnS" ][   'Hlt1TOS' ] = '(Hlt1DiMuonLowMassDecision|Hlt1DiMuonNoL0Decision|Hlt1TrackMuonMVADecision|Hlt1TrackMuonDecision)'
        lines    [ "RnS" ][   'Hlt1TIS' ] = '(Hlt1DiMuonLowMassDecision|Hlt1DiMuonNoL0Decision|Hlt1TrackMuonMVADecision|Hlt1TrackMuonDecision)'
        lines    [ "RnS" ][   'Hlt2TOS' ] = 'Hlt2DiMuonSoftDecision'
        lines    [ "RnS" ][   'Hlt2TIS' ] = 'Hlt2DiMuonSoftDecision'

        sc = self.tisTos_initialize ( triggers , lines )
        if sc.isFailure() : return sc
        if not "RandomMuonOrder" in dir (self): self.RandomMuonOrder = False
        return SUCCESS   
    
    def analyse(self):
        
        TES = appMgr().evtsvc()
        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the bsloop
        Done["TES"] = TES
        if self.PROMPT_KS_COUNTER: PromptKsCounter(self,Done)
        
        self.COUNTER["EVT"] += 1
        
        self.setFilterPassed( False )

        #prt =self. COUNTER["EVT"] * 0.01   
        if self.DEBUG: print self.decayname, self.COUNTER["EVT"], "--DEBUG--"
        
        #TES = appMgr().evtsvc()
       
      
        pvs = self.vselect("pvs", ISPRIMARY)
        
        #if self.dataFormat != "MDST":
         #   if self.DEBUG and pvs.empty() and TES["Rec/Vertex/Primary"].size() : print "pvs.empty() but PV's in Vertex/Primary"
       
        bsloop = TES["/Event/"+self.LookIn+"/Particles"] 
        ##print "bsloop=",bsloop
        ##print "the path:","/Event/"+self.LookIn+"/Particles"
        ##print "TES dump:",TES.dump()
        
        if not bsloop :
            if self.DEBUG: print "Nothing to do", self.name()
            return SUCCESS
        if pvs.empty():
            if self.DEBUG: print "PVs empty"
            return SUCCESS 
        #bsloop = bsloop.containedObjects()
        #self.plot(bsloop.size(),"B candidates ",0.,40.,40)
        
        if not bsloop.size():
            if self.DEBUG: print "Nothing in bsloop", self.name()
            return SUCCESS

       
        muons_fV = self.select("mfv", (ABSID ==13))   ### Fot JPsi Veto Studies
        ips2cuter = MIPCHI2(pvs,self.geo())

        mulist = []  ## For Bc --> J/Psi mu nu Veto
        
        odin = TES["DAQ/ODIN"]
        runNum,evtNum = odin.runNumber(), odin.eventNumber()
        
        if self.MC_INFO or not ("bunchCrossingType") in dir(odin):
            ###print "oooooo xtype = -1 "
            xtype = -1.
            ptype = TES["Gen/Header"].collisions()[0].processType()
            mcpv = TES["Gen/Header"].numOfCollisions()
            bid = -1
            
            
        else:
            bid = float(odin.bunchId())
            xtype = float(odin.bunchCrossingType())
            ptype = -1.
            mcpv = -1.
            
        
        
        for mu in muons_fV:
            mulist.append(mu)
       
      
        Done[longTracksName] = [] ### For isolation
        #if self.dataFormat != "MDST":
        tracks = TES[self.RootInTES + 'Rec/Track/Best']
        ##print "tracks=",tracks
        self.plot(pvs.size(),"Event - primary vertices",0,20,20)
        self.plot(tracks.size(),"Event - best tracks",0,500,500)

        ##print "first option: ", self.get_(TES[self.RootInTES + 'Dimuon/Phys/TriggerTestLine/Muon1BDT'],True)
        ##print "second option: ", self.get_(self.RootInTES + '/Event/Dimuon/Phys/TriggerTestLine/Muon1BDT',True)
        ##print "third option: ", self.get_('/Event/Dimuon/Phys/TriggerTestLine/Muon1BDT', True)  

        ##cacas1 = self.get_(self.RootInTES + '/Event/Dimuon/Phys/TriggerTestLine/Muon1BDT',True)
        ##xscacas2 = TES[self.RootInTES + '/Event/Dimuon/Phys/TriggerTestLine/Muon1BDT']
        
        ##print "cacas1:",cacas1
        ##print "dir(cacas1):",dir(cacas1)
        ##print "cacas2:",cacas2
        ##print "dir(cacas2):",dir(cacas2)

        #print "the GiGi path:",self.muon1BDT_path#'/Event/Dimuon/Phys/TriggerTestLine/Muon1BDT'
        muon1BDT_func = RELINFO(self.muon1BDT_path, 'MU_BDT', -2000.)
        muon2BDT_func = RELINFO(self.muon2BDT_path, 'MU_BDT', -2000.)
        ##print "muonBDT_func = ",muonBDT_func
        ##for caca1 in cacas1:
        ##    print "caca1:",caca1
        ##    print "dir(caca1):",dir(caca1)

        ##for caca2 in cacas2:
        ##    print "caca2:",caca2
        ##    print "dir(caca2):",dir(caca2)
                      
        for track in tracks:
            if track.type() == 3 and track.chi2PerDoF() < 5: Done[longTracksName].append(track)

        if self.MC_INFO: Done["link"]= linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")
       
        
        for b in bsloop:
            
#            print "INSIDE THE BSLOOP"
            if not b.endVertex():
                if self.DEBUG: "b does not have a end Vertex"
                self.COUNTER["weird"] +=1
                continue
            CandidateInfo = {}
            
            if self.RandomMuonOrder:
                r1 = rnd.random()
                r2 = rnd.random()
                b1 = (r1 > r2)
                mu1, mu2 = b.daughters().at(int(b1)), b.daughters().at(int(not b1))
                
            else: mu1, mu2 = b.daughters().at(0), b.daughters().at(1)

            #print "type(mu1),type(mu2)",type(mu1),type(mu2)
            
            self.COUNTER["MuMuCouplesAnalyzed"] += 1
            PVips2 = VIPCHI2( b, self.geo() )
            

            PV = self.bestVertex ( b )
            
            if not PV:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            if self.DEBUG:
                print "PV key ", PV.key()
                print "PV position ", VX(PV), VY(PV), VZ(PV)
                print "is Valid" , VVALID(PV)

           
            Done["Candidate"], Done["PV"], Done["mu1"], Done["mu2"] =  b, PV, mu1, mu2
            Done["refittedPV"] = PV.clone()
            self.PVRefitter.remove(b, Done["refittedPV"])
            Dis2 = VDCHI2( PV )
            Dis2_r = VDCHI2(  Done["refittedPV"] )
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(b.endVertex()), VY(b.endVertex()), VZ(b.endVertex()) )
        
            rSV = SVvec - PVvec
            ## if rSV[2] <0:
##                 if self.DEBUG: "negative DZ"
##                 self.COUNTER["weird"] +=1
##                 continue
            
            Bmass = M(b)
            sigDOFS = Dis2(b)
            mu1ips2, mu2ips2, Bips2 = ips2cuter(mu1), ips2cuter(mu2), PVips2(PV)
            ipscheck = min(sigDOFS, mu1ips2, mu2ips2, Bips2)
            #print "ipscheck=",ipscheck
            if ipscheck < 0:
                if self.DEBUG: print "error on IPS calculations"
                self.COUNTER["negSq"] +=1
                ### continue, Let's decide what to do afterwards...
            
            sigDOFS = psqrt(sigDOFS)
          
            o1, o2 = mu1.proto().track().position(), mu2.proto().track().position()
            mus3, pts3, jpsis3 = isolation.anotherMuon_v2(mulist, b)   
            pts3.sort()
            jpsis3.sort()

            isoProtos = []

            for proto in TES["/Event/Phys/StdNoPIDsPions/Particles"]:
                #Done[longTracksname].append(proto.proto().track())
                isoProtos.append(proto)
            isolations, isolations5 = isolation.compatibleVertices2([mu1,mu2], isoProtos,  PVvec, pvs, SVvec,self,ips2cuter)
            junk = isolation.compatibleVerticesK([mu1,mu2], isoProtos,  PVvec, pvs, SVvec,self,ips2cuter)
            isosRnS = isolation.compatibleVerticesByTrack([mu1,mu2], TES[self.RootInTES + "Rec/Track/Best"], TES[self.RootInTES + "Rec/Vertex/Primary"])
            isosRnSHLT1 = isolation.compatibleVerticesByTrack([mu1,mu2], TES[self.RootInTES + "Rec/Track/FittedHLT1VeloTracks"], TES[self.RootInTES + "Rec/Vertex/Primary"])            
            # Marco quantities ###

            mippvs = MIP( pvs, self.geo() )
            iptoPV = IP (PV, self.geo())
            
            dirAngle = DIRA (PV)
            ctau = CTAU (PV)
            ctau_r = CTAU (Done["refittedPV"])
            if "target" in dir(mu1): muDOCA = CLAPP (mu1.target(), self.geo())
            else: muDOCA = CLAPP (mu1, self.geo())
            
            mu1ippvs_ = mippvs (mu1) # IP
            mu2ippvs_ = mippvs (mu2)
            mu1ip_ = iptoPV (mu1)
            mu2ip_ = iptoPV (mu2)
            Bip_ = iptoPV (b)
            DirAngle_ = acos(dirAngle (b))
           
            
            Blife_ = ctau(b)*light_cte  #in picosecond
            muonsDOCA_ = muDOCA(mu2)
            ###
            p1 = vector(PX(mu1), PY(mu1), PZ(mu1))
            p2 = vector(PX(mu2), PY(mu2), PZ(mu2))
            
            B1 = [E(mu1), p1]
            B2 = [E(mu2), p2]

            p = p1 + p2
            pu = vunit(p)
            pl1 = vdot(p1,pu)
            pl2 = vdot(p2,pu)
            ap_alpha = (pl1-pl2)/(pl1+pl2)
            ap_pt = psqrt(vmod(p1)**2 - pl1**2)

            uSV = vunit(rSV)
            thing1 = closest_point(vector(o1.x(), o1.y(), o1.z()),p1,vector(0,0,0), vector(0,0,1))
            thing2 = closest_point(vector(o2.x(), o2.y(), o2.z()),p2,vector(0,0,0), vector(0,0,1))

            CandidateInfo = {}
            CandidateInfo["evt"] = self.COUNTER["EVT"] + self.evt_of
            CandidateInfo["MET"] = psqrt( vmod(p)**2 - (vdot(uSV,p))**2)
            CandidateInfo["ipscheck"] = ipscheck
            CandidateInfo["mu1iso"] = isolations[0]
            CandidateInfo["mu2iso"] = isolations[1]
            CandidateInfo["mu1iso5"] = isolations5[0]
            CandidateInfo["mu2iso5"] = isolations5[1]
            CandidateInfo["mu1isoK"] = junk[0]
            CandidateInfo["mu2isoK"] = junk[1]
            CandidateInfo["DOCAZ1"] =  vmod(thing1[0]-thing1[1])
            CandidateInfo["DOCAZ2"] =  vmod(thing2[0]-thing2[1])
            CandidateInfo["mu1Phi"] = PHI(mu1)
            CandidateInfo["mu2Phi"] = PHI(mu2)
            CandidateInfo["longTracks"] = len(Done[longTracksName])
            CandidateInfo["mu1ismu"] = ISMUON(mu1)
            CandidateInfo["mu2ismu"] = ISMUON(mu2)
            CandidateInfo["mu1GP"] = TrGHOSTPROB(mu1.proto().track())
            CandidateInfo["mu2GP"] = TrGHOSTPROB(mu2.proto().track())
            CandidateInfo["ProbNNmu1"] = PROBNNmu(mu1)
            CandidateInfo["ProbNNmu2"] = PROBNNmu(mu2)
            CandidateInfo["ProbNNghost1"] = PROBNNghost(mu1)
            CandidateInfo["ProbNNghost2"] = PROBNNghost(mu2)
            
            CandidateInfo["mu2_eta"] = ETA(mu2)
            CandidateInfo["mu1_eta"] = ETA(mu1)
            CandidateInfo["B_eta"] = ETA(b)
            
            CandidateInfo["xtype"] = xtype
            CandidateInfo["mcpv"] = mcpv
            CandidateInfo["ptype"] = ptype
            
           
            CandidateInfo["Bip_r"], CandidateInfo["Bips_r"] = Double(0.), Double(0.)
            self.Geom.distance(b, Done["refittedPV"],CandidateInfo["Bip_r"], CandidateInfo["Bips_r"])
            CandidateInfo["Bips_r"] = float(psqrt(CandidateInfo["Bips_r"]))
            CandidateInfo["Bip_r"] = float(CandidateInfo["Bip_r"])
            
            CandidateInfo["mu1p1"] = PX(mu1)
            CandidateInfo["mu1p2"] = PY(mu1)
            CandidateInfo["mu1p3"] = PZ(mu1)
            CandidateInfo["mu2p1"] = PX(mu2)
            CandidateInfo["mu2p2"] = PY(mu2)
            CandidateInfo["mu2p3"] = PZ(mu2)
            CandidateInfo["mu1o1"] = o1.x() 
            CandidateInfo["mu1o2"] = o1.y() 
            CandidateInfo["mu1o3"] = o1.z() 
            CandidateInfo["mu2o1"] = o2.x() 
            CandidateInfo["mu2o2"] = o2.y() 
            CandidateInfo["mu2o3"] = o2.z() 
            CandidateInfo["PIDmu1"] = PIDmu(mu1)
            CandidateInfo["PIDmu2"] = PIDmu(mu2)
            CandidateInfo["PIDk1"] = PIDK(mu1)
            CandidateInfo["PIDk2"] = PIDK(mu2)
            CandidateInfo["PIDe1"] = PIDe(mu1)
            CandidateInfo["PIDe2"] = PIDe(mu2)
            CandidateInfo["PIDp1"] = PIDp(mu1)
            CandidateInfo["PIDp2"] = PIDp(mu2)
            CandidateInfo["SV1"] = VX(b.endVertex())
            CandidateInfo["SV2"] = VY(b.endVertex())
            CandidateInfo["SV3"] = VZ(b.endVertex())
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["PV1_r"] = VX(Done["refittedPV"])
            CandidateInfo["PV2_r"] = VY(Done["refittedPV"])
            CandidateInfo["PV3_r"] = VZ(Done["refittedPV"])
            
            CandidateInfo["nTrBestPV"] = PV.tracks().size()
            CandidateInfo["dPhi"] = mu1.momentum().phi() - mu2.momentum().phi()
            
            CandidateInfo["ID1"] = ID(mu1)
            CandidateInfo["ID2"] = ID(mu2)            
            CandidateInfo["Bdissig"] = sigDOFS
            CandidateInfo["Bdissig_r"] = psqrt(Dis2_r(b))
            CandidateInfo["Bpt"] = PT(b)          
            CandidateInfo["Bips"] = psqrt(Bips2)
            CandidateInfo["Bmip"] = mippvs(b)
            CandidateInfo["Vchi2"] = VCHI2(b.endVertex())
            CandidateInfo["PVchi2"] = PV.chi2()
            CandidateInfo["PVchi2_r"] = Done["refittedPV"].chi2()
            
            CandidateInfo["mu1ttype"] = mu1.proto().track().type()
            CandidateInfo["mu2ttype"] = mu2.proto().track().type()
            
            CandidateInfo["Bmass"] = Bmass
            CandidateInfo["evtNum"] = evtNum
            CandidateInfo["runNum"] = runNum
            CandidateInfo["3muon"]=  mus3
            CandidateInfo["3muonMaxpt"] = pts3[-1]
            CandidateInfo["3muonMinjpsi"] =  jpsis3[0]
            CandidateInfo["JPsiChi2"] = CandidateInfo["Vchi2"]
            CandidateInfo["mu1ips"] = psqrt(mu1ips2)
            CandidateInfo["mu2ips"] = psqrt(mu2ips2)
            CandidateInfo["mu1mip"] = mu1ippvs_
            CandidateInfo["mu2mip"] = mu2ippvs_
            CandidateInfo["mu1ip"] = mu1ip_
            CandidateInfo["mu2ip"] = mu2ip_
            CandidateInfo["Blife_ps"] = Blife_
            CandidateInfo["Blife_ps_r"] = ctau_r(b)*light_cte

            #### Magnet polarity and measured mass error (Carlos):
            magTool = appMgr().service('MagneticFieldSvc', interface = cpp.ILHCbMagnetSvc)
            if magTool.isDown(): magPol = -1.
            elif not magTool.isDown(): magPol = 1.
            else: magPol = 0.
            CandidateInfo["Polarity"] = magPol
            CandidateInfo["B_MMass"] = b.measuredMass()
            CandidateInfo["B_MMass_Err"] = b.measuredMassErr()
            ##############################
        
            CandidateInfo["DOCA"] = muonsDOCA_
            CandidateInfo["Bip"] = Bip_
            CandidateInfo["angle"] = DirAngle_                      
            CandidateInfo["lessIPSmu"] = min(CandidateInfo["mu1ips"],CandidateInfo["mu2ips"])
            CandidateInfo["C_angle"] =  angleToflight(B1, B2)
            CandidateInfo["buggy_angle"] =  buggy_angle(B1, B2)
            CandidateInfo["AP_alpha"] = ap_alpha
            CandidateInfo["AP_pt"] = ap_pt
            CandidateInfo["mu1_track_Chi2"] = TRCHI2 (mu1)
            CandidateInfo["mu2_track_Chi2"] = TRCHI2 (mu2)
            CandidateInfo["bid"] = bid
            CandidateInfo["NPVs"] = TES[self.RootInTES + "Rec/Vertex/Primary"].size()
            CandidateInfo["mu1_track_Chi2DoF"] = TRCHI2DOF (mu1)
            CandidateInfo["mu2_track_Chi2DoF"] = TRCHI2DOF (mu2)
            
            KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
            CandidateInfo["mu1_track_CloneDist"] = KULLBACK(mu1)
            CandidateInfo["mu2_track_CloneDist"] = KULLBACK(mu2)

            CandidateInfo['mu1ptot'] = P(mu1)
            CandidateInfo['mu1pt'] = PT(mu1)
            CandidateInfo['mu2ptot'] = P(mu2)
            CandidateInfo['mu2pt'] = PT(mu2)
            CandidateInfo['Bptot'] = P(b)
            CandidateInfo['Bpt'] = PT(b)
            #print "PT(b)=",PT(b)
            CandidateInfo['Bdis'] = vmod(rSV)
            CandidateInfo["LF_time"], CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] = Double(0.), Double(0.), Double(0.)
            sc = self.LifeFitter.fit ( Done["refittedPV"], b , CandidateInfo["LF_time"],  CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] )

            CandidateInfo["LF_time"] = CandidateInfo["LF_time"]*1000
            CandidateInfo["LF_terr"] = CandidateInfo["LF_terr"]*1000

            theDira = DIRA(Done["refittedPV"])
            CandidateInfo["DIRA"] = theDira(b)
            #print "USING THE NEW BENDER"
            
            CandidateInfo["muon1BDT"] = muon1BDT_func(b)
            CandidateInfo["muon2BDT"] = muon2BDT_func(b)
            
            #CandidateInfo["mu1ID"] = ID(mu1)
            #CandidateInfo["mu2ID"] = ID(mu2)
            
            for i in range(1,6):
                CandidateInfo["mu1isoRnS"+str(i)] = isosRnS[0][i]
                CandidateInfo["mu2isoRnS"+str(i)] = isosRnS[1][i]
            CandidateInfo["mu1isoRnSHLT1"] = isosRnSHLT1[0][1]
            CandidateInfo["mu2isoRnSHLT1"] = isosRnSHLT1[1][1]
                #print "Here"
                
                

            
            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
           
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    ##print "key=",key
                    tup.column(key,CandidateInfo[key])
                if self.TRIGGER:
                    self.decisions   ( b , self.triggers['KS0'] )
                    self.tisTos      (
                        b               , ## particle 
                        tup               , ## n-tuple
                        'V0_'            , ## prefix for variable name in n-tuple
                        self.lines['KS0'] , ## trigger lines to be inspected
                        verbose = True      ## good ooption for those who hates bit-wise operations 
                        )
                    self.decisions   ( b , self.triggers['Miguel'] )
                    self.tisTos      (
                        b               , ## particle 
                        tup               , ## n-tuple
                        'Miguelito_'            , ## prefix for variable name in n-tuple
                        self.lines['Miguel'] , ## trigger lines to be inspected
                        verbose = True      ## good ooption for those who hates bit-wise operations 
                        )
                    self.decisions   ( b , self.triggers['RnS'] )
                    self.tisTos      (
                        b               , ## particle 
                        tup               , ## n-tuple
                        'RnS_'            , ## prefix for variable name in n-tuple
                        self.lines['RnS'] , ## trigger lines to be inspected
                        verbose = True      ## good ooption for those who hates bit-wise operations 
                        )
                ##print "inside self.TUP"
                ##print "RELINFO:",RELINFO('/Event/Dimuon/Phys/TriggerTestLine/Muon1BDT', 'MU_BDT', -42.)
                ##print "todo with RELINFO:",dir(RELINFO('/Event/Dimuon/Phys/TriggerTestLine/Muon1BDT', 'MU_BDT', -42.))
                
                tup.write()
            if self.DST:
                self.addedKeys = CandidateInfo.keys()
                self.addedKeys.sort()
                for i in range(len(self.addedKeys)):
                    j = 500 + i
                    b.addInfo(j,CandidateInfo[self.addedKeys[i]])
           ##  if self.DST and self.Sel_Above_GL:
##                 if CandidateInfo["GLsb."] > self.Sel_Above_GL:
##                     self.setFilterPassed( True )
##                     print "GL :", CandidateInfo["GLsb."]
        self.setFilterPassed( True )

                    
                
        ## This is outside the loop !!
        #if not self.Sel_Above_GL: self.setFilterPassed( True )
        if self.DEBUG: print "Preselected "+self.decayname
        return SUCCESS

    def finalize(self):
        print self.decayname + " counter. \n -----------"
        print self.COUNTER
        if self.DST:
            print "[||] Keys for added Info", self.name()
            for i in range(len(self.addedKeys)):
                print 500+i, self.addedKeys[i]
        print "../../../../../"
        return Algo.finalize ( self ) # used to be AlgoMC

