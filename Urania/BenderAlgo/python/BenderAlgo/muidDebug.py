
Xend = [[ 47.7, 95.5,191,382],
        [ 60, 120, 240, 480],
        [ 65, 129, 259, 517],
        [ 69, 139, 277, 555 ],
        [ 74, 148, 296, 593 ]]

Yend = [[ 39.8, 79.6, 159., 318],
        [ 50, 100, 200, 400 ],
        [ 54, 110, 210, 430 ],
        [ 57, 116, 230, 460 ],
        [ 62, 124, 247, 490 ]]


def getTrRegion (x, y, station):
    region=-1
    if ((abs(x) <= Xend[station][0])
        and (abs(y) <= Yend[station][0])): region= 0
    
    if (((abs(x) <= Xend[station][1])
         and (abs(y) <= Yend[station][1]) )
        and ((abs(x) >= Xend[station][0]) or (abs(y) >= Yend[station][0])) ): region= 1
    
    if ( ((abs(x) <= Xend[station][2]) and (abs(y) <= Yend[station][2]) )
         and ((abs(x) >= Xend[station][1]) or (abs(y) >= Yend[station][1])) ): region= 2

    if ( ((abs(x) <= Xend[station][3]) and (abs(y) <= Yend[station][3]))
         and ((abs(x) >= Xend[station][2]) or (abs(y) >= Yend[station][2])) ): region= 3
    return region



z_sts=[12110.0,15270.0,16470.0,17670.0,18870.0]



class GetRegion:
    def __init__(self,par,st):
        self.par=par
        self.st=st
        self.z=z_sts[st-1]
        
    def extrapol(self,tx,ty,x0,y0,z0):
        x=x0+tx*(self.z-z0)
        y=y0+ty*(self.z-z0)
        return x,y

    def reg(self,t1):
        
        x0=t1[self.par+"o1"]
        y0=t1[self.par+"o2"]
        z0=t1[self.par+"o3"]
        if float(t1[self.par+"p3"])!=0:
            tx=t1[self.par+"p1"]/float(t1[self.par+"p3"])
            ty=t1[self.par+"p2"]/float(t1[self.par+"p3"])
        else:
            tx=-1000000000
            ty=-1000000000
        x,y=self.extrapol(tx,ty,x0,y0,z0)
        return getTrRegion (x/10., y/10., self.st-1)



def muonhits(TES):
    MuonCoords = TES["Raw/Muon/Coords"]
    myd={}
    for st in range(1,6):
        myd[st]={}
        for reg in range(1,5): myd[st][reg]=0
    if not MuonCoords:
        print "BenderAlgo.muidDebug speaking: MuonCoords doesn't exist. Skipping. Most likely you don't really care"
        return myd
    if not MuonCoords:
        print "BenderAlgo.muidDebug speaking: MuonCoords exists but has no size. Skipping. Most likely you don't really care"
        return myd    
    for el in MuonCoords:
        tile=el.key()
        station=tile.station()+1
        region=tile.region()+1
        myd[station][region]+=1

    return myd


def muonhitsFOI(mu,TES):

    mutrack=0
    for mpid in TES["Rec/Muon/MuonPID"]:
        if not mpid.IsMuonLoose(): continue
        if mpid.idTrack().key()==mu.proto().track().key():
            mutrack=mpid.muonTrack()
            break

    out={1:0,2:0,3:0,4:0,5:0}
    if not mutrack: return out
    for id in mutrack.lhcbIDs():
        st=id.muonID().station()+1
        out[st]+=1
    return out
